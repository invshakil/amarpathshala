<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    //
    protected $fillable = [
        'title', 'description', 'user_id', 'category',
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function getAuthorName()
    {
        return User::where('id', $this->user_id)->first()->name;
    }

    public function category()
    {
        return $this->belongsTo(BlogCategory::class,'category','id');
    }

    public function getCategoryName()
    {
        return BlogCategory::where('id', $this->category)->first()->name;
    }

}
