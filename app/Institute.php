<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Institute extends Model
{
    protected $fillable = [
        'institute_name',
        'type',
        'description',
        'website',
        'country',
        'state',
        'area',
        'status',
    ];

    static function getAllArticles($name)
    {
        $id = Institute::where('institute_name',$name)->value('institute_id');
        return Article::where('institute', $id)->paginate(12);
    }

    public function instituteType()
    {
        return $this->belongsTo(Institute::class,'type','institute_id');
    }

    public function getInstituteTypeName($id)
    {
        return InstituteType::where('id', $id)->value('institute_type_title');
    }
}
