<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsType extends Model
{
    protected $fillable = ['news_type_title', 'news_type_desc', 'visibility_status'];

    public function article()
    {
        return $this->hasMany(Article::class,'news_type','id');
    }

    public function ArticleCount($id)
    {
        $data = Article::where('news_type', $id)->get();
        return count($data);
    }
}
