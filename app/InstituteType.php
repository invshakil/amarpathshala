<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstituteType extends Model
{
    protected $fillable = ['institute_type_title', 'institute_type_desc', 'visibility_status'];
}
