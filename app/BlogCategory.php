<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class BlogCategory extends Model
{
    protected $fillable = [
        'name', 'description', 'status'
    ];

    public function blogCategories()
    {
        return $this->hasMany(BlogCategory::class);
    }

    public function blogs()
    {
        return $this->hasMany('App\Blog', 'category', 'id');
    }

    public function BlogCount($id)
    {
        $data = Blog::where('category', $id)->get();
        return count($data);
    }




}
