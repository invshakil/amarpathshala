<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstituteGroup extends Model
{
    protected $fillable = ['institute_group_title', 'institute_group_desc', 'visibility_status'];
}
