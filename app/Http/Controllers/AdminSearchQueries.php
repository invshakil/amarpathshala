<?php

namespace App\Http\Controllers;

use App\Article;
use App\Blog;
use App\Institute;
use App\InstituteType;
use Illuminate\Http\Request;

class AdminSearchQueries extends Controller
{
    //

    public function SearchInstituteType(Request $request)
    {
        $query = $request->input('search');

        $data['result'] = InstituteType::where('institute_type_title',"LIKE", "%" . $query . "%")->paginate(10);


        $data['nav_class'] = "sidebar-o sidebar-inverse side-scroll";
        $data['page_title'] = 'Search Result of '.$query;

        // PAGE SETTINGS

        //MODULE BASED NAVIGATION
        $nav = view('backend.admin.navigation.adminNav.newsNav',$data);
        $data['navigation'] = view('backend.admin.navigation.adminNav.default')->with(['nav' => $nav]);

        return view('backend.admin.content_pages.news_publications.search_result_institute_type', $data);
    }

    public function SearchInstitute(Request $request)
    {
        $query = $request->input('search');

        $data['institutes'] = Institute::where('institute_name',"LIKE", "%" . $query . "%")->paginate(10);

        $data['nav_class'] = "sidebar-o sidebar-inverse side-scroll";
        $data['page_title'] = 'Manage Institute Type';

        // PAGE SETTINGS

        //MODULE BASED NAVIGATION
        $nav = view('backend.admin.navigation.adminNav.newsNav',$data);
        $data['navigation'] = view('backend.admin.navigation.adminNav.default')->with(['nav' => $nav]);

        return view('backend.admin.content_pages.news_publications.manage_institute', $data);
    }

    public function SearchNews(Request $request)
    {
        $query = $request->input('search');

        $data['articles'] = Article::where('article_title',"LIKE", "%" . $query . "%")->paginate(10);

        $data['nav_class'] = "sidebar-o sidebar-inverse side-scroll";
        $data['page_title'] = 'Search Result of '.$query;

        // PAGE SETTINGS

        //MODULE BASED NAVIGATION
        $nav = view('backend.admin.navigation.adminNav.newsNav',$data);
        $data['navigation'] = view('backend.admin.navigation.adminNav.default')->with(['nav' => $nav]);

        return view('backend.admin.content_pages.news_publications.manage_news', $data);
    }

    public function SearchBlog(Request $request)
    {
        $query = $request->input('search');

        $data['blogs'] = Blog::where('title',"LIKE", "%" . $query . "%")->paginate(10);

        $data['nav_class'] = "sidebar-o sidebar-inverse side-scroll";
        $data['page_title'] = 'Search Result of '.$query;

        // PAGE SETTINGS

        //MODULE BASED NAVIGATION
        $nav = view('backend.blogs.blogNav.blogNav', $data);

        $data['navigation'] = view('backend.admin.navigation.adminNav.default')->with(['nav' => $nav]);

        return view('backend.blogs.manage_blog', $data);
    }
}
