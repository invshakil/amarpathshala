<?php

namespace App\Http\Controllers;

use App\Blog;
use App\BlogCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\ImageManagerStatic as Image;

class BlogManagement extends Controller
{
    public function blogCategory()
    {
        $data = array();
        $data['nav_class'] = "sidebar-o sidebar-inverse side-scroll";
        $data['page_title'] = 'Manage Blog Category';

        //MODULE BASED NAVIGATION
        $nav = view('backend.blogs.blogNav.blogNav', $data);

        $data['navigation'] = view('backend.admin.navigation.adminNav.default')->with(['nav' => $nav]);

        return view('backend.blogs.blog_category', $data);
    }

    public function FetchBlogCategory(Request $request)
    {
        $items = BlogCategory::orderBy('id', 'desc')->paginate(10);
        return response()->json($items);
    }

    public function BlogCategoryStore(Request $request)
    {

        $category = new BlogCategory;
        $category->name = $request->name;
        $category->description = $request->description;
        $category->status = $request->status;

        $create = $category->save();
        return response()->json($create);

    }

    public function BlogCategoryDelete(Request $request)
    {
        $id = $request->input('id');

        BlogCategory::find($id)->delete();
        return response()->json(['done']);
    }

    public function BlogCategoryUpdate(Request $request)
    {
        $category = BlogCategory::firstOrCreate(['id' => $request->id]);
        $category->name = $request->name;
        $category->description = $request->description;
        $category->status = $request->status;

        $updated = $category->save();
        return response()->json($updated);
    }

    public function CreateBlog()
    {
        $data = array();
        $data['nav_class'] = "sidebar-o sidebar-inverse side-scroll";
        $data['page_title'] = 'Create Blog';

        //MODULE BASED NAVIGATION
        $nav = view('backend.blogs.blogNav.blogNav', $data);

        $data['navigation'] = view('backend.admin.navigation.adminNav.default')->with(['nav' => $nav]);

        return view('backend.blogs.create_blog', $data);
    }

    public function BlogStore(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:blogs',
            'description' => 'required',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:512',
            'url_slug' => 'unique:blogs',
        ]);

        $blog = new Blog;
        $blog->title = $request->title;
        $blog->description = $request->description;
        $blog->category = $request->category;
        $blog->meta_keyword = $request->meta_keyword;
        $blog->meta_description = $request->meta_description;
        $blog->tags = $request->tags;
        $blog->url_slug = $request->url_slug;
        $blog->status = $request->status;


        //Getting image file and setting the name of image with extension
        $photo = $request->file('image');
        $image_name = time() . '.' . $photo->getClientOriginalExtension();

        //Setting up the image path
        $destinationPath = public_path('/uploads/blog_images/');
        $photo->move($destinationPath, $image_name);

        $img = Image::make('public/uploads/blog_images/' . $image_name);
        //Image resizing with aspect ratio and saving it to the directory
        $img->resize(null, 440, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($destinationPath . '/' . $image_name, 85);

        $image_path = '/uploads/blog_images/' . $image_name; // large image path

        // Thumbnail Size conversion

        $thumbs_path = public_path('/uploads/blog_images/re_sized/');

        //Image resizing with aspect ratio and saving it to the directory
        $img->resize(null, 80, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($thumbs_path . '/' . $image_name, 80);
        $thumb_path = '/uploads/blog_images/re_sized/' . $image_name; // thumb image path

        $blog->cover = $image_path;
        $blog->thumbnail = $thumb_path;
        $blog->user_id = auth()->user()->id;

        $create = $blog->save();

        if ($create) {
            alert()->success('Blog Information Saved Successfully!', 'Great Job!')->autoclose(3500)->persistent("Close this");
        } else {
            alert()->error('Something Went Wrong!', 'Oops')->autoclose(3500)->persistent("Close this");
        }
        return back();
    }

    public function ManageBlog()
    {
        $data = array();
        $data['nav_class'] = "sidebar-o sidebar-inverse side-scroll";
        $data['page_title'] = 'Manage Blogs';

        $blogs = Blog::orderBy('updated_at','desc')->paginate(10);
        $data['blogs'] = $blogs;

        //MODULE BASED NAVIGATION
        $nav = view('backend.blogs.blogNav.blogNav', $data);

        $data['navigation'] = view('backend.admin.navigation.adminNav.default')->with(['nav' => $nav]);

        return view('backend.blogs.manage_blog', $data);
    }

    public function BlogDelete($id)
    {
        $done = Blog::where('id', $id)->delete();

        if ($done) {
            alert()->success('Blog Information info Deleted!', 'Great Job!')->autoclose(3500)->persistent("Close this");
        } else {
            alert()->error('Something Went Wrong!', 'Oops')->autoclose(3500)->persistent("Close this");
        }
        return back();
    }

    public function EditBlog($id)
    {
        $data = array();
        $data['id'] = $id;

        $data['nav_class'] = "sidebar-o sidebar-inverse side-scroll";
        $data['page_title'] = 'Edit Blog';

        //MODULE BASED NAVIGATION
        $nav = view('backend.blogs.blogNav.blogNav', $data);

        $data['navigation'] = view('backend.admin.navigation.adminNav.default')->with(['nav' => $nav]);

        return view('backend.blogs.edit_blog', $data);
    }

    public function BlogUpdate(Request $request)
    {
        $this->validate($request, [
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:512',
        ]);

        // UPLOADING LOGO

        if ($request->image != '') {

            //Getting image file and setting the name of image with extension
            $photo = $request->file('image');
            $image_name = time() . '.' . $photo->getClientOriginalExtension();

            //Setting up the image path
            $destinationPath = public_path('/uploads/blog_images/');
            $photo->move($destinationPath, $image_name);

            $img = Image::make('public/uploads/blog_images/' . $image_name);
            //Image resizing with aspect ratio and saving it to the directory
            $img->resize(null, 440, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($destinationPath . '/' . $image_name, 85);

            $image_path = '/uploads/blog_images/' . $image_name; // large image path

            // Thumbnail Size conversion

            $thumbs_path = public_path('/uploads/blog_images/re_sized/');

            //Image resizing with aspect ratio and saving it to the directory
            $img->resize(null, 80, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbs_path . '/' . $image_name, 80);
            $thumb_path = '/uploads/blog_images/re_sized/' . $image_name; // thumb image path

        }

        $data = array(
            'title' => $request->input('title'),

            'category' => $request->input('category'),
            'description' => $request->input('description'),
            'meta_keyword' => $request->input('meta_keyword'),
            'meta_description' => $request->input('meta_description'),
            'status' => $request->input('status'),
            'url_slug' => $request->input('url_slug'),
            'tags' => $request->input('tags'),

            'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
        );

        if (isset($image_path)) {
            $data['cover'] = $image_path;
        }

        if (isset($thumb_path)) {
            $data['thumbnail'] = $thumb_path;
        }

        $id = $request->id;

        $update = DB::table('blogs')->where('id', $id)->update($data);

        if ($update) {
            alert()->success('Article Information Updated Successfully!', 'Great Job!')->autoclose(3500)->persistent("Close this");
        } else {
            alert()->error('Something Went Wrong!', 'Oops')->autoclose(3500)->persistent("Close this");
        }
        return back();
    }

    public function PendingBlog()
    {
        $data = array();
        $data['nav_class'] = "sidebar-o sidebar-inverse side-scroll";
        $data['page_title'] = 'Pending Blogs';

        $blogs = Blog::orderBy('updated_at','desc')->where('status',0)->paginate(10);
        $data['blogs'] = $blogs;

        //MODULE BASED NAVIGATION
        $nav = view('backend.blogs.blogNav.blogNav', $data);

        $data['navigation'] = view('backend.admin.navigation.adminNav.default')->with(['nav' => $nav]);

        return view('backend.blogs.manage_blog', $data);
    }

    public function BlogByUser(Request $request)
    {

        $data = array();
        $data['nav_class'] = "sidebar-o sidebar-inverse side-scroll";
        $data['page_title'] = 'My Blogs';

        $blogs = Blog::orderBy('updated_at','desc')->where('user_id', $request->author)->paginate(10);
        $data['blogs'] = $blogs;

        //MODULE BASED NAVIGATION
        $nav = view('backend.blogs.blogNav.blogNav', $data);

        $data['navigation'] = view('backend.admin.navigation.adminNav.default')->with(['nav' => $nav]);

        return view('backend.blogs.manage_blog', $data);
    }
}
