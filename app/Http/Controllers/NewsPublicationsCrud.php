<?php

namespace App\Http\Controllers;

use App\Area;
use App\Article;
use App\Country;
use App\Institute;
use App\InstituteGroup;
use App\InstituteType;
use App\NewsType;
use App\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;


class NewsPublicationsCrud extends Controller
{
    /**
     * Display a listing of the News Type.
     *
     * @return \Illuminate\Http\Response
     */

    public function news_type_index(Request $request)
    {
        $items = NewsType::orderBy('id', 'desc')->paginate(10);
        return response()->json($items);
    }

    /**
     * Store a newly created News Type in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function news_type_store(Request $request)
    {
        $create = NewsType::create($request->all());
        return response()->json($create);
    }

    public function news_type_update(Request $request)
    {
        $data = array(
            'news_type_title' => $request->input('news_type_title'),
            'news_type_desc' => $request->input('news_type_desc'),
            'visibility_status' => $request->input('visibility_status'),
            'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
        );

        $id = $request->input('news_type_id');

        $edit = DB::table('news_types')->where('id', $id)->update($data);

        return response()->json($edit);
    }

    public function news_type_delete(Request $request)
    {

        $id = $request->input('id');

        NewsType::find($id)->delete();
        return response()->json(['done']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function inst_type_index(Request $request)
    {
        $items = InstituteType::orderBy('id', 'desc')->paginate(10);
        return response()->json($items);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function inst_type_store(Request $request)
    {
        $create = InstituteType::create($request->all());
        return response()->json($create);
    }

    public function inst_type_update(Request $request)
    {
        $data = array(
            'institute_type_title' => $request->input('institute_type_title'),
            'institute_type_desc' => $request->input('institute_type_desc'),
            'visibility_status' => $request->input('visibility_status'),
            'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
        );

        $id = $request->input('institute_type_id');

        $edit = DB::table('institute_types')->where('id', $id)->update($data);

        return response()->json($edit);
    }

    public function inst_type_delete(Request $request)
    {

        $id = $request->input('id');

        InstituteType::find($id)->delete();
        return response()->json(['done']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function inst_group_index(Request $request)
    {
        $items = InstituteGroup::orderBy('id', 'desc')->paginate(10);
        return response()->json($items);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function inst_group_store(Request $request)
    {
        $create = InstituteGroup::create($request->all());
        return response()->json($create);
    }

    public function inst_group_update(Request $request)
    {
        $data = array(
            'institute_group_title' => $request->input('institute_group_title'),
            'institute_group_desc' => $request->input('institute_group_desc'),
            'visibility_status' => $request->input('visibility_status'),
            'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
        );

        $id = $request->input('institute_group_id');

        $edit = DB::table('institute_groups')->where('id', $id)->update($data);

        return response()->json($edit);
    }

    public function inst_group_delete(Request $request)
    {

        $id = $request->input('id');

        InstituteGroup::find($id)->delete();
        return response()->json(['done']);
    }

    /*
     * COUNTRY, STATE/DIVISION, DISTRICT/AREA CRUD
     */

    public function countryStore(Request $request)
    {
        $country = new Country;
        $country->country_name = $request->country_name;
        $create = $country->save();
        return response()->json($create);
    }

    public function getAllCountries()
    {
        $result = DB::table('countries')->orderBy('country_name', 'asc')->get();

        return response()->json($result);
    }

    public function stateStore(Request $request)
    {
        $State = new State;
        $State->state_name = $request->state_name;
        $State->country = $request->country;
        $create = $State->save();
        return response()->json($create);
    }

    public function getStateById($id)
    {
        $result = DB::table('states')->where('country', $id)->get();

        return response()->json($result);
    }

    public function areaStore(Request $request)
    {
        $area = new Area;
        $area->area_name = $request->area_name;
        $area->state = $request->state;
        $area->country = $request->country;
        $create = $area->save();
        return response()->json($create);
    }

    public function getAreaById($id)
    {
        $result = DB::table('areas')->where('state', $id)->get();

        return response()->json($result);
    }

    /*
     * Institutes CRUD
     */

    public function InstituteStore(Request $request)
    {
        $this->validate($request, [
            'institute_name' => 'unique:institutes',
            'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:512',
        ]);

        $institute = new Institute;

        $institute->institute_name = $request->institute_name;
        $institute->type = $request->institute_type;
        $institute->description = $request->description;
        $institute->website = $request->website;
        $institute->email = $request->email;
        $institute->director = $request->director;
        $institute->country = $request->country;
        $institute->state = $request->state;
        $institute->area = $request->area;
        $institute->status = $request->status;

        // UPLOADING LOGO

        if ($request->logo != '') {
            $logoName = $request->institute_name . '.' . $request->logo->getClientOriginalExtension();
            $request->logo->move(public_path('uploads/institute_logo/'), $logoName);
            $institute->logo = 'uploads/institute_logo/' . $logoName;
        }

        $create = $institute->save();

        if ($create) {
            alert()->success('Institute Information Saved Successfully!', 'Great Job!')->autoclose(3500)->persistent("Close this");
        } else {
            alert()->error('Something Went Wrong!', 'Oops')->autoclose(3500)->persistent("Close this");
        }
        return back();
    }

    public function InstituteDelete($id)
    {
        $done = Institute::where('institute_id', $id)->delete();

        if ($done) {
            alert()->success('Institute Information info Deleted!', 'Great Job!')->autoclose(3500)->persistent("Close this");
        } else {
            alert()->error('Something Went Wrong!', 'Oops')->autoclose(3500)->persistent("Close this");
        }
        return back();
    }

    public function InstituteUpdate(Request $request, $id)
    {
        $this->validate($request, [
            'logo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:512',
        ]);

        // UPLOADING LOGO

        if ($request->logo != '') {
            $logoName = $request->institute_name . '.' . $request->logo->getClientOriginalExtension();
            $request->logo->move(public_path('uploads/institute_logo/'), $logoName);
            $logoPath = 'uploads/institute_logo/' . $logoName;

        }

        $data = array(
            'institute_name' => $request->institute_name,
            'type' => $request->institute_type,
            'description' => $request->description,
            'website' => $request->website,
            'email' => $request->email,
            'director' => $request->director,
            'country' => $request->country,
            'state' => $request->state,
            'area' => $request->area,
            'status' => $request->status,
        );

        if (isset($logoPath)) {
            $data['logo'] = $logoPath;
        }

        $update = DB::table('institutes')->where('institute_id', $id)->update($data);

        if ($update) {
            alert()->success('Institute Information Updated Successfully!', 'Great Job!')->autoclose(3500)->persistent("Close this");
        } else {
            alert()->error('Something Went Wrong!', 'Oops')->autoclose(3500)->persistent("Close this");
        }
        return back();
    }

    public function fetchByTypeId($id)
    {
        $result = DB::table('institutes')->where('type', $id)->get();

        return response()->json($result);
    }

    /*
     * News CRUD
     */

    public function StoreNews(Request $request)
    {
        $this->validate($request, [
            'article_title' => 'unique:articles',
            'image' => 'required',
            'institute_type' => 'required',
            'institute' => 'required',
            'group' => 'required',
            'url_slug' => 'unique:articles',
        ]);


        //Getting image file and setting the name of image with extension
        $photo = $request->file('image');
        $image_name = time() . '.' . $photo->getClientOriginalExtension();

        //Setting up the image path
        $destinationPath = public_path('/uploads/article_image/');
        $photo->move($destinationPath, $image_name);

        $img = Image::make('public/uploads/article_image/' . $image_name);
        //Image resizing with aspect ratio and saving it to the directory
        $img->resize(null, 440, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($destinationPath . '/' . $image_name, 85);

        $image_path = '/uploads/article_image/' . $image_name; // large image path

        //Medium Size Image conversion

        $m_path = public_path('/uploads/article_image/m_size/');

        //Image resizing with aspect ratio and saving it to the directory
        $img->resize(null, 180, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($m_path . '/' . $image_name, 70);
        $m_path = '/uploads/article_image/m_size/' . $image_name; // medium image path

        // Thumbnail Size conversion

        $thumbs_path = public_path('/uploads/article_image/re_sized/');

        //Image resizing with aspect ratio and saving it to the directory
        $img->resize(null, 70, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($thumbs_path . '/' . $image_name, 70);
        $thumb_path = '/uploads/article_image/re_sized/' . $image_name; // thumb image path


        $data = array(
            'article_title' => $request->input('article_title'),

            'cover' => $image_path,
            'thumbnail_size' => $thumb_path,
            'tiny_thumb' => $m_path,

            'news_type' => $request->input('news_type'),
            'content' => $request->input('content'),
            'institute_type' => $request->input('institute_type'),
            'meta_keyword' => $request->input('meta_keyword'),
            'meta_description' => $request->input('meta_description'),
            'status' => $request->input('status'),
            'group' => $request->input('group'),
            'url_slug' => $request->input('url_slug'),
            'institute' => $request->input('institute'),

            'created_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
        );

        $create = DB::table('articles')->insert($data);

        if ($create) {
            alert()->success('News Information Saved Successfully!', 'Great Job!')->autoclose(3500)->persistent("Close this");
        } else {
            alert()->error('Something Went Wrong!', 'Oops')->autoclose(3500)->persistent("Close this");
        }

        if ($data['status'] == 'Yes')
        {
            $value = Session::get('mail_status_' . $data['url_slug']);

            if ($value != 1) {

                $this->sendMailToInstitute($data);

                Session::put('mail_status_' . $data['url_slug'], 1);
            }
        }

        return back();
    }

    public function EditNews($id)
    {
        $data = array();
        $data['article_id'] = $id;
        $data['nav_class'] = "sidebar-o sidebar-inverse side-scroll";
        $data['page_title'] = 'Edit News';

        // PAGE AND NAV SETTINGS

        $page = view('backend.admin.content_pages.news_publications.edit_pages.edit_news', $data);
        $nav = view('backend.admin.navigation.adminNav.newsNav', $data);

        //MODULE BASED NAVIGATION

        $data['navigation'] = view('backend.admin.navigation.adminNav.default')->with(['nav' => $nav]);

        return view('backend.admin.master', $data)->with(['page' => $page]);
    }

    public function NewsDelete($id)
    {
        $done = DB::table('articles')->where('article_id', $id)->delete();

        if ($done) {
            alert()->success('Article Information info Deleted!', 'Great Job!')->autoclose(3500)->persistent("Close this");
        } else {
            alert()->error('Something Went Wrong!', 'Oops')->autoclose(3500)->persistent("Close this");
        }
        return back();
    }

    public function UpdateNewsInfo(Request $request)
    {
        $this->validate($request, [
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:512',
        ]);

        // UPLOADING LOGO

        if ($request->image != '') {
            //Getting image file and setting the name of image with extension
            $photo = $request->file('image');
            $image_name = time() . '.' . $photo->getClientOriginalExtension();

            //Setting up the image path
            $destinationPath = public_path('/uploads/article_image/');
            $photo->move($destinationPath, $image_name);

            $img = Image::make('public/uploads/article_image/' . $image_name);
            //Image resizing with aspect ratio and saving it to the directory
            $img->resize(null, 440, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($destinationPath . '/' . $image_name, 85);

            $image_path = '/uploads/article_image/' . $image_name; // large image path

            //Medium Size Image conversion

            $m_path = public_path('/uploads/article_image/m_size/');

            //Image resizing with aspect ratio and saving it to the directory
            $img->resize(null, 160, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($m_path . '/' . $image_name, 70);
            $m_path = '/uploads/article_image/m_size/' . $image_name; // medium image path

            // Thumbnail Size conversion

            $thumbs_path = public_path('/uploads/article_image/re_sized/');

            //Image resizing with aspect ratio and saving it to the directory
            $img->resize(null, 70, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbs_path . '/' . $image_name, 70);
            $thumb_path = '/uploads/article_image/re_sized/' . $image_name; // thumb image path

        }

        $data = array(
            'article_title' => $request->input('article_title'),

            'news_type' => $request->input('news_type'),
            'content' => $request->input('content'),
            'institute_type' => $request->input('institute_type'),
            'meta_keyword' => $request->input('meta_keyword'),
            'meta_description' => $request->input('meta_description'),
            'status' => $request->input('status'),
            'group' => $request->input('group'),
            'url_slug' => $request->input('url_slug'),
            'institute' => $request->input('institute'),

            'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
        );

        if (isset($image_path)) {
            $data['cover'] = $image_path;
        }
        if (isset($m_path)) {
            $data['thumbnail_size'] = $m_path;
        }
        if (isset($thumb_path)) {
            $data['tiny_thumb'] = $thumb_path;
        }

        $id = $request->article_id;

        $update = DB::table('articles')->where('article_id', $id)->update($data);


        if ($update) {
            alert()->success('Article Information Updated Successfully!', 'Great Job!')->autoclose(3500)->persistent("Close this");
        } else {
            alert()->error('Something Went Wrong!', 'Oops')->autoclose(3500)->persistent("Close this");
        }


        if ($data['status'] == 'Yes')
        {
            $value = Session::get('mail_status_' . $data['url_slug']);

            if ($value != 1) {

                $news_info = Article::where('article_id', $id)->first()->toArray();

                $this->sendMailToInstitute($news_info);

                Session::put('mail_status_' . $data['url_slug'], 1);
            }
        }


        return back();
    }

    public function sendMailToInstitute($param_data)
    {
        // SENDING MAIL TO INSTITUTE
        $inst_id = $param_data['institute'];
        $inst_info = Institute::where('institute_id', $inst_id)->first()->toArray();


        if (count($inst_info) > 0) {
            $data['email'] = $inst_info['email'];
            $data['subject'] = $param_data['article_title'];

            Mail::send('mails.SendNewsMail', ['param_data' => $param_data], function ($m) use ($data) {
                $m->from('info@amarpathshala.com', 'Amarpathshala');
                $m->to($data['email'], 'www.amarpathshala.com')->subject($data['subject']);
            });


        } else {
            alert()->warning('Institute Email not found!', 'Add email to institute to send mail!')->autoclose(3500)->persistent("Close this");
        }
    }


}
