<?php

namespace App\Http\Controllers;

use App\Article;
use App\Blog;
use App\BlogCategory;
use App\Institute;
use App\InstituteType;
use App\NewsType;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request as Req;

class HomeController extends Controller
{
    public function index()
    {
        $data = array();

        $data['news_types'] = NewsType::where('visibility_status', 'Yes')->get();

        $data['articles'] = Article::orderBy('created_at', 'desc')->where('status', 'Yes')->simplePaginate(9,['*'], 'latest-news');

        $data['blogs'] = Blog::orderBy('created_at', 'desc')->where('status', 1)->simplePaginate(8,['*'], 'latest-blogs');

        $data['keywords'] = 'Amarpathshala';
        $data['description'] = 'Amarpathshala';
        $data['author'] = 'Amarpathshala';

        $data['image'] = 'https://i.imgur.com/VnhXjna.png';
        $data['site_name'] = 'Amarpathshala';
        $data['url'] = Req::url();

        $data['title'] = 'Home | Amarpathshala';
        return view('frontend.pages.homeContent', $data);
    }

    public function getArticleByNewsType($name)
    {
        $type = str_replace('-', ' ', $name);

        $id = NewsType::where('news_type_title', $type)->value('id');

        $data = array();
        $data['cat_name'] = $type;
        $data['articles'] = Article::orderBy('created_at', 'desc')->where(['news_type' => $id, 'status' => 'Yes'])->simplePaginate(10);
        $data['news_types'] = NewsType::where('visibility_status', 'Yes')->get();
        $data['title'] = $type . ' | Amarpathshala';
        return view('frontend.pages.ArticleByType', $data);

    }

    public function getArticleDetails($slug)
    {
        $data = array();
        $data['article_details'] = Article::where('url_slug', $slug)->first();
        $data['related_articles'] = Article::where('institute', $data['article_details']->institute)
            ->whereNotIn('url_slug', [$slug])
            ->limit(4)
            ->get();


        $id = $data['article_details']->article_id;

        $next = Article::where('status', 'Yes')->orderBy('article_id','asc')
            ->where('article_id', '>', $id)
            ->first();

        $prev = Article::where('status', 'Yes')
            ->where('article_id', '<', $id)->orderBy('article_id','desc')
            ->first();



        if(count($next) > 0)
        {
            $data['next_slug'] = $next->url_slug;
            $data['next_title'] = $next->article_title;
        }

        if (count($prev) > 0) {
            $data['prev_slug'] = $prev->url_slug;
            $data['prev_title'] = $prev->article_title;
        }


        $data['keywords'] = $data['article_details']->meta_keyword;
        $data['description'] = $data['article_details']->meta_description;
        $data['author'] = 'Admin';

        $data['image'] = asset($data['article_details']->cover);
        $data['site_name'] = 'Amarpathshala';
        $data['url'] = Req::url();

        $get_count = $data['article_details']->hit_count;
        $add_count = $get_count + 1;

        DB::table('articles')->where('article_id', $data['article_details']->article_id)->update(['hit_count' => $add_count]);

        $data['title'] = $data['article_details']->article_title . ' | Amarpathshala';
        return view('frontend.pages.ArticleDetails', $data);

    }

    public function getBlogDetails($slug)
    {
        $data = array();
        $data['blog_details'] = Blog::where('url_slug', $slug)->first();
        $data['related_blogs'] = Blog::where('category', $data['blog_details']->category)
            ->whereNotIn('url_slug', [$slug])
            ->orderBy(DB::raw('RAND()'))
            ->limit(4)->get();

        $id = $data['blog_details']->id;

        $next = Blog::where('status', 1)->orderBy('id','asc')
            ->where('id', '>', $id)
            ->first();

        $prev = Blog::where('status', 1)
            ->where('id', '<', $id)->orderBy('id','desc')
            ->first();



        if(count($next) > 0)
        {
            $data['next_slug'] = $next->url_slug;
            $data['next_title'] = $next->title;
        }

        if (count($prev) > 0) {
            $data['prev_slug'] = $prev->url_slug;
            $data['prev_title'] = $prev->title;
        }



        $data['keywords'] = $data['blog_details']->meta_keyword;
        $data['description'] = $data['blog_details']->meta_description;
        $data['author'] = $data['blog_details']->getAuthorName();

        $data['image'] = asset($data['blog_details']->cover);
        $data['site_name'] = 'Amarpathshala';
        $data['url'] = Req::url();

        $get_count = $data['blog_details']->hit_count;
        $add_count = $get_count + 1;

        DB::table('blogs')->where('id', $data['blog_details']->id)->update(['hit_count' => $add_count]);

        $data['title'] = $data['blog_details']->title . ' | Amarpathshala';
        return view('frontend.pages.BlogDetails', $data);
    }

    public function getBlogsByBlogCategory($name)
    {
        $type = str_replace('-', ' ', $name);

        $id = BlogCategory::where('name', $type)->value('id');

        $data = array();
        $data['cat_name'] = $type;
        $data['articles'] = Blog::orderBy('created_at', 'desc')->where(['category' => $id, 'status' => 1])->simplePaginate(10);
        $data['categories'] = BlogCategory::where('status', 1)->get();
        $data['title'] = $type . ' | Amarpathshala';
        return view('frontend.pages.BlogsByCategory', $data);
    }

    public function getChaptersBySubject()
    {
        $data = array();
        $data['title'] = 'All Chapters of Subject | Amarpathshala';
        return view('frontend.pages.chapters_by_subject', $data);
    }

    public function getLectureBySubject()
    {
        $data = array();
        $data['title'] = 'All Lectures of Subject | Amarpathshala';
        return view('frontend.pages.lectures_by_chapter', $data);
    }

    public function getInstituteDetails($slug)
    {
        $name = str_replace('-', ' ', $slug);
        $data['info'] = Institute::where('institute_name', $name)->first();
        $data['articles'] = Institute::getAllArticles($name);

        $data['keywords'] = $name;
        $data['description'] = substr($data['info']->description, 0, '160') . '...';
        $data['author'] = 'Amarpathshala';

        $data['image'] = asset($data['info']->logo);
        $data['site_name'] = 'Amarpathshala';
        $data['url'] = Req::url();

        $data['title'] = $name . ' | Amarpathshala';
        $data['name'] = $name;
        return view('frontend.pages.InstituteDetails', $data);
    }

    public function getInstitutesByType($slug)
    {
        $name = str_replace('-', ' ', $slug);
        $info = InstituteType::where('institute_type_title', $name)->first();

        $data['institutes'] = Institute::where('type', $info->id)->simplePaginate(12);

        $data['keywords'] = $name;
        $data['description'] = substr($info->institute_type_desc, 0, '160') . '...';
        $data['author'] = 'Amarpathshala';

        $data['image'] = 'https://i.imgur.com/VnhXjna.png';
        $data['site_name'] = 'Amarpathshala';
        $data['url'] = Req::url();

        $data['title'] = $name . ' | Amarpathshala';
        $data['name'] = $name;
        return view('frontend.pages.getInstitutesByType', $data);
    }

    public function getAuthorProfile($slug)
    {
        $name = str_replace('-', ' ', $slug);
        $data['info'] = User::where('name', $name)->first();
        $id = $data['info']->id;
        $data['articles'] = Blog::where('user_id', $id)->orderBy('id', 'desc')->simplePaginate(12);

        $data['keywords'] = $name;
        $data['description'] = substr($data['info']->name . 'is an author of amarpathshala. Check out his blogs!', 0, '160') . '...';
        $data['author'] = $name;

        $data['image'] = asset($data['info']->profile_picture);
        $data['site_name'] = 'Amarpathshala';
        $data['url'] = Req::url();

        $data['title'] = $name . ' | Amarpathshala';
        $data['name'] = $name;
        return view('frontend.pages.getAuthorProfile', $data);
    }

    public function SearchQuery(Request $request)
    {
        $query = $request->input('search');
        $data['search'] = $query;
        $data['title'] = 'Search Result on ' . $query . ' | Amarpathshala';

        /*
         * HERE SEARCH CAN BE 3 TYPE
         * 1. NEWS
         * 2. BLOG
         * 3. INSTITUTE
         */

        // News
        $articles = Article::where('article_title', "LIKE", "%" . $query . "%")->simplePaginate(10);
        $blogs = Blog::where('title', "LIKE", "%" . $query . "%")->simplePaginate(10);
        $institutes = Institute::where('institute_name', "LIKE", "%" . $query . "%")->simplePaginate(10);
        if (count($articles) > 0) {
            $data['count'] = count($articles);
            $data['articles'] = $articles;

        } elseif (count($blogs) > 0) {
            $data['count'] = count($blogs);
            $data['blogs'] = $blogs;
        } elseif (count($institutes) > 0) {
            $data['count'] = count($institutes);
            $data['institutes'] = $institutes;
        }

        return view('frontend.pages.Search_Result', $data);
    }
}
