<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;


class AdminController extends Controller
{
    public function __construct()
    {
//        $this->middleware('AdminMiddleware');
    }

    /*
     * ADMIN DASHBOARD VIEW
     */
    public function index()
    {

        $data = array();
        $data['page_title'] = 'Welcome to Admin Dashboard';

        return view('backend.admin..content_pages.dashboard_content', $data);
    }

    /*
     * NEWS PUBLICATIONS MODULE
     */

    public function newsPublications()
    {
        $data = array();
        $data['nav_class'] = "sidebar-o sidebar-inverse side-scroll";
        $data['page_title'] = 'Post New News';

        // PAGE AND NAV SETTINGS

        $nav = view('backend.admin.navigation.adminNav.newsNav',$data);

        //MODULE BASED NAVIGATION

        $data['navigation'] = view('backend.admin.navigation.adminNav.default')->with(['nav' => $nav]);

        return view('backend.admin..content_pages.news_publications.new_news', $data);
    }

    public function ManageNews()
        {
            $data = array();
            $data['nav_class'] = "sidebar-o sidebar-inverse side-scroll";
            $data['page_title'] = 'Manage News';

            $data['articles'] = DB::table('articles')->latest()->paginate(15);
            // PAGE AND NAV SETTINGS

            $nav = view('backend.admin.navigation.adminNav.newsNav',$data);

            //MODULE BASED NAVIGATION

            $data['navigation'] = view('backend.admin.navigation.adminNav.default')->with(['nav' => $nav]);

            return view('backend.admin.content_pages.news_publications.manage_news', $data);
        }

    public function newsType()
    {
        $data = array();
        $data['nav_class'] = "sidebar-o sidebar-inverse side-scroll";
        $data['page_title'] = 'Manage News Type';

        // PAGE SETTINGS


        //MODULE BASED NAVIGATION
        $nav = view('backend.admin.navigation.adminNav.newsNav',$data);
        $data['navigation'] = view('backend.admin.navigation.adminNav.default')->with(['nav' => $nav]);

        return view('backend.admin.content_pages.news_publications.news_type', $data);
    }

    public function instituteType()
    {
        $data = array();
        $data['nav_class'] = "sidebar-o sidebar-inverse side-scroll";
        $data['page_title'] = 'Manage Institute Type';

        // PAGE SETTINGS

        //MODULE BASED NAVIGATION
        $nav = view('backend.admin.navigation.adminNav.newsNav',$data);
        $data['navigation'] = view('backend.admin.navigation.adminNav.default')->with(['nav' => $nav]);

        return view('backend.admin.content_pages.news_publications.institute_type', $data);
    }

    public function AddInstitute()
    {
        $data = array();
        $data['nav_class'] = "sidebar-o sidebar-inverse side-scroll";
        $data['page_title'] = 'Manage Institute Type';

        // PAGE SETTINGS

        //MODULE BASED NAVIGATION
        $nav = view('backend.admin.navigation.adminNav.newsNav',$data);
        $data['navigation'] = view('backend.admin.navigation.adminNav.default')->with(['nav' => $nav]);

        return view('backend.admin.content_pages.news_publications.add_institute', $data);
    }

    public function ManageInstitute()
    {
        $data = array();
        $data['institutes'] = DB::table('institutes')->latest()->paginate(15);
        $data['nav_class'] = "sidebar-o sidebar-inverse side-scroll";
        $data['page_title'] = 'Manage Institute Type';

        // PAGE SETTINGS

        //MODULE BASED NAVIGATION
        $nav = view('backend.admin.navigation.adminNav.newsNav',$data);
        $data['navigation'] = view('backend.admin.navigation.adminNav.default')->with(['nav' => $nav]);

        return view('backend.admin.content_pages.news_publications.manage_institute', $data);
    }

    public function instituteGroup()
    {
        $data = array();
        $data['nav_class'] = "sidebar-o sidebar-inverse side-scroll";
        $data['page_title'] = 'Manage Institute Groups';


        //MODULE BASED NAVIGATION
        $nav = view('backend.admin.navigation.adminNav.newsNav',$data);
        $data['navigation'] = view('backend.admin.navigation.adminNav.default')->with(['nav' => $nav]);

        return view('backend.admin.content_pages.news_publications.institute_group', $data);
    }


    public function user_settings()
    {
        $data = array();
        $data['page_title'] = 'User Settings';

        return view('backend.admin.content_pages.user_settings', $data);
    }

    public function UpdateUserSettings(Request $request)
    {
        $this->validate($request, [
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:512',
        ]);

        // UPLOADING LOGO

        if ($request->image != '') {

            //Getting image file and setting the name of image with extension
            $photo = $request->file('image');
            $image_name = time() . '.' . $photo->getClientOriginalExtension();

            //Setting up the image path
            $destinationPath = public_path('/uploads/author_images/');
            $photo->move($destinationPath, $image_name);

            $img = Image::make('public/uploads/author_images/' . $image_name);
            //Image resizing with aspect ratio and saving it to the directory
            $img->resize(null, 500, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($destinationPath . '/' . $image_name, 85);

            $image_path = '/uploads/author_images/' . $image_name; // large image path

            // Thumbnail Size conversion

            $thumbs_path = public_path('/uploads/author_images/re_sized/');

            //Image resizing with aspect ratio and saving it to the directory
            $img->resize(null, 80, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbs_path . '/' . $image_name, 80);
            $thumb_path = '/uploads/author_images/re_sized/' . $image_name; // thumb image path

        }

        $data = array(
            'name' => $request->input('name'),

            'facebook' => $request->input('facebook'),
            'twitter' => $request->input('twitter'),
            'google' => $request->input('google'),
            'linkedin' => $request->input('linkedin'),

            'email' => $request->input('email'),
            'address' => $request->input('address'),
            'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
        );

        if (isset($image_path)) {
            $data['profile_picture'] = $image_path;
        }

        if (isset($thumb_path)) {
            $data['thumb_pic'] = $thumb_path;
        }

        $id = $request->id;

        $update = DB::table('users')->where('id', $id)->update($data);

        if ($update) {
            alert()->success('User Information Updated Successfully!', 'Great Job!')->autoclose(3500)->persistent("Close this");
        } else {
            alert()->error('Something Went Wrong!', 'Oops')->autoclose(3500)->persistent("Close this");
        }
        return back();
    }

}
