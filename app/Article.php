<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    protected $fillable = [
        'article_title', 'cover', 'news_type', 'content', 'meta_description', 'meta_keyword', 'url_slug', 'institute_type',
        'institute', 'group', 'status'
    ];


    public function news_type()
    {
        return $this->belongsTo(NewsType::class,'id','news_type');
    }

    public function getNewsTypeName()
    {
        return NewsType::where('id', $this->news_type)->first()->news_type_title;
    }

    public function institute()
    {
        return $this->belongsTo(Institute::class,'institute_id','institute');
    }

    public function instituteDetails()
    {
        return Institute::where('institute_id',$this->institute)->first();
    }
}
