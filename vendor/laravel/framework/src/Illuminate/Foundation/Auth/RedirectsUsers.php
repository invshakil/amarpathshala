<?php

namespace Illuminate\Foundation\Auth;

trait RedirectsUsers
{
    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
//        if (method_exists($this, 'redirectTo')) {
//            return $this->redirectTo();
//        }
//
//        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/login';
        if (auth()->user()->role == 'Admin')
        {
            return '/admin/dashboard';
        }
        elseif (auth()->user()->role == 'Teacher')
        {
            return '/teacher';
        }
        elseif (auth()->user()->role == 'Student')
        {
            return '/student';
        }

        return '/login';
    }
}
