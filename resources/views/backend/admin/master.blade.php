<!doctype html>
<!--[if lte IE 9]>
<html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!-->
<html lang="en" class="no-focus"> <!--<![endif]-->
<head>
    @include('backend.admin.layout_pages.header_script')
</head>
<body>

<div id="page-container" class="@if(isset($nav_class)){{ $nav_class }}@endif page-header-fixed main-content-narrow">

@if(isset($navigation))
    {!! $navigation !!}
@endif


<!-- Header -->
    <header id="page-header">
        <!-- Header Content -->
        @include('backend.admin.layout_pages.content_header')
        <!-- END Header Content -->



        <!-- Header Loader -->
        <!-- Please check out the Activity page under Elements category to see examples of showing/hiding it -->
        <div id="page-header-loader" class="overlay-header bg-primary">
            <div class="content-header content-header-fullrow text-center">
                <div class="content-header-item">
                    <i class="fa fa-sun-o fa-spin text-white"></i>
                </div>
            </div>
        </div>
        <!-- END Header Loader -->
    </header>
    <!-- END Header -->

    <!-- Main Container -->
    <main id="main-container" >
        <!-- Page Content -->
        <div class="content" style="padding: 10px!important; max-width: 98%">
            @yield('main_content')
        </div>
        <!-- END Page Content -->
    </main>
    <!-- END Main Container -->

    <!-- Footer -->
    <footer id="page-footer" class="opacity-0">
        <div class="content py-20 font-size-xs clearfix">
            <div class="float-right">
                Developed By <i class="fa fa-heart text-pulse"></i> by <a class="font-w600" href="http://techno71.com"
                                                                          target="_blank">Techno-71</a>
            </div>
            <div class="float-left">
                <a class="font-w600" href="https://sshakil.com" target="_blank">Shakil</a> &copy; <span
                        class="js-year-copy">2017</span>
            </div>
        </div>
    </footer>
    <!-- END Footer -->
</div>
<!-- END Page Container -->

<!-- Codebase Core JS -->
@include('backend.admin.layout_pages.footer_script')



</body>
</html>