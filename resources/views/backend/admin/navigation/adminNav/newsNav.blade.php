@extends('backend.admin.navigation.adminNav.default')

@section('nav')

            <li>
                <a @if(Request::url() == route('admin.institute.types')) class="active" @endif
                        href="{{ route('admin.institute.types') }}"><i class="fa fa-tags"></i> Institute Types</a>
            </li>
            <li>
                <a @if(Request::url() == route('admin.add.institutes')) class="active" @endif
                        href="{{ route('admin.add.institutes') }}"><i class="fa fa-tags"></i> Add Institutes</a>
            </li>
            <li>
                <a @if(Request::url() == route('admin.manage.institutes')) class="active" @endif
                href="{{ route('admin.manage.institutes') }}"><i class="fa fa-tags"></i> Manage Institutes</a>
            </li>
            <li >
                <a @if(Request::url() == route('admin.institute.group')) class="active" @endif
                href="{{ route('admin.institute.group') }}"><i class="fa fa-tags"></i> Group Names</a>
            </li>
            <li >
                <a @if(Request::url() == route('admin.news.type')) class="active" @endif
                href="{{ route('admin.news.type') }}"><i class="fa fa-tags"></i> News Type</a>
            </li>
            <li >
                <a @if(Request::url() == route('admin.new.news')) class="active" @endif
                href="{{ route('admin.new.news') }}"><i class="fa fa-tags"></i> New News</a>
            </li>
            <li >
                <a @if(Request::url() == route('admin.manage.news')) class="active" @endif
                href="{{ route('admin.manage.news') }}"><i class="fa fa-tags"></i> Manage News</a>
            </li>
@endsection