<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

<title>@yield('title')</title>

<meta name="description"
      content="">
<meta name="author" content="pixelcave">
<meta name="robots" content="noindex, nofollow">

<!-- Open Graph Meta -->
<meta property="og:title" content="">
<meta property="og:site_name" content="">
<meta property="og:description"
      content="">
<meta property="og:type" content="website">
<meta property="og:url" content="">
<meta property="og:image" content="">

<!-- Icons -->
<!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
<link rel="shortcut icon" href="{{ asset('assets/') }}/img/favicons/favicon.png">
{{--<link rel="icon" type="image/png" sizes="192x192" href="{{ asset('assets/') }}/img/favicons/favicon-192x192.png">--}}
{{--<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/') }}/img/favicons/apple-touch-icon-180x180.png">--}}
<!-- END Icons -->

<!-- Stylesheets -->
<!-- Codebase framework -->
<link rel="stylesheet" id="css-main" href="{{ asset('assets/') }}/css/codebase.min.css">

<script src="{{ asset('assets/') }}/js/core/jquery.min.js"></script>

<style>

    /*.Apagination {*/
    /*display: inline-block;*/
    /*}*/

    .Apagination a {
        color: black;
        float: left;
        padding: 8px 16px;
        text-decoration: none;
        border: 1px solid #ddd;
    }

    .Apagination li.active {
        background-color: #337ab7;

        border-color: #337ab7;
    }

    .Apagination li.active a {
        color: #FFFFFF;
    }

    .Apagination a:hover:not(.active) {
        background-color: #ddd;
    }

    .Apagination a:first-child {
        border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;
    }

    .Apagination a:last-child {
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
    }

    .Apagination > .disabled > a, .Apagination > .disabled > a:focus, .Apagination > .disabled > a:hover, .Apagination > .disabled > span, .Apagination > .disabled > span:focus, .Apagination > .disabled > span:hover {
        color: #777;
        cursor: not-allowed;
        background-color: #fff;
        border-color: #ddd;
    }

    img {
        max-width: 100%;
    }

    input[type=file] {
        padding: 10px;
        background: #2d2d2d;
    }

    .content-heading {
        margin: 0 auto !important;
        padding: 10px !important;
    }

    .badge {
        padding: 10px 20px;
        font-size: 14px;
    }

    .has-error .form-control {
        border-color: #a94442;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);

    }

    .has-error .checkbox, .has-error .checkbox-inline, .has-error .control-label, .has-error .help-block, .has-error .radio, .has-error .radio-inline, .has-error.checkbox label, .has-error.checkbox-inline label, .has-error.radio label, .has-error.radio-inline label {
        color: white;
        border-radius: 5px;
        padding-left: 15px;
        padding-top: 5px;
        padding-bottom: 1px;
        font-size: 13px;
    }

    .help-block {
        display: block;
        margin-top: 5px;
        margin-bottom: 10px;
        color: #737373;
        background-color: rgba(255, 8, 16, 0.71);

    }

    .text-dual-primary-dark {
        color: #005800!important;
    }

    .font-size-xl {
        font-size: 1.428571rem !important;
        color: red!important;
    }
</style>

<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="{{ asset('/') }}/assets/js/plugins/sweetalert2/sweetalert2.min.css">
<script src="{{ asset('/') }}/assets/js/plugins/sweetalert2/sweetalert2.min.js"></script>

@include('sweet::alert')