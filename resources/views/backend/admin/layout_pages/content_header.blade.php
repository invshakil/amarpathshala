<div class="content-header">
    <!-- Left Section -->
    <div class="content-header-section">
        <!-- Toggle Sidebar -->
        <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
        @if(isset($navigation))
            <button type="button" class="btn btn-circle btn-dual-secondary" data-toggle="layout"
                    data-action="sidebar_toggle">
                <i class="fa fa-navicon"></i>
            </button>
    @endif




    <!-- END Toggle Sidebar -->

    </div>
    <div class="content-header-section d-none d-lg-block">

        <ul class="nav-main-header">
            <li>
                <a @if(Request::url() == route('admin.dashboard')) class="active"
                   @endif href="{{ route('admin.dashboard') }}"><i class="si si-compass"></i>Dashboard</a>
            </li>
            <li>
                <a class="nav-submenu
                    @if(Request::url() == route('admin.new.news') ||
                    Request::url() == route('admin.institute.types') ||
                    Request::url() == route('admin.add.institutes') ||
                    Request::url() == route('admin.manage.institutes') ||
                    Request::url() == route('admin.institute.group')||
                    Request::url() == route('admin.news.type')
                    )  active
                    @endif
                        " data-toggle="nav-submenu" href="#"><i class="si si-puzzle"></i>News Publications</a>
                <ul>
                    <li>
                        <a @if(Request::url() == route('admin.institute.types')) class="active" @endif
                        href="{{ route('admin.institute.types') }}"><i class="fa fa-tags"></i> Institute Types</a>
                    </li>
                    <li>
                        <a @if(Request::url() == route('admin.add.institutes')) class="active" @endif
                        href="{{ route('admin.add.institutes') }}"><i class="fa fa-tags"></i> Add Institutes</a>
                    </li>
                    <li>
                        <a @if(Request::url() == route('admin.manage.institutes')) class="active" @endif
                        href="{{ route('admin.manage.institutes') }}"><i class="fa fa-tags"></i> Manage Institutes</a>
                    </li>
                    <li>
                        <a @if(Request::url() == route('admin.institute.group')) class="active" @endif
                        href="{{ route('admin.institute.group') }}"><i class="fa fa-tags"></i> Group Names</a>
                    </li>
                    <li>
                        <a @if(Request::url() == route('admin.news.type')) class="active" @endif
                        href="{{ route('admin.news.type') }}"><i class="fa fa-tags"></i> News Type</a>
                    </li>
                    <li>
                        <a @if(Request::url() == route('admin.new.news')) class="active" @endif
                        href="{{ route('admin.new.news') }}"><i class="fa fa-tags"></i> New News</a>
                    </li>
                    <li >
                        <a @if(Request::url() == route('admin.manage.news')) class="active" @endif
                        href="{{ route('admin.manage.news') }}"><i class="fa fa-tags"></i> Manage News</a>
                    </li>
                </ul>
            </li>
            <li>
                <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-compass"></i>Blog</a>
                <ul>
                    <li>
                        <a @if(Request::url() == route('blog.category.index')) class="active" @endif
                        href="{{ route('blog.category.index') }}"><i class="fa fa-tags"></i> Blog Categories</a>
                    </li>
                    <li>
                        <a @if(Request::url() == route('create.blog.index')) class="active" @endif
                        href="{{ route('create.blog.index') }}"><i class="fa fa-tags"></i> Create Blog</a>
                    </li>
                    <li>
                        <a @if(Request::url() == route('admin.manage.blog')) class="active" @endif
                        href="{{ route('admin.manage.blog') }}"><i class="fa fa-tags"></i> Manage Blogs</a>
                    </li>
                    <li>
                        <a @if(Request::url() == route('admin.pending.blog')) class="active" @endif
                        href="{{ route('admin.pending.blog') }}"><i class="fa fa-tags"></i> Pending Blogs</a>
                    </li>
                    <li>
                        <a @if(Request::url() == route('admin.blog.by.user')) class="active" @endif
                        href="{{ route('admin.blog.by.user',['author'=>auth()->user()->id, 'name'=>auth()->user()->name]) }}"><i
                                    class="fa fa-tags"></i> My Blogs</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="si si-magnifier"></i>Search</a>
            </li>
            <li>
                <a href="#"><i class="si si-action-undo"></i>Go Back</a>
            </li>
        </ul>
    </div>
    <!-- END Left Section -->

    <!-- Right Section -->
    <div class="content-header-section">
        <!-- User Dropdown -->
        <div class="btn-group" role="group">
            <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ auth()->user()->name }}<i class="fa fa-angle-down ml-5"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right min-width-150"
                 aria-labelledby="page-header-user-dropdown">
                <a class="dropdown-item" href="#">
                    <i class="si si-user mr-5"></i> Profile
                </a>

                <div class="dropdown-divider"></div>

                <!-- Toggle Side Overlay -->
                <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
                <a class="dropdown-item" href="{{ route('user.settings') }}" data-toggle="layout"
                   data-action="side_overlay_toggle">
                    <i class="si si-wrench mr-5"></i> Settings
                </a>
                <!-- END Side Overlay -->

                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('logout')}}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <i class="si si-logout mr-5"></i> Sign Out
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
        <!-- END User Dropdown -->

    </div>
    <!-- END Right Section -->
</div>