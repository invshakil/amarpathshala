<script src="{{ asset('assets/') }}/js/core/popper.min.js"></script>
<script src="{{ asset('assets/') }}/js/core/bootstrap.min.js"></script>
<script src="{{ asset('assets/') }}/js/core/jquery.slimscroll.min.js"></script>
<script src="{{ asset('assets/') }}/js/core/jquery.scrollLock.min.js"></script>
<script src="{{ asset('assets/') }}/js/core/jquery.appear.min.js"></script>
<script src="{{ asset('assets/') }}/js/core/jquery.countTo.min.js"></script>
<script src="{{ asset('assets/') }}/js/core/js.cookie.min.js"></script>
<script src="{{ asset('assets/') }}/js/codebase.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>


