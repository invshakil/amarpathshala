@extends('backend.admin.master')

@section('title')
    {{ $page_title }}
@endsection

@section('main_content')

    @if (Session::has('sweet_alert.alert'))
        <script>
            swal({!! Session::get('sweet_alert.alert') !!});
        </script>
    @endif

    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <script>
                swal(
                        'Oops!',
                        '{{ $error }}',
                        'error'
                );
            </script>
        @endforeach
    @endif

    <!-- USER SETTINGS -->

    <div class="block">
        <div class="block-header block-header-default">
            <h3></h3>
            <div class="block-options">
                <button type="button" class="btn-block-option" data-toggle="block-option"
                        data-action="fullscreen_toggle"></button>
                <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle"
                        data-action-mode="demo">
                    <i class="si si-refresh"></i>
                </button>
                <button type="button" class="btn-block-option" data-toggle="block-option"
                        data-action="content_toggle"></button>
            </div>
        </div>
        <div class="block-content items-push">
            <h2 class="content-heading">User Settings</h2>

            <form data-toggle="validator" action="{{ route('user.settings.update') }}" method="post"
                  enctype="multipart/form-data">

                {{ csrf_field() }}

                <input type="hidden" name="id" value="{{ auth()->user()->id }}">
                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="example-text-input">User Name</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" value="{{ auth()->user()->name }}"
                               name="name" placeholder="Full Name.." data-error="Please enter Full Name." required>

                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="example-text-input">User Name</label>
                    <div class="col-md-9">
                        <input type="email" class="form-control" value="{{ auth()->user()->email }}"
                               name="email" placeholder="Enter Email.." data-error="Please enter Email." required>

                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label">Profile Picture</label>
                    <div class="col-md-3 col-form-label">
                        <label class="custom-file">
                            <input type="file" class="custom-file-input" id=""
                                   name="image" onchange="readURL(this);">

                            <span class="custom-file-control"></span>

                        </label>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-md-3">
                        @if(auth()->user()->profile_picture == NULL)
                            <img id="blah" src="http://placehold.it/620x348" style="max-height: 150px;"
                                 class="img-responsive img-thumbnail img-center" alt="your image"/>
                        @else
                            <img src="{{ asset(auth()->user()->profile_picture) }}" style="max-height: 150px;"
                                 class="img-responsive img-thumbnail img-center" alt="your image"/>
                        @endif
                    </div>
                </div>


                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="example-textarea-input">User Address</label>
                    <div class="col-md-9">
                                <textarea type="text" class="form-control" name="address"
                                          rows="6" placeholder="Description.."
                                          data-error="Please enter Description."
                                          required>{{ auth()->user()->address }}</textarea>

                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="example-text-input">Facebook Profile Link</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" value="{{ auth()->user()->facebook }}"
                               name="facebook" placeholder="Enter facebook link..">

                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="example-text-input">Twitter Profile Link</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" value="{{ auth()->user()->twitter }}"
                               name="twitter" placeholder="Enter twitter link..">

                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="example-text-input">Google Plus Profile Link</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" value="{{ auth()->user()->google }}"
                               name="google" placeholder="Enter google link..">

                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="example-text-input">Linkedin Profile Link</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" value="{{ auth()->user()->linkedin }}"
                               name="linkedin" placeholder="Enter linkedin link..">

                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="form-group row">

                    <div class="col-md-9 ml-auto">
                        <button type="submit" class="btn btn-alt-primary mr-5 mb-5">
                            <i class="fa fa-plus mr-5"></i> Update Profile
                        </button>
                    </div>
                </div>

            </form>

        </div>
    </div>

    <style>
        img {
            max-width: 100%;
        }

        input[type=file] {
            padding: 10px;
            background: #2d2d2d;
        }

        .content-heading {
            margin: 0 auto !important;
            margin-bottom: 40px !important;
        }

    </style>

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                            .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

@endsection