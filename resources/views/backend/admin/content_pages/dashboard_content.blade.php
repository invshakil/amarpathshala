@extends('backend.admin.master')

@section('title')
    {{ $page_title }}
@endsection

@section('main_content')
    <!-- Colorful Multi Row Icon Tiles -->
    <h2 class="content-heading">Module <small>List</small></h2>
    <div class="row gutters-tiny">
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-transparent text-center bg-primary" href="{{ route('blog.category.index') }}">
                <div class="block-content bg-black-op-5">
                    <p class="font-w600 text-white">Blog Module</p>
                </div>
                <div class="block-content">
                    <p>
                        <i class="si si-badge fa-4x text-white-op"></i>
                    </p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-transparent text-center bg-success" href="{{ route('admin.new.news') }}">
                <div class="block-content bg-black-op-5">
                    <p class="font-w600 text-white">News Publications</p>
                </div>
                <div class="block-content">
                    <p>
                        <i class="si si-envelope fa-4x text-white-op"></i>
                    </p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-transparent text-center bg-gd-sun" href="javascript:void(0)">
                <div class="block-content bg-black-op-5">
                    <p class="font-w600 text-white">Cart</p>
                </div>
                <div class="block-content">
                    <p>
                        <i class="si si-bag fa-4x text-white-op"></i>
                    </p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-transparent text-center bg-corporate" href="javascript:void(0)">
                <div class="block-content bg-black-op-5">
                    <p class="font-w600 text-white">Live Stats</p>
                </div>
                <div class="block-content">
                    <p>
                        <i class="si si-bar-chart fa-4x text-white-op"></i>
                    </p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-transparent text-center bg-flat" href="javascript:void(0)">
                <div class="block-content bg-black-op-5">
                    <p class="font-w600 text-white">~ 7.5 MB/s</p>
                </div>
                <div class="block-content bg-flat">
                    <p>
                        <i class="si si-cloud-upload fa-4x text-white-op"></i>
                    </p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-transparent text-center bg-elegance" href="javascript:void(0)">
                <div class="block-content bg-black-op-5">
                    <p class="font-w600 text-white">Lab</p>
                </div>
                <div class="block-content">
                    <p>
                        <i class="si si-chemistry fa-4x text-white-op"></i>
                    </p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-transparent text-center bg-info" href="javascript:void(0)">
                <div class="block-content">
                    <p class="mt-5">
                        <i class="si si-diamond fa-4x text-white-op"></i>
                    </p>
                </div>
                <div class="block-content bg-black-op-5">
                    <p class="font-w600 text-white">Minecraft</p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-transparent text-center bg-gd-dusk" href="javascript:void(0)">
                <div class="block-content">
                    <p class="mt-5">
                        <i class="si si-camera fa-4x text-white-op"></i>
                    </p>
                </div>
                <div class="block-content bg-black-op-5">
                    <p class="font-w600 text-white">Videos</p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-transparent text-center bg-warning" href="javascript:void(0)">
                <div class="block-content">
                    <p class="mt-5">
                        <i class="si si-support fa-4x text-white-op"></i>
                    </p>
                </div>
                <div class="block-content bg-black-op-5">
                    <p class="font-w600 text-white">Support</p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-transparent text-center bg-pulse" href="javascript:void(0)">
                <div class="block-content">
                    <p class="mt-5">
                        <i class="si si-bubbles fa-4x text-white-op"></i>
                    </p>
                </div>
                <div class="block-content bg-black-op-5">
                    <p class="font-w600 text-white">Chat</p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-transparent text-center bg-gd-lake" href="javascript:void(0)">
                <div class="block-content">
                    <p class="mt-5">
                        <i class="si si-compass fa-4x text-white-op"></i>
                    </p>
                </div>
                <div class="block-content bg-black-op-5">
                    <p class="font-w600 text-white">Locating..</p>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-4 col-xl-2">
            <a class="block block-transparent text-center bg-black-op-75" href="javascript:void(0)">
                <div class="block-content">
                    <p class="mt-5">
                        <i class="si si-globe fa-4x text-white-op"></i>
                    </p>
                </div>
                <div class="block-content bg-white-op-5">
                    <p class="font-w600 text-white">World Live</p>
                </div>
            </a>
        </div>
    </div>
    <!-- END Colorful Multi Row Icon Tiles -->
@endsection()