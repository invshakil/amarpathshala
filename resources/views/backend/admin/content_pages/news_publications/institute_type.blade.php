@extends('backend.admin.master')

@section('title')
    {{ $page_title }}
@endsection

@section('main_content')

    <div class="row">

        <div class="col-sm-12 col-lg-3">


            <div class="block">
                <h2 class="content-heading">Add Institute type </h2>

                <div class="block-content " id="create-type">

                    <form data-toggle="validator" action="{{ route('institute-type.store') }}" method="post">

                        <input type="hidden" name="csrf-token" value="{{ csrf_token() }}">

                        <div class="form-group row">
                            <label class="col-12 col-form-label" for="example-hf-email">Institute Type Title</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="institute_type_title" id="title"
                                       placeholder="Enter Title" data-error="Please enter title." required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-12 col-form-label" for="example-hf-password">Institute Type
                                Description</label>
                            <div class="col-md-12">
                                    <textarea type="text" name="institute_type_desc" id="description"
                                              class="form-control"
                                              placeholder="Description.." data-error="Please enter Description."
                                              required></textarea>
                                <div class="help-block with-errors"></div>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-12">Visibility Status</label>
                            <div class="col-md-12">
                                <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="status"
                                           name="visibility_status" value="Yes" checked>
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Yes</span>
                                </label>

                                <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="status"
                                           name="visibility_status" value="No">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">No</span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-alt-primary mr-5 mb-5 crud-submit">
                                <i class="fa fa-plus mr-5"></i> Add Institute Type
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-lg-9">

            <!-- MANAGE NEWS -->

            <div class="block">
                <h2 class="content-heading">Manage Institute Type </h2>

                <div class="block-content">

                    {{-- SEARCH --}}

                    <div class="block pull-r-l">
                        <div class="block-content block-content-full block-content-sm bg-body-light">
                            <form action="{{ route('admin.search.institute.type') }}" method="get">
                                <div class="input-group">
                                    <input type="text" class="form-control"  name="search"
                                           placeholder="Search..">
                                    <span class="input-group-btn">
                                            <button type="submit" class="btn btn-secondary px-10">
                                                <i class="fa fa-search"></i>
                                            </button>
                            </span>
                                </div>
                            </form>
                        </div>
                    </div>


                    <!-- DataTables init on table by adding .js-dataTable-full-Apagination class, functionality initialized in js/pages/be_tables_datatables.js -->
                    <table class="table table-bordered table-striped table-vcenter">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>Title</th>
                            <th class="d-none d-sm-table-cell">Description</th>
                            <th class="d-none text-center d-sm-table-cell" style="width: 20%;">Visibility Status</th>
                            <th class="text-center" style="width: 15%;">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <!-- AJAX DATA -->
                        </tbody>
                    </table>
                    <nav aria-label="Page navigation">
                        <ul id="pagination" class="Apagination pagination-sm"></ul>
                    </nav>

                </div>
            </div>
        </div>
    </div>



    <!--MODAL AREA -->

    <!-- Pop Out Modal -->

    <div class="modal fade" id="ConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="modal-popout"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-popout" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Are you sure?</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <p>Do you really want to delete these records? This process cannot be undone.</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="data_id" class="btn btn-danger" data-dismiss="modal">
                        <i class="fa fa-check"></i> Yes, Delete this data
                    </button>
                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Pop Out Modal -->


    <!-- Edit Item Modal -->
    <div class="modal fade" id="edit-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Item</h4>
                </div>
                <div class="modal-body">

                    <form data-toggle="validator" action="{{ route('institute-type.edit') }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="institute_type_id" value="" id="update_id">

                        <div class="form-group row">
                            <label class="col-12 col-form-label" for="example-hf-email">Institute Type Title</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="institute_type_title" id="update_title"
                                       placeholder="Enter Title" data-error="Please enter title." required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-12 col-form-label" for="example-hf-password">Institute Type
                                Description</label>
                            <div class="col-md-12">
                                    <textarea type="text" name="institute_type_desc" id="update_desc"
                                              class="form-control"
                                              placeholder="Description.." data-error="Please enter Description."
                                              required></textarea>
                                <div class="help-block with-errors"></div>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-12">Visibility Status</label>
                            <div class="col-md-12">
                                <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="update_status"
                                           name="visibility_status" value="Yes">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Yes</span>
                                </label>

                                <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="update_status"
                                           name="visibility_status" value="No">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">No</span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success crud-submit-edit">Submit</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <!--MODAL AREA -->

    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.3.1/jquery.twbsPagination.min.js"></script>


    <script type="text/javascript">
        var url = "<?php echo route('institute-type.index')?>";
    </script>

    <script>

        /*
         * Pagination Functions with fetching data
         */

        var page = 1;
        var current_page = 1;
        var total_page = 0;
        var is_ajax_fire = 0;

        manageData();

        /* manage data list */

        function manageData() {
            $.ajax({
                dataType: 'json',
                url: url,
                data: {page: page}
            }).done(function (data) {

                total_page = data.last_page;
                current_page = data.current_page;

                $('#pagination').twbsPagination({
                    totalPages: total_page,
                    visiblePages: current_page,
                    onPageClick: function (event, pageL) {
                        page = pageL;
                        if (is_ajax_fire != 0) {
                            getPageData();
                        }
                    }
                });

                manageRow(data.data);
                is_ajax_fire = 1;
            });
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('input[name="csrf-token"]').val()
            }
        });

        /* Get Page Data*/
        function getPageData() {
            $.ajax({
                dataType: 'json',
                url: url,
                data: {page: page}
            }).done(function (data) {
                manageRow(data.data);
            });
        }


        function manageRow(data) {
            var rows = '';
            $.each(data, function (key, value) {
                rows = rows + '<tr>';
                rows = rows + '<td width="10%" class="text-center">' + value.id + '</td>';
                rows = rows + '<td width="20%" class="font-w600">' + value.institute_type_title + '</td>';
                rows = rows + '<td width="25%" class="d-none d-sm-table-cell">' + value.institute_type_desc + '</td>';

                if (value.visibility_status == 'Yes') {
                    rows = rows + '<td width="10%" class="text-center d-none d-sm-table-cell"><span class="badge badge-success">' + value.visibility_status + '</span></td>';

                }

                else {
                    rows = rows + '<td width="10%" class="text-center d-none d-sm-table-cell"><span class="badge badge-danger">' + value.visibility_status + '</span></td>';
                }
                rows = rows + '<td width="20%" data-id="' + value.id + '">';
                rows = rows + '<button data-toggle="modal"  data-target="#edit-item" class="btn btn-primary btn-sm edit-item"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button> ';

                rows = rows + '<a href="#" class="btn btn-danger btn-sm" id="confirm-delete" data-id="' + value.id + '" data-toggle="modal" data-target="#ConfirmDelete" >' +
                        '<i class="fa fa-trash-o" aria-hidden="true"></i> Delete</a>';
                rows = rows + '</td>';
                rows = rows + '</tr>';
            });

            $("tbody").html(rows);
        }


        /*
         * END of Pagination Functions
         */





        /*
         * Create new data Functions
         */


        $(".crud-submit").click(function (e) {
            e.preventDefault();
            var form_action = $("#create-type").find("form").attr("action");

            var institute_type_title = $("#title").val();
            var institute_type_desc = $("#description").val();
            var visibility_status = $("#status").val();

            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: form_action,
                data: {
                    institute_type_title: institute_type_title,
                    institute_type_desc: institute_type_desc,
                    visibility_status: visibility_status
                }
            }).done(function (data) {
                getPageData();
                swal(
                        'Good job!',
                        'Institute Type Created Successfully!',
                        'success'
                );
            });

            $('form')[0].reset();

        });

        /*
         * END of Create new data Functions
         */



        /*
         * Remove Data functions
         */


        $(document).ready(function () {
            $("body").on("click", "#confirm-delete", function () {

                var id = $(this).parent("td").data('id');


                $('#data_id').on('click', function (e) {
                    e.preventDefault();

                    $.ajax({
                        url: "{{ route('institute-type.delete') }}",
                        data: {id: id},
                        method: 'post',
                        success: function (data) {
                            $(".modal").modal('hide');
                            swal(
                                    'Good job!',
                                    'Institute Type Deleted Successfully!',
                                    'success'
                            );
                            getPageData();
                        }

                    });
                });


            });
        });


        /*
         * End of Remove Data functions
         */



        /*
         * Edit & Update Data Functions
         *
         */

        $(document).ready(function () {
            $("body").on("click", ".edit-item", function () {
                var id = $(this).parent("td").data('id');
                var title = $(this).parent("td").prev("td").prev("td").prev("td").text();
                var description = $(this).parent("td").prev("td").prev("td").text();
                var status = $(this).parent("td").prev("td").text();

                $('#update_title').val(title);
                $('#update_id').val(id);
                $('textarea#update_desc').val(description);
                $("input[name='visibility_status'][value='" + status + "']").prop('checked', true);

            });
        });


        /* Updated new Item */
        $(".crud-submit-edit").click(function (e) {

            e.preventDefault();

            var form_action = $("#edit-item").find("form").attr("action");

            var institute_type_title = $("#update_title").val();
            var institute_type_desc = $("#update_desc").val();
            var visibility_status = $("#update_status:checked").val();
            var institute_type_id = $("#update_id").val();

            var dataString = 'institute_type_id=' + institute_type_id + '&institute_type_title='
                    + institute_type_title + '&institute_type_desc=' + institute_type_desc + '&visibility_status=' + visibility_status;

            console.log(dataString);

            $.ajax({

                type: 'post',
                url: form_action,
                data: dataString,
                async: false,
                success: function (data) {
                    getPageData();
                    $(".modal").modal('hide');
                    swal(
                            'Good job!',
                            'Institute Type Updated Successfully!',
                            'success'
                    );
                }
            });
        });

        /*
         * End of Edit & Update Data Functions
         *
         */
    </script>
@endsection