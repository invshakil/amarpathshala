@extends('backend.admin.master')

@section('title')
    {{ $page_title }}
@endsection

@section('main_content')

    @if (Session::has('sweet_alert.alert'))
        <script>
            swal({!! Session::get('sweet_alert.alert') !!});

        </script>
    @endif

    @if (count($errors) > 0)
        <div>
            <ul>
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">

        <div class="col-sm-12 col-lg-8">
            <div class="block">

                <div class="block-content">

                    <h2 class="content-heading">Add Institute Information </h2>
                    <br/>
                    <form data-toggle="validator" action="{{ route('institute.store') }}" method="post"
                          enctype="multipart/form-data"
                    >
                        {{ csrf_field() }}

                        <div class="form-group row">
                            <label class="col-12" for="example-text-input">Institute Name</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" id=""
                                       name="institute_name" placeholder="Text.."
                                       data-error="Please enter Institute Name." required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-12">Institute Logo</label>
                            <div class="col-12">
                                <label class="custom-file">
                                    <input type="file" class="custom-file-input" id=""
                                           name="logo" onchange="readURL(this);">

                                    <span class="custom-file-control"></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group row" style="padding-left: 15px;">
                            <img id="blah" src="http://placehold.it/620x348" style="max-height: 150px;"
                                 class="img-responsive img-thumbnail img-center" alt="your image"/>
                        </div>

                        <div class="form-group row">
                            <label class="col-12" for="example-select">Select Institute Type</label>
                            <div class="col-md-12">
                                <select class="form-control" id="" name="institute_type" required>
                                    <option >Please select</option>
                                    @php $types= DB::table('institute_types')->get(); @endphp
                                    @foreach($types as $type)
                                        <option value="{{ $type->id }}">{{ $type->institute_type_title }}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-12" for="example-textarea-input">Description & Information</label>
                            <div class="col-12">
                                <textarea type="text" class="form-control" id="" name="description"
                                          data-error="Please enter description." rows="6" placeholder="Description.." required></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-12" for="example-email-input">Website</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" id=""
                                       name="website" placeholder="Website.." data-error="Please enter Website Address." required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-12" for="example-email-input">Email</label>
                            <div class="col-md-12">
                                <input type="email" class="form-control" id=""
                                       name="email" placeholder="Email.." data-error="Please enter Email Information."
                                required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-12" for="example-email-input">Director/Institute Head</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" id="" value="N/A"
                                       name="director" placeholder="Director/Institute Head..">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-12" for="example-select">Select Country</label>
                            <div class="col-md-12">
                                <select class="form-control" id="country" name="country">


                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-12" for="example-select">Select Division/State</label>
                            <div class="col-md-12">
                                <select class="form-control" id="state" name="state">


                                </select>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-12" for="example-select">Select Area/District</label>
                            <div class="col-md-12">
                                <select class="form-control" id="area" name="area">


                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-12">Visibility Status</label>
                            <div class="col-12">
                                <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id=""
                                           name="status" value="Yes" checked>
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Yes</span>
                                </label>
                                <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="example-inline-radio2"
                                           name="status" value="No">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">No</span>
                                </label>
                            </div>
                        </div>


                        <div class="form-group">
                            <button type="submit" class="btn btn-alt-primary mr-5 mb-5">
                                <i class="fa fa-plus mr-5"></i> Add Institute Information
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

        <div class="col-sm-12 col-lg-4">
            <div class="block">
                <div class="block-content">
                    <div class="block">
                        <h2 class="content-heading">Add Country</h2>
                        <div class="block-content" id="add_country">

                            <form id="AddCountry" action="{{ route('country.store') }}" method="post">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="example-nf-email">Title</label>
                                    <input type="text" class="form-control" id="country_name"
                                           name="country_name" placeholder="Enter Country..">
                                </div>

                                <div class="form-group">
                                    <button type="submit" id="CountrySubmit" class="btn btn-alt-primary mr-5 mb-5">
                                        <i class="fa fa-plus mr-5"></i> Add Country
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Normal Form -->

                </div>
            </div>

            <div class="block">
                <div class="block-content">
                    <div class="block">
                        <h2 class="content-heading">Add Division/State</h2>

                        <div class="block-content" id="add_state">
                            <form id="AddState" action="{{ route('state.store') }}" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="example-nf-email">Division/State Name</label>
                                    <input type="text" class="form-control" id="state_name"
                                           name="state_name" placeholder="Enter Division..">
                                </div>
                                <div class="form-group row">
                                    <label class="col-12" for="example-select">Select Country</label>
                                    <div class="col-md-12">
                                        <select class="form-control" name="country" id="CountryForState">


                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" id="StateSubmit" class="btn btn-alt-primary mr-5 mb-5">
                                        <i class="fa fa-plus mr-5"></i> Add Division
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Normal Form -->

                </div>
            </div>

            <div class="block">
                <div class="block-content">
                    <div class="block">
                        <h2 class="content-heading">Add District/Area</h2>
                        <div class="block-content" id="add_area">


                            <form id="AddArea" action="{{ route('area.store') }}" method="post">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="example-nf-email">Area/District Name</label>
                                    <input type="text" class="form-control" id="area_name"
                                           name="area_name" placeholder="Enter District..">
                                </div>
                                <div class="form-group row">
                                    <label class="col-12" for="example-select">Select Country</label>
                                    <div class="col-md-12">
                                        <select class="form-control" id="CountryForArea" name="country">

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-12" for="example-select">Select Division/State</label>
                                    <div class="col-md-12">
                                        <select class="form-control" id="state_list" name="state">
                                            <option value="0">Please Select State</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" id="AreaSubmit" class="btn btn-alt-primary mr-5 mb-5">
                                        <i class="fa fa-plus mr-5"></i> Add District
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Normal Form -->

                </div>
            </div>
        </div>
    </div>

    <script>
        // Calling Country List
        fetchAllCountries();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('input[name="_token"]').val()
            }
        });

        /*
         * Create Country Functions
         */


        $("#CountrySubmit").click(function (e) {
            e.preventDefault();

            var form_action = $("#add_country").find("form").attr("action");
            var country_name = $("#country_name").val();

            $.ajax({
                dataGroup: 'json',
                type: 'POST',
                url: form_action,
                data: {
                    country_name: country_name
                }
            }).done(function (data) {
                swal(
                        'Good job!',
                        'Country Created Successfully!',
                        'success'
                );
                fetchAllCountries();
            });

            $('#AddCountry')[0].reset();

        });

        /*
         * END of Create Country Functions
         */


        /*
         * CREATE NEW STATE FUNCTION
         */

        $("#StateSubmit").click(function (e) {
            e.preventDefault();

            var form_action = $("#add_state").find("form").attr("action");

            var state_name = $("#state_name").val();
            var country = $("#CountryForState").val();

            $.ajax({
                dataGroup: 'json',
                type: 'POST',
                url: form_action,
                data: {

                    state_name: state_name,
                    country: country
                }
            }).done(function (data) {
                swal(
                        'Good job!',
                        'District/Area Created Successfully!',
                        'success'
                );
            });

            $('#AddState')[0].reset();

        });

        /*
         * FETCHING COUNTRY LIST
         */


        function fetchAllCountries() {
            $.ajax({
                url: '{{ route('countries.fetch') }}',
                type: "GET",
                dataType: "json",
                success: function (data) {

                    //For State Creation
                    $('#CountryForState').empty();
                    $('#CountryForArea').empty();
                    $('#Country').empty();

                    //For Area Creation
                    $('#CountryForArea').append('<option value="0">Please Select Country</option>');

                    $.each(data, function (key, value) {
                        //For Institute Creation
                        $('#country').append('<option value="' + value.country_id + '">' + value.country_name + '</option>');

                        //For State Creation
                        $('#CountryForState').append('<option value="' + value.country_id + '">' + value.country_name + '</option>');

                        //For Area Creation
                        $('#CountryForArea').append('<option value="' + value.country_id + '">' + value.country_name + '</option>');
                    });
                }
            });
        }

        /*
         * FETCHING STATE LIST BASED ON COUNTRY
         */

        $('#CountryForArea, #country').on('change', function () {
            var country = $(this).val();

            var url = '{{ route("states.fetch.byId", ":id") }}';
            url = url.replace(':id', country);

            if (country != '') {
                $.ajax({
                    url: url,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        $('#state_list').empty();
                        $('#state').empty();

                        $('#state_list, #state').append('<option value="0" selected>Please Select State</option>');

                        $.each(data, function (key, value) {
                            //For Area Creation
                            $('#state_list').append('<option value="' + value.state_id + '">' + value.state_name + '</option>');

                            //For Institute Creation
                            $('#state').append('<option value="' + value.state_id + '">' + value.state_name + '</option>');
                        });
                    }
                });
            }

            else {
                $('#CountryForArea').empty();
            }
        });


        /*
         * CREATE NEW AREA FUNCTION
         */

        $("#AreaSubmit").click(function (e) {
            e.preventDefault();

            var form_action = $("#add_area").find("form").attr("action");
            var area_name = $("#area_name").val();
            var country = $("#CountryForArea").val();
            var state = $("#state_list").val();


            $.ajax({
                dataGroup: 'json',
                type: 'POST',
                url: form_action,
                data: {
                    area_name: area_name,
                    state: state,
                    country: country
                }
            }).done(function (data) {
                swal(
                        'Good job!',
                        'Division/State Created Successfully!',
                        'success'
                );
            });

            $('#AddArea')[0].reset();

        });


        /*
         * FETCHING AREA LIST BASED ON COUNTRY
         */

        $('#state').on('change', function () {
            var state = $(this).val();
            console.log(state);

            var url = '{{ route("areas.fetch.byId", ":id") }}';
            url = url.replace(':id', state);


            if (state != '') {
                $.ajax({
                    url: url,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {

                        $('#area').empty();

                        $('#area').append('<option value="0" selected>Please Select Area</option>');

                        $.each(data, function (key, value) {

                            //For Institute Creation
                            $('#area').append('<option value="' + value.area_id + '">' + value.area_name + '</option>');
                        });
                    }
                });
            }

            else {
                $('#CountryForArea').empty();
            }
        });

    </script>


    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                            .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>



@endsection