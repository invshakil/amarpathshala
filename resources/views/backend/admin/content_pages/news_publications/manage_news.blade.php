@extends('backend.admin.master')

@section('title')
    {{ $page_title }}
@endsection

@section('main_content')

    @if (Session::has('sweet_alert.alert'))
        <script>
            swal({!! Session::get('sweet_alert.alert') !!});

        </script>
    @endif

    @if (count($errors) > 0)
        <div>
            <ul>
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                @endforeach
            </ul>
        </div>
    @endif
    <!-- MANAGE Institute -->

    <div class="block">
        <div class="block-header block-header-default">
            <h3></h3>
            <div class="block-options">
                <button type="button" class="btn-block-option" data-toggle="block-option"
                        data-action="fullscreen_toggle"></button>
                <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle"
                        data-action-mode="demo">
                    <i class="si si-refresh"></i>
                </button>
                <button type="button" class="btn-block-option" data-toggle="block-option"
                        data-action="content_toggle"></button>
            </div>
        </div>
        <div class="block-content">
            <h2 class="content-heading">Manage Institute</h2>

            {{-- SEARCH --}}

            <div class="block pull-r-l">
                <div class="block-content block-content-full block-content-sm bg-body-light">
                    <form action="{{ route('admin.search.news') }}" method="get">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search"
                                   placeholder="Search..">
                            <span class="input-group-btn">
                                            <button type="submit" class="btn btn-secondary px-10">
                                                <i class="fa fa-search"></i>
                                            </button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>

            <table class="table table-bordered table-striped table-vcenter">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th class="text-center">Cover</th>
                    <th>Title</th>
                    <th class="d-none d-sm-table-cell">News Type</th>
                    <th class="d-none text-center d-sm-table-cell" style="width: 20%;">Institute</th>
                    <th class="d-none text-center d-sm-table-cell">Last Updated</th>
                    <th class="d-none text-center d-sm-table-cell" style="width: 10%;">Status</th>
                    <th class="text-center" style="width: 15%;">Actions</th>
                </tr>
                </thead>
                <tbody>
                

                @foreach($articles as $row)
                    <tr>
                        <td class="text-center">{{ $row->article_id }}</td>

                        <td class="text-center"><img src="{{ asset($row->tiny_thumb) }}" class="img-thumbnail"
                                                     width="100"
                                                     alt=""></td>

                        <td class="font-w600" width="25%">
                            <a target="_blank" href="{{route('news.details',['slug'=>$row->url_slug])}}">{{ $row->article_title }}</a>
                        </td>

                        <td class="d-none d-sm-table-cell">
                            @php
                                $news_type = $row->news_type;
                                echo DB::table('news_types')->where('id',$news_type)->value('news_type_title');
                            @endphp
                        </td>
                        <td class="d-none d-sm-table-cell">
                            @php
                                $institute = $row->institute;
                                echo DB::table('institutes')->where('institute_id',$institute)->value('institute_name');
                            @endphp
                        </td>
                        <td class="font-w600 d-sm-table-cell text-center" width="15%">{{ date('d/M h:i A', strtotime($row->updated_at)) }}</td>
                        <td class="d-none d-sm-table-cell text-center" width="5%">
                            @if($row->status == 'Yes')
                                <span class="badge badge-success">{{ $row->status }}</span>
                            @else
                                <span class="badge badge-danger">{{ $row->status }}</span>
                            @endif
                        </td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="{{ route('news.edit', $row->article_id) }}" class="btn btn-sm btn-info" data-toggle="tooltip"
                                    title="Edit">
                                    <i class="fa fa-pencil-square-o"></i> Edit
                                </a>
                                <a href="#" data-toggle="modal" data-id="{{ $row->article_id }}"
                                   data-target="#confirm-delete" class="btn btn-sm btn-danger"
                                   title="Delete">
                                    <i class="fa fa-trash-o"></i> Delete
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>

            @if(count($articles) > 0)

                <div class="alert alert-info">
                    Showing {{ $articles->firstItem() }} to {{ $articles->lastItem() }} entry of
                    total {{ $articles->total() }} entries.
                </div>

                <nav aria-label="Page navigation">
                    @if ($articles->lastPage() > 1)
                        <ul class="pagination pagination-sm">
                            <li class="page-item {{ ($articles->currentPage() == 1) ? ' disabled' : '' }}">
                                <a class="page-link" href="{{ $articles->url(1) }}">Previous</a>
                            </li>
                            @for ($i = 1; $i <= $articles->lastPage(); $i++)
                                <li class="page-item {{ ($articles->currentPage() == $i) ? 'page active' : '' }}">
                                    <a class="page-link" href="{{ $articles->url($i) }}">{{ $i }}</a>
                                </li>
                            @endfor
                            <li class="page-item {{ ($articles->currentPage() == $articles->lastPage()) ? ' disabled' : '' }}">
                                <a class="page-link"
                                   href="{{ $articles->url($articles->currentPage()+1) }}">Next</a>
                            </li>
                        </ul>
                    @endif
                </nav>
            @else
                <div class="alert alert-danger">
                    No Result Found!
                </div>
            @endif

        </div>
    </div>


    <!-- Delete Modal -->

    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="modal-popout"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-popout" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Are you sure?</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>

                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <p>Do you really want to delete these records? This process cannot be undone.</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <a id="id" class="btn btn-danger">
                        <i class="fa fa-check"></i> Yes, Delete this data
                    </a>
                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Delete Modal -->



    <script type="text/javascript">

        $('#confirm-delete').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id');

            var url = '{{ route("news.delete", ":id") }}';
            url = url.replace(':id', id);

            $('a#id').attr('href', url);
        });

    </script>

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                            .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection