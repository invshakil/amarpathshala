@extends('backend.admin.master')

@section('title')
    {{ $page_title }}
@endsection

@section('main_content')

    @if (Session::has('sweet_alert.alert'))
        <script>
            swal({!! Session::get('sweet_alert.alert') !!});

        </script>
    @endif

    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <script>
                swal(
                        'Oops!',
                        '{{ $error }}',
                        'error'
                );
            </script>
        @endforeach
    @endif

    @php
        $article_info = DB::table('articles')->where('article_id',$article_id)->first();

        $title = $article_info->article_title;
        $thumbnail_size = $article_info->thumbnail_size;
        $news_type = $article_info->news_type;
        $content = $article_info->content;
        $meta_description = $article_info->meta_description;
        $meta_keyword = $article_info->meta_keyword;
        $url_slug = $article_info->url_slug;
        $institute_type = $article_info->institute_type;
        $institute = $article_info->institute;
        $group = $article_info->group;
        $status = $article_info->status;
    @endphp


    <!-- New News -->

    <div class="block">
        <div class="block-header block-header-default">
            <h3></h3>
            <div class="block-options">
                <button type="button" class="btn-block-option" data-toggle="block-option"
                        data-action="fullscreen_toggle"></button>
                <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle"
                        data-action-mode="demo">
                    <i class="si si-refresh"></i>
                </button>
                <button type="button" class="btn-block-option" data-toggle="block-option"
                        data-action="content_toggle"></button>
            </div>
        </div>
        <div class="block-content">
            <h2 class="content-heading">Update News Information</h2>

            <form data-toggle="validator" action="{{ route('news.update') }}" method="post"
                  enctype="multipart/form-data">

                {{ csrf_field() }}

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="example-text-input">News Title</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="" value="{{ $title }}"
                               name="article_title" placeholder="Title.." data-error="Please enter title." required>

                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label">News Image</label>
                    <div class="col-md-3 col-form-label">
                        <label class="custom-file">
                            <input type="file" class="custom-file-input" id=""
                                   name="image" onchange="readURL(this);">

                            <span class="custom-file-control"></span>

                        </label>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-md-3">
                        <img id="blah" src="{{ asset($thumbnail_size) }}" style="max-height: 150px;"
                             class="img-responsive img-thumbnail img-center" alt="your image"/>
                    </div>
                </div>


                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="example-textarea-input">Description</label>
                    <div class="col-md-9">
                                <textarea type="text" class="form-control" id="js-ckeditor" name="content"
                                          rows="6" placeholder="Description.."
                                          data-error="Please enter Description." required>{!! $content !!}</textarea>

                        <div class="help-block with-errors"></div>
                    </div>
                </div>


                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="example-email-input">Meta Description</label>
                    <div class="col-md-9">
                        <textarea type="text" class="form-control" id="" cols="3"
                                  name="meta_description" placeholder="Meta Description.."
                                  data-error="Please enter Meta Description." required>{{$meta_description}}</textarea>

                        <div class="help-block with-errors"></div>
                    </div>
                </div>


                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="example-email-input">Meta Keyword</label>
                    <div class="col-md-9">
                        <textarea type="text" class="form-control" id="" cols="3"
                                  name="meta_keyword" placeholder="Meta Keyword.."
                                  data-error="Please enter Meta Keyword." required>{{$meta_keyword}}</textarea>

                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="example-email-input">URL Slug/Permalink</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="" value="{{$url_slug}}"
                               name="url_slug" placeholder="Url Slug/Permalink.."
                               data-error="Please enter URL Slug." required>

                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="example-select">Select News Type</label>
                    <div class="col-md-9">
                        <select class="form-control" id="" name="news_type" required>
                            <option>Please select</option>
                            @php $types = DB::table('news_types')->where('visibility_status','Yes')->get();@endphp
                            @foreach($types as $type)
                                <option value="{{ $type->id }}"
                                @if($news_type == $type->id) selected @endif> {{ $type->news_type_title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="example-select">Select Institute Type</label>
                    <div class="col-md-9">
                        <select class="form-control" id="institute_type" name="institute_type">
                            <option>Please select</option>
                            @php $i_types = DB::table('institute_types')->where('visibility_status','Yes')->get();@endphp
                            @foreach($i_types as $type)
                                <option value="{{ $type->id }}"
                                        @if($institute_type == $type->id) selected @endif> {{ $type->institute_type_title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="example-select">Select Institute</label>
                    <div class="col-md-9">
                        <select class="form-control" id="institute" name="institute">

                            @php $ins = DB::table('institutes')->where('status','Yes')->get();@endphp
                            @foreach($ins as $i)
                                <option value="{{ $i->institute_id }}"
                                        @if($institute == $i->institute_id) selected @endif> {{ $i->institute_name }}</option>
                            @endforeach

                        </select>
                    </div>
                </div>


                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="example-select">Select Group Name</label>
                    <div class="col-md-9">
                        <select class="form-control" id="" name="group">
                            <option value="0">Please select</option>
                            @php $groups = DB::table('institute_groups')->where('visibility_status','Yes')->get();@endphp
                            @foreach($groups as $g)
                                <option value="{{ $g->id }}"
                                        @if($group == $g->id) selected @endif> {{ $g->institute_group_title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label">Visibility Status</label>
                    <div class="col-md-3 col-form-label">
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id=""
                                   name="status" value="Yes" @if($status == 'Yes') checked @endif>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Yes</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="example-inline-radio2"
                                   name="status" value="No" @if($status == 'No') checked @endif>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">No</span>
                        </label>
                    </div>
                </div>

                <input type="hidden" name="article_id" value="{{ $article_id }}">

                <div class="form-group row">

                    <div class="col-md-9 ml-auto">
                        <button type="submit" class="btn btn-alt-primary mr-5 mb-5">
                            <i class="fa fa-plus mr-5"></i> Update News Information
                        </button>
                    </div>
                </div>

            </form>

        </div>
    </div>

    <script src="{{ asset('assets/') }}/js/plugins/ckeditor/ckeditor.js"></script>
    <!-- Page JS Code -->
    <script>
        jQuery(function () {
            // Init page helpers (CKEditor + SimpleMDE plugins)
            Codebase.helpers(['ckeditor']);
        });
    </script>

    <style>
        img {
            max-width: 100%;
        }

        input[type=file] {
            padding: 10px;
            background: #2d2d2d;
        }

        .content-heading {
            margin: 0 auto !important;
            margin-bottom: 40px !important;
        }

    </style>

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                            .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

    <!-- Page JS Code -->
    <script>

        $(document).ready(function () {
            $('#institute_type').on('change', function () {
                var type = $(this).val();
                console.log(type);

                var url = '{{ route("institute.fetch.byTypeId", ":id") }}';
                url = url.replace(':id', type);


                if (type != '') {
                    $.ajax({
                        url: url,
                        type: "GET",
                        dataType: "json",
                        success: function (data) {

                            if (data.length > 0) {
                                $('#institute').empty().append('<option selected>Please Select Institute</option>');

                                $.each(data, function (key, value) {

                                    //For Institute Creation
                                    $('#institute').append('<option value="' + value.institute_id + '">' + value.institute_name + '</option>');
                                });
                            }
                            else {
                                swal(
                                        'Oops!',
                                        'No Institute found for this type, Try new type!',
                                        'error'
                                );
                                $('#institute').empty();
                            }

                        }
                    });
                }

                else {
                    $('#institute').empty();
                }
            });
        });

    </script>

@endsection