@extends('backend.admin.master')

@section('title')
    {{ $page_title }}
@endsection

@section('main_content')

    <div class="row">


        <div class="col-sm-12 col-lg-12">

            <!-- MANAGE NEWS -->

            <div class="block">
                <h2 class="content-heading">Manage Institute Type </h2>

                <div class="block-content">

                    {{-- SEARCH --}}

                    <div class="block pull-r-l">
                        <div class="block-content block-content-full block-content-sm bg-body-light">
                            <form action="{{ route('admin.search.institute.type') }}" method="get">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="search"
                                           placeholder="Search..">
                                    <span class="input-group-btn">
                                            <button type="submit" class="btn btn-secondary px-10">
                                                <i class="fa fa-search"></i>
                                            </button>
                            </span>
                                </div>
                            </form>
                        </div>
                    </div>


                    <!-- DataTables init on table by adding .js-dataTable-full-Apagination class, functionality initialized in js/pages/be_tables_datatables.js -->
                    <table class="table table-bordered table-striped table-vcenter">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>Title</th>
                            <th class="d-none d-sm-table-cell">Description</th>
                            <th class="d-none text-center d-sm-table-cell" style="width: 20%;">Visibility Status</th>
                            <th class="text-center" style="width: 15%;">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($result as $row)
                            <tr>
                                <td width="10%" class="text-center">
                                    {{ $row->id }}
                                </td>

                                <td width="20%" class="font-w600">
                                    {{ $row->institute_type_title }}
                                </td>


                                <td width="25%" class="d-none d-sm-table-cell">
                                    {{ $row->institute_type_desc }}
                                </td>

                                <td width="10%" class="text-center d-none d-sm-table-cell">
                                    @if($row->visibility_status == 'Yes')
                                        <span class="badge badge-success"> Yes </span>
                                    @else
                                        <span class="badge badge-danger"> No </span>
                                    @endif
                                </td>


                                <td width="20%"
                                    data-id="{{ $row->id }}"
                                    data-title="{{ $row->institute_type_title }}"
                                    data-desc="{{ $row->institute_type_desc }}"
                                    data-status="{{ $row->visibility_status }}"
                                >
                                    <button data-toggle="modal" data-target="#edit-item"
                                            class="btn btn-primary btn-sm edit-item"><i class="fa fa-pencil-square-o"
                                                                                        aria-hidden="true"></i> Edit
                                    </button>

                                    <a href="#" class="btn btn-danger btn-sm" id="confirm-delete"
                                       data-id="{{ $row->id }}" data-toggle="modal" data-target="#ConfirmDelete">
                                        <i class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                    </a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    @if(count($result) > 0)
                        <div class="alert alert-info">
                            Showing {{ $result->firstItem() }} to {{ $result->lastItem() }} entry of
                            total {{ $result->total() }} entries.
                        </div>


                        <nav aria-label="Page navigation">
                            @if ($result->lastPage() > 1)
                                <ul class="pagination pagination-sm">
                                    <li class="page-item {{ ($result->currentPage() == 1) ? ' disabled' : '' }}">
                                        <a class="page-link" href="{{ $result->url(1) }}">Previous</a>
                                    </li>
                                    @for ($i = 1; $i <= $result->lastPage(); $i++)
                                        <li class="page-item {{ ($result->currentPage() == $i) ? 'page active' : '' }}">
                                            <a class="page-link" href="{{ $result->url($i) }}">{{ $i }}</a>
                                        </li>
                                    @endfor
                                    <li class="page-item {{ ($result->currentPage() == $result->lastPage()) ? ' disabled' : '' }}">
                                        <a class="page-link"
                                           href="{{ $result->url($result->currentPage()+1) }}">Next</a>
                                    </li>
                                </ul>
                            @endif
                        </nav>
                    @else
                        <div class="alert alert-danger">
                            No Result Found!
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>



    <!--MODAL AREA -->

    <!-- Pop Out Modal -->

    <div class="modal fade" id="ConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="modal-popout"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-popout" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Are you sure?</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <p>Do you really want to delete these records? This process cannot be undone.</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="data_id" class="btn btn-danger" data-dismiss="modal">
                        <i class="fa fa-check"></i> Yes, Delete this data
                    </button>
                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Pop Out Modal -->


    <!-- Edit Item Modal -->
    <div class="modal fade" id="edit-item" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Item</h4>
                </div>
                <div class="modal-body">

                    <form data-toggle="validator" action="{{ route('institute-type.edit') }}">

                        {{ csrf_field() }}

                        <input type="hidden" name="institute_type_id" value="" id="update_id">

                        <div class="form-group row">
                            <label class="col-12 col-form-label" for="example-hf-email">Institute Type Title</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="institute_type_title" id="update_title"
                                       placeholder="Enter Title" data-error="Please enter title." required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-12 col-form-label" for="example-hf-password">Institute Type
                                Description</label>
                            <div class="col-md-12">
                                    <textarea type="text" name="institute_type_desc" id="update_desc"
                                              class="form-control"
                                              placeholder="Description.." data-error="Please enter Description."
                                              required></textarea>
                                <div class="help-block with-errors"></div>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-12">Visibility Status</label>
                            <div class="col-md-12">
                                <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="update_status"
                                           name="visibility_status" value="Yes">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Yes</span>
                                </label>

                                <label class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="update_status"
                                           name="visibility_status" value="No">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">No</span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success crud-submit-edit">Submit</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <!--MODAL AREA -->



    <script>


        /*
         * Remove Data functions
         */


        $(document).ready(function () {
            $("body").on("click", "#confirm-delete", function () {

                var id = $(this).parent("td").data('id');


                $('#data_id').on('click', function (e) {
                    e.preventDefault();

                    $.ajax({
                        url: "{{ route('institute-type.delete') }}",
                        data: {id: id},
                        method: 'post',
                        success: function (data) {
                            $(".modal").modal('hide');
                            swal(
                                    'Good job!',
                                    'Institute Type Deleted Successfully!',
                                    'success'
                            );
                            getPageData();
                        }

                    });
                });


            });
        });


        /*
         * End of Remove Data functions
         */



        /*
         * Edit & Update Data Functions
         *
         */

        $(document).ready(function () {
            $("body").on("click", ".edit-item", function () {
                var id = $(this).parent("td").data('id');
                var title = $(this).parent("td").data('title');
                var description = $(this).parent("td").data('desc');
                var status = $(this).parent("td").data('status');

                $('#update_title').val(title);
                $('#update_id').val(id);
                $('textarea#update_desc').val(description);
                $("input[name='visibility_status'][value='" + status + "']").prop('checked', true);

            });
        });


        /* Updated new Item */
        $(".crud-submit-edit").click(function (e) {

            e.preventDefault();

            var form_action = $("#edit-item").find("form").attr("action");

            var institute_type_title = $("#update_title").val();
            var institute_type_desc = $("#update_desc").val();
            var visibility_status = $("#update_status:checked").val();
            var institute_type_id = $("#update_id").val();

            var dataString = 'institute_type_id=' + institute_type_id + '&institute_type_title='
                    + institute_type_title + '&institute_type_desc=' + institute_type_desc + '&visibility_status=' + visibility_status;

            console.log(dataString);

            $.ajax({

                type: 'get',
                url: form_action,
                data: dataString,
                dataType: 'text',
                async: true,
                success: function (data) {

                    $(".modal").modal('hide');
                    swal(
                            'Good job!',
                            'Institute Type Updated Successfully!',
                            'success'
                    );

                    window.location.href = "{{ route('admin.institute.types') }}";
                }
            });
        });

        /*
         * End of Edit & Update Data Functions
         *
         */
    </script>
@endsection