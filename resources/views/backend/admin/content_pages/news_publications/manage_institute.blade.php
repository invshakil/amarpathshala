@extends('backend.admin.master')

@section('title')
    {{ $page_title }}
@endsection

@section('main_content')

    @if (Session::has('sweet_alert.alert'))
        <script>
            swal({!! Session::get('sweet_alert.alert') !!});

        </script>
    @endif

    @if (count($errors) > 0)
        <div>
            <ul>
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                @endforeach
            </ul>
        </div>
    @endif
    <!-- MANAGE Institute -->

    <div class="block">
        <div class="block-header block-header-default">
            <h3></h3>
            <div class="block-options">
                <button type="button" class="btn-block-option" data-toggle="block-option"
                        data-action="fullscreen_toggle"></button>
                <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle"
                        data-action-mode="demo">
                    <i class="si si-refresh"></i>
                </button>
                <button type="button" class="btn-block-option" data-toggle="block-option"
                        data-action="content_toggle"></button>
            </div>
        </div>
        <div class="block-content">
            <h2 class="content-heading">Manage Institute</h2>

            {{-- SEARCH --}}

            <div class="block pull-r-l">
                <div class="block-content block-content-full block-content-sm bg-body-light">
                    <form action="{{ route('admin.search.institute') }}" method="get">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search"
                                   placeholder="Search..">
                            <span class="input-group-btn">
                                            <button type="submit" class="btn btn-secondary px-10">
                                                <i class="fa fa-search"></i>
                                            </button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>

            <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality initialized in js/pages/be_tables_datatables.js -->
            <table class="table table-bordered table-striped table-vcenter">
                <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th class="text-center">Logo</th>
                    <th>Name</th>
                    <th class="d-none d-sm-table-cell">Contact Details</th>
                    <th class="d-none text-center d-sm-table-cell" style="width: 20%;">Visibility Status</th>
                    <th class="d-none text-center d-sm-table-cell" style="width: 20%;">View Profile</th>
                    <th class="text-center" style="width: 15%;">Actions</th>
                </tr>
                </thead>
                <tbody>
                @php

                        @endphp

                @foreach($institutes as $row)
                    <tr>
                        <td class="text-center">{{ $row->institute_id }}</td>

                        <td class="text-center"><img src="{{ asset($row->logo) }}" class="img-thumbnail" width="100"
                                                     alt=""></td>

                        <td class="font-w600">{{ $row->institute_name }}</td>

                        <td class="d-none d-sm-table-cell">
                            {!! $row->description !!}
                        </td>
                        <td class="d-none d-sm-table-cell text-center">
                            @if($row->status == 'Yes')
                                <span class="badge badge-success">{{ $row->status }}</span>
                            @else
                                <span class="badge badge-danger">{{ $row->status }}</span>
                            @endif
                        </td>
                        <td class="font-w600 text-center">
                            <a class="btn btn-sm btn-primary mr-5" href="#" data-toggle="tooltip" title="Profile">
                                <b style="color: white"> <i class="fa fa-picture-o mr-5"></i> View Profile</b>
                            </a>
                        </td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="#" class="btn btn-sm btn-info" data-toggle="modal"

                                   data-id="{{$row->institute_id}}"
                                   data-name="{{$row->institute_name}}"
                                   data-logo="{{$row->logo}}"
                                   data-type="{{$row->type}}"
                                   data-description="{{$row->description}}"
                                   data-website="{{$row->website}}"
                                   data-email="{{$row->email}}"
                                   data-director="{{$row->director}}"
                                   data-country="{{$row->country}}"
                                   data-state="{{$row->state}}"
                                   data-area="{{$row->area}}"
                                   data-status="{{$row->status}}"

                                   data-target="#edit-form" title="Edit">
                                    <i class="fa fa-pencil-square-o"></i> Edit
                                </a>
                                <a href="#" data-toggle="modal" data-id="{{ $row->institute_id }}"
                                   data-target="#confirm-delete" class="btn btn-sm btn-danger"
                                   title="Delete">
                                    <i class="fa fa-trash-o"></i> Delete
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>


            @if(count($institutes) > 0)

                <div class="alert alert-info">
                    Showing {{ $institutes->firstItem() }} to {{ $institutes->lastItem() }} entry of
                    total {{ $institutes->total() }} entries.
                </div>

                <nav aria-label="Page navigation">
                    @if ($institutes->lastPage() > 1)
                        <ul class="pagination pagination-sm">
                            <li class="page-item {{ ($institutes->currentPage() == 1) ? ' disabled' : '' }}">
                                <a class="page-link" href="{{ $institutes->url(1) }}">Previous</a>
                            </li>
                            @for ($i = 1; $i <= $institutes->lastPage(); $i++)
                                <li class="page-item {{ ($institutes->currentPage() == $i) ? 'page active' : '' }}">
                                    <a class="page-link" href="{{ $institutes->url($i) }}">{{ $i }}</a>
                                </li>
                            @endfor
                            <li class="page-item {{ ($institutes->currentPage() == $institutes->lastPage()) ? ' disabled' : '' }}">
                                <a class="page-link"
                                   href="{{ $institutes->url($institutes->currentPage()+1) }}">Next</a>
                            </li>
                        </ul>
                    @endif
                </nav>
            @else
                <div class="alert alert-danger">
                    No Result Found!
                </div>
            @endif

        </div>
    </div>


    <!-- Delete Modal -->

    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="modal-popout"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-popout" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Are you sure?</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>

                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <p>Do you really want to delete these records? This process cannot be undone.</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <a id="id" class="btn btn-danger">
                        <i class="fa fa-check"></i> Yes, Delete this data
                    </a>
                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END Delete Modal -->



    <!-- Edit Modal -->

    <div class="modal fade" id="edit-form" tabindex="-1" role="dialog" aria-labelledby="modal-popout"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-popout" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block">

                        <div class="block-content">

                            <h2 class="content-heading">Update Institute Information </h2>
                            <br/>
                            <form id="update" data-toggle="validator" method="post"
                                  enctype="multipart/form-data"
                            >
                                {{ csrf_field() }}

                                <div class="form-group row">
                                    <label class="col-12" for="example-text-input">Institute Name</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" id="updateName"
                                               name="institute_name" placeholder="Text.."
                                               data-error="Please enter Institute Name." required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-12">Institute Logo</label>
                                    <div class="col-12">
                                        <label class="custom-file">
                                            <input type="file" class="custom-file-input" id=""
                                                   name="logo" onchange="readURL(this);">

                                            <span class="custom-file-control"></span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group row" style="padding-left: 15px;">
                                    <img id="blah" src="http://placehold.it/620x348" style="max-height: 150px;"
                                         class="img-responsive img-thumbnail img-center" alt="your image"/>
                                </div>

                                <div class="form-group row">
                                    <label class="col-12" for="example-select">Select Institute Type</label>
                                    <div class="col-md-12">
                                        <select class="form-control" id="updateType" name="institute_type" required>
                                            <option>Please select</option>
                                            @php $types= DB::table('institute_types')->get(); @endphp
                                            @foreach($types as $type)
                                                <option value="{{ $type->id }}">{{ $type->institute_type_title }}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-12" for="example-textarea-input">Description & Information</label>
                                    <div class="col-12">
                                <textarea type="text" class="form-control" id="updateDescription" name="description"
                                          data-error="Please enter description." rows="6" placeholder="Description.."
                                          required></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-12" for="example-email-input">Website</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" id="updateWebsite"
                                               name="website" placeholder="Website.."
                                               data-error="Please enter Website Address." required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-12" for="example-email-input">Email</label>
                                    <div class="col-md-12">
                                        <input type="email" class="form-control" id="updateEmail"
                                               name="email" placeholder="Email.."
                                               data-error="Please enter Email Information."
                                               required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-12" for="example-email-input">Director</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" id="updateDirector"
                                               name="director" placeholder="Title..">
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-12" for="example-select">Select Country</label>
                                    <div class="col-md-12">
                                        <select class="form-control" id="updateCountry" name="country">


                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-12" for="example-select">Select Division/State</label>
                                    <div class="col-md-12">
                                        <select class="form-control" id="updateState" name="state">

                                            @php $states = DB::table('states')->get(); @endphp
                                            @foreach($states as $state)
                                                <option value="{{ $state->state_id }}">{{ $state->state_name }}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-12" for="example-select">Select District</label>
                                    <div class="col-md-12">
                                        <select class="form-control" id="updateArea" name="area">

                                            @php $areas = DB::table('areas')->get(); @endphp
                                            @foreach($areas as $area)
                                                <option value="{{ $area->area_id }}">{{ $area->area_name }}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-12">Visibility Status</label>
                                    <div class="col-12">
                                        <label class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id=""
                                                   name="status" value="Yes" checked>
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Yes</span>
                                        </label>
                                        <label class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="example-inline-radio2"
                                                   name="status" value="No">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">No</span>
                                        </label>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <button type="submit" class="btn btn-alt-primary mr-5 mb-5">
                                        <i class="fa fa-plus mr-5"></i> Update Information
                                    </button>
                                </div>
                            </form>

                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- END Edit Modal -->


    <script type="text/javascript">

        $('#confirm-delete').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id');

            var url = '{{ route("institute.delete", ":id") }}';
            url = url.replace(':id', id);

            $('a#id').attr('href', url);
        });


        $('#edit-form').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id');
            var name = $(e.relatedTarget).data('name');
            var logo = $(e.relatedTarget).data('logo');
            var type = $(e.relatedTarget).data('type');
            var description = $(e.relatedTarget).data('description');
            var website = $(e.relatedTarget).data('website');
            var email = $(e.relatedTarget).data('email');
            var director = $(e.relatedTarget).data('director');
            var country = $(e.relatedTarget).data('country');
            var state = $(e.relatedTarget).data('state');
            var area = $(e.relatedTarget).data('area');
            var status = $(e.relatedTarget).data('status');

            logo = '{{ asset('/') }}' + '/' + logo;

            $('#updateId').val(id);
            $('#updateName').val(name);

            $('img#blah').attr('src', logo);

            $('select#updateType').val(type);
            $('#updateDescription').val(description);
            $('#updateEmail').val(email);
            $('#updateDirector').val(director);
            $('#updateWebsite').val(website);

            $('select#updateCountry').val(country);
            $('select#updateState').val(state);
            $('select#updateArea').val(area);

            var url = '{{ route("institute.update", ":id") }}';
            url = url.replace(':id', id);

            $('form#update').attr('action', url);
        });


        // Calling Country List
        fetchAllCountries();

        /*
         * FETCHING COUNTRY LIST
         */


        function fetchAllCountries() {
            $.ajax({
                url: '{{ route('countries.fetch') }}',
                type: "GET",
                dataType: "json",
                success: function (data) {

                    $('#updateCountry').append('<option value="0" selected>Please Select Country</option>');

                    $.each(data, function (key, value) {

                        $('#updateCountry').append('<option value="' + value.country_id + '">' + value.country_name + '</option>');
                    });
                }
            });
        }

        /*
         * FETCHING STATE LIST BASED ON COUNTRY
         */

        $('#updateCountry').on('change', function () {
            var country = $(this).val();

            var url = '{{ route("states.fetch.byId", ":id") }}';
            url = url.replace(':id', country);

            if (country != '') {
                $.ajax({
                    url: url,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {

                        $('#updateState').empty().append('<option value="0" selected>Please Select State</option>');

                        $('#updateArea').empty();

                        $.each(data, function (key, value) {

                            $('#updateState').append('<option value="' + value.state_id + '">' + value.state_name + '</option>');
                        });
                    }
                });
            }
            else {
                $('#updateState').empty();
            }
        });


        /*
         * FETCHING AREA LIST BASED ON COUNTRY
         */

        $('#updateState').on('change', function () {
            var state = $(this).val();
            console.log(state);

            var url = '{{ route("areas.fetch.byId", ":id") }}';
            url = url.replace(':id', state);


            if (state != '') {
                $.ajax({
                    url: url,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        console.log(data);

                        $('#updateArea').empty().append('<option value="0" selected>Please Select Area</option>');

                        $.each(data, function (key, value) {

                            //For Institute Creation
                            $('#updateArea').append('<option value="' + value.area_id + '">' + value.area_name + '</option>');
                        });
                    }
                });
            }

            else {
                $('#updateArea').empty();
            }
        });

    </script>

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                            .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection