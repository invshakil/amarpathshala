@extends('backend.admin.master')

@section('title')
    {{ $page_title }}
@endsection

@section('main_content')

    @if (Session::has('sweet_alert.alert'))
        <script>
            swal({!! Session::get('sweet_alert.alert') !!});
        </script>
    @endif

    @if (count($errors) > 0)
        @foreach ($errors->all() as $error)
            <script>
                swal(
                        'Oops!',
                        '{{ $error }}',
                        'error'
                );
            </script>
        @endforeach
    @endif

    <!-- New News -->

    <div class="block">
        <div class="block-header block-header-default">
            <h3></h3>
            <div class="block-options">
                <button type="button" class="btn-block-option" data-toggle="block-option"
                        data-action="fullscreen_toggle"></button>
                <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle"
                        data-action-mode="demo">
                    <i class="si si-refresh"></i>
                </button>
                <button type="button" class="btn-block-option" data-toggle="block-option"
                        data-action="content_toggle"></button>
            </div>
        </div>
        <div class="block-content items-push">
            <h2 class="content-heading">Write New News</h2>

            <form data-toggle="validator" action="{{ route('blog.store') }}" method="post"
                  enctype="multipart/form-data">

                {{ csrf_field() }}

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="example-text-input">Blog Title</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id=""
                               name="title" placeholder="Title.." data-error="Please enter title." required>

                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="example-select">Select Blog Category</label>
                    <div class="col-md-9">
                        <select class="form-control" id="" name="category" required>
                            <option>Please select</option>
                            @php $types = App\BlogCategory::where('status',1)->get(); @endphp

                            @foreach($types as $type)
                                <option value="{{ $type->id }}"> {{ $type->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label">Cover Image</label>
                    <div class="col-md-3 col-form-label">
                        <label class="custom-file">
                            <input type="file" class="custom-file-input" id=""
                                   name="image" onchange="readURL(this);" data-error="Please upload an image." required>

                            <span class="custom-file-control"></span>

                        </label>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="col-md-3">
                        <img id="blah" src="http://placehold.it/620x348" style="max-height: 150px;"
                             class="img-responsive img-thumbnail img-center" alt="your image"/>
                    </div>
                </div>


                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="example-textarea-input">Description</label>
                    <div class="col-md-9">
                                <textarea type="text" class="form-control" id="js-ckeditor" name="description"
                                          rows="6" placeholder="Description.."
                                          data-error="Please enter Description." required></textarea>

                        <div class="help-block with-errors"></div>
                    </div>
                </div>


                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="example-email-input">Meta Description</label>
                    <div class="col-md-9">
                        <textarea type="text" class="form-control" id="" cols="3"
                                  name="meta_description" placeholder="Meta Description.."
                                  data-error="Please enter Meta Description." required></textarea>

                        <div class="help-block with-errors"></div>
                    </div>
                </div>


                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="example-email-input">Meta Keyword</label>
                    <div class="col-md-9">
                        <textarea type="text" class="form-control" id="" cols="3"
                                  name="meta_keyword" placeholder="Meta Keyword.."
                                  data-error="Please enter Meta Keyword." required></textarea>

                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="example-email-input">URL Slug/Permalink</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id=""
                               name="url_slug" placeholder="Url Slug/Permalink.."
                               data-error="Please enter URL Slug." required>

                        <div class="help-block with-errors"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label">Status</label>
                    <div class="col-md-3 col-form-label">
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id=""
                                   name="status" value="1" checked>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Yes</span>
                        </label>
                        <label class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="example-inline-radio2"
                                   name="status" value="0">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">No</span>
                        </label>
                    </div>
                </div>

                <div class="form-group row ">
                    <label class="col-md-3" for="example-tags1">Tags</label>
                    <div class="col-md-9 ">
                        <input type="text" class="js-tags-input form-control"  id="example-tags3" name="tags" value="">
                    </div>
                </div>


                <div class="form-group row">

                    <div class="col-md-9 ml-auto">
                        <button type="submit" class="btn btn-alt-primary mr-5 mb-5">
                            <i class="fa fa-plus mr-5"></i> Add Blog Post
                        </button>
                    </div>
                </div>

            </form>

        </div>
    </div>

    <style>
        img {
            max-width: 100%;
        }

        input[type=file] {
            padding: 10px;
            background: #2d2d2d;
        }

        .content-heading {
            margin: 0 auto !important;
            margin-bottom: 40px !important;
        }

    </style>

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                            .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>


    <script src="{{ asset('assets/') }}/js/plugins/ckeditor/ckeditor.js"></script>
    <!-- Page JS Code -->
    <script>
        jQuery(function () {
            // Init page helpers (CKEditor + SimpleMDE plugins)
            Codebase.helpers(['ckeditor']);
        });
    </script>


    <link rel="stylesheet" href="{{ asset('/assets/') }}/js/plugins/jquery-tags-input/jquery.tagsinput.css">
    <script src="{{ asset('/assets/') }}/js/plugins/jquery-tags-input/jquery.tagsinput.min.js"></script>

    <script>
        jQuery(function () {
            Codebase.helpers(['tags-inputs']);
        });
    </script>
@endsection