@extends('backend.admin.navigation.adminNav.default')

@section('nav')

    <li>
        <a @if(Request::url() == route('blog.category.index')) class="active" @endif
        href="{{ route('blog.category.index') }}"><i class="fa fa-tags"></i> Blog Categories</a>
    </li>
    <li>
        <a @if(Request::url() == route('create.blog.index')) class="active" @endif
        href="{{ route('create.blog.index') }}"><i class="fa fa-tags"></i> Create Blog</a>
    </li>
    <li>
        <a @if(Request::url() == route('admin.manage.blog')) class="active" @endif
        href="{{ route('admin.manage.blog') }}"><i class="fa fa-tags"></i> Manage Blogs</a>
    </li>
    <li>
        <a @if(Request::url() == route('admin.pending.blog')) class="active" @endif
        href="{{ route('admin.pending.blog') }}"><i class="fa fa-tags"></i> Pending Blogs</a>
    </li>
    <li>
        <a @if(Request::url() == route('admin.blog.by.user')) class="active" @endif
        href="{{ route('admin.blog.by.user',['author'=>auth()->user()->id, 'name'=>auth()->user()->name]) }}"><i class="fa fa-tags"></i> My Blogs</a>
    </li>
@endsection