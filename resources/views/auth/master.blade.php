<!doctype html>
<html lang="{{ app()->getLocale() }}" class="no-focus">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>@yield('title') || Amarpathshala</title>

    <meta name="description"
          content="">
    <meta name="author" content="Shakil">
    <meta name="robots" content="">

    <!-- Open Graph Meta -->
    <meta property="og:title" content="">
    <meta property="og:site_name" content="">
    <meta property="og:description"
          content="">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="{{ asset('') }}/assets/img/favicons/favicon.png">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('') }}/assets/img/favicons/favicon-192x192.png">
    <link rel="apple-touch-icon" sizes="180x180"
          href="{{ asset('') }}/assets/img/favicons/apple-touch-icon-180x180.png">
    <!-- END Icons -->

    <!-- Stylesheets -->
    <!-- Codebase framework -->
    <link rel="stylesheet" id="css-main" href="{{ asset('') }}/assets/css/codebase.min.css">

    <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
    <!-- <link rel="stylesheet" id="css-theme" href="/assets/css/themes/flat.min.css"> -->
    <!-- END Stylesheets -->

    <style>
        .text-primary-dark {
            color: #0A5742!important;
        }

        .font-size-xl {
            font-size: 1.428571rem!important;
            color: red;
        }
    </style>
</head>
<body>
<!-- Page Container -->

<div id="page-container" class="main-content-boxed">
    <!-- Main Container -->
    <main id="main-container">
        <!-- Page Content -->
        <div class="bg-body-dark bg-pattern"
             style="background-image: url('{{ asset('') }}/assets/img/various/bg-pattern-inverse.png');">
            <div class="row mx-0 justify-content-center">
                <div class="hero-static col-lg-6 col-xl-4">
                    @yield('content')
                </div>
            </div>
        </div>
        <!-- END Page Content -->
    </main>
    <!-- END Main Container -->
</div>
<!-- END Page Container -->

<!-- Codebase Core JS -->
<script src="{{ asset('') }}/assets/js/core/jquery.min.js"></script>
<script src="{{ asset('') }}/assets/js/core/popper.min.js"></script>
<script src="{{ asset('') }}/assets/js/core/bootstrap.min.js"></script>
<script src="{{ asset('') }}/assets/js/core/jquery.slimscroll.min.js"></script>
<script src="{{ asset('') }}/assets/js/core/jquery.scrollLock.min.js"></script>
<script src="{{ asset('') }}/assets/js/core/jquery.appear.min.js"></script>
<script src="{{ asset('') }}/assets/js/core/jquery.countTo.min.js"></script>
<script src="{{ asset('') }}/assets/js/core/js.cookie.min.js"></script>
<script src="{{ asset('') }}/assets/js/codebase.js"></script>

<!-- Page JS Plugins -->
<script src="{{ asset('') }}/assets/js/plugins/jquery-validation/jquery.validate.min.js"></script>

@yield('js')

</body>
</html>