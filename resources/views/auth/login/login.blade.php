@extends('auth.master')

@section('title')
    Sign In
@endsection

@section('content')

    <div class="content content-full overflow-hidden">
        <!-- Header -->
        <div class="py-30 text-center">
            <a class="link-effect font-w700" href="{{ route('/') }}">
                <i class="fa fa-graduation-cap"></i>
                <span class="font-size-xl text-primary-dark">Amar</span><span
                        class="font-size-xl">Pathshala</span>
            </a>
            <h1 class="h4 font-w700 mt-30 mb-10">Sign In to your Dashboard</h1>
            <h2 class="h5 font-w400 text-muted mb-0">Enter your secret credentials!</h2>
        </div>
        <!-- END Header -->

        <!-- Sign In Form -->
        <!-- jQuery Validation (.js-validation-signin class is initialized in js/pages/op_auth_signin.js) -->
        <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
        <form class="js-validation-signin" action="{{ route('login') }}" method="post">
            {!! csrf_field() !!}
            <div class="block block-themed block-rounded block-shadow">
                <div class="block-header bg-gd-dusk">
                    <h3 class="block-title">Please Sign In</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option">
                            <i class="si si-wrench"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">

                    <div class="form-group row {{ $errors->has('email') ? ' is-invalid' : '' }}">
                        <div class="col-12">
                            <label for="login-username">Email</label>
                            <input type="email" class="form-control" id="email"
                                   value="{{ old('email') }}" name="email" required autofocus>

                            @if ($errors->has('email'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('email') }}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row {{ $errors->has('password') ? ' is-invalid' : '' }}">
                        <div class="col-12">
                            <label for="login-password">Password</label>
                            <input type="password" class="form-control" id="password"
                                   name="password" required>
                            @if ($errors->has('password'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('password') }}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-sm-6 d-sm-flex align-items-center push">
                            <label class="custom-control custom-checkbox mr-auto ml-0 mb-0">
                                <input type="checkbox" class="custom-control-input"
                                       {{ old('remember') ? 'checked' : '' }}
                                       id="login-remember-me" name="remember">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Remember Me</span>
                            </label>
                        </div>
                        <div class="col-sm-6 text-sm-right push">
                            <button type="submit" class="btn btn-alt-primary">
                                <i class="si si-login mr-10"></i> Sign In
                            </button>
                        </div>
                    </div>
                </div>
                <div class="block-content bg-body-light">
                    <div class="form-group text-center">
                        <a class="link-effect text-muted mr-10 mb-5 d-inline-block"
                           href="{{ route('register') }}">
                            <i class="fa fa-plus mr-5"></i> Create Account
                        </a>
                        <a class="link-effect text-muted mr-10 mb-5 d-inline-block"
                           href="{{ route('password.request') }}">
                            <i class="fa fa-warning mr-5"></i> Forgot Password
                        </a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END Sign In Form -->
    </div>


@endsection

@section('js')
    <script src="{{ asset('') }}/assets/js/pages/op_auth_signin.js"></script>
@endsection