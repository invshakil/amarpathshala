@extends('auth.master')

@section('title')
    Reset Password
@endsection

@section('content')
    <div class="content content-full overflow-hidden">
        <!-- Header -->
        <div class="py-30 text-center">
            <a class="link-effect font-w700" href="{{ route('/') }}">
                <i class="fa fa-graduation-cap"></i>
                <span class="font-size-xl text-primary-dark">Amar</span><span class="font-size-xl">Pathshala</span>
            </a>
            <h1 class="h4 font-w700 mt-30 mb-10">Reset Password</h1>
            <h2 class="h5 font-w400 text-muted mb-0">Please enter your new Password</h2>
        </div>
        <!-- END Header -->

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

    <!-- Reminder Form -->
        <form class="js-validation-signup" action="{{ route('password.request') }}" method="post">
            {!! csrf_field() !!}

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="block block-themed block-rounded block-shadow">
                <div class="block-header bg-gd-primary">
                    <h3 class="block-title">Reset Password</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option">
                            <i class="si si-wrench"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <div class="form-group row {{ $errors->has('email') ? ' is-invalid' : '' }}">
                        <div class="col-12">
                            <label for="reminder-credential">User Email</label>
                            <input type="email" class="form-control" id="email" name="email"
                                   value="{{ $email or old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('email') }}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row {{ $errors->has('password') ? ' is-invalid' : '' }}">
                        <div class="col-12">
                            <label for="signup-password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" required
                                   placeholder="********">

                            @if ($errors->has('password'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('password') }}
                                </div>
                            @endif
                        </div>


                    </div>

                    <div class="form-group row {{ $errors->has('password-confirmation') ? ' is-invalid' : '' }}">
                        <div class="col-12">
                            <label for="signup-password-confirm">Password Confirmation</label>
                            <input type="password" class="form-control" id="password_confirmation" required
                                   name="password_confirmation" placeholder="********">

                            @if ($errors->has('password_confirmation'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('password_confirmation') }}
                                </div>
                            @endif
                        </div>


                    </div>

                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-alt-primary">
                            <i class="fa fa-asterisk mr-10"></i> Reset Password
                        </button>
                    </div>
                </div>
                <div class="block-content bg-body-light">
                    <div class="form-group text-center">
                        <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="{{ route('login') }}">
                            <i class="fa fa-user text-muted mr-5"></i> Sign In
                        </a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END Reminder Form -->
    </div>
@endsection

@section('js')
    <script src="{{ asset('') }}/assets/js/pages/op_auth_signup.js"></script>
@endsection