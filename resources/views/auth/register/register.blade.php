@extends('auth.master')

@section('title')
    User Registration
@endsection

@section('content')
    <div class="content content-full overflow-hidden">
        <!-- Header -->
        <div class="py-30 text-center">
            <a class="link-effect font-w700" href="{{ route('/') }}">
                <i class="fa fa-graduation-cap"></i>
                <span class="font-size-xl text-primary-dark">Amar</span><span class="font-size-xl">Pathshala</span>
            </a>
            <h1 class="h4 font-w700 mt-30 mb-10">Create New Account</h1>
            <h2 class="h5 font-w400 text-muted mb-0">We’re excited to have you on board!</h2>
        </div>
        <!-- END Header -->

        <!-- Sign Up Form -->
        <!-- jQuery Validation (.js-validation-signup class is initialized in js/pages/op_auth_signup.js) -->
        <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
        <form class="js-validation-signup" action="{{ route('register') }}" method="post">
            {!! csrf_field() !!}
            <div class="block block-themed block-rounded block-shadow">
                <div class="block-header bg-gd-emerald">
                    <h3 class="block-title">Please enter required information</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option">
                            <i class="si si-wrench"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">

                    <div class="form-group row {{ $errors->has('name') ? ' is-invalid' : '' }}">
                        <div class="col-12">
                            <label for="signup-username">Full Name</label>
                            <input type="text" class="form-control" id="name" name="name"
                                   placeholder="eg: John Smith">

                            @if ($errors->has('name'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('name') }}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row {{ $errors->has('email') ? ' is-invalid' : '' }}">
                        <div class="col-12">
                            <label for="signup-email">Email</label>
                            <input type="email" class="form-control" id="email" name="email"
                                   placeholder="eg: john@example.com">

                            @if ($errors->has('email'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('email') }}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row {{ $errors->has('password') ? ' is-invalid' : '' }}">
                        <div class="col-12">
                            <label for="signup-password">Password</label>
                            <input type="password" class="form-control" id="password" name="password"
                                   placeholder="********">

                            @if ($errors->has('password'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('password') }}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row {{ $errors->has('password') ? ' is-invalid' : '' }}">
                        <div class="col-12">
                            <label for="signup-password-confirm">Password Confirmation</label>
                            <input type="password" class="form-control" id="password_confirmation"
                                   name="password_confirmation" placeholder="********">

                            @if ($errors->has('password'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('password') }}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row  {{ $errors->has('role') ? ' is-invalid' : '' }}">
                        <label class="col-4">Register As:</label>
                        <div class="col-8">
                            <label class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id=""
                                       name="role" value="Teacher">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Teacher</span>
                            </label>
                            <label class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="example-inline-radio2"
                                       name="role" value="Student">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Student</span>
                            </label>

                            @if ($errors->has('role'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('role') }}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-sm-6 push">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="terms"
                                       name="terms">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">I agree to Terms &amp; Conditions</span>
                            </label>
                        </div>
                        <div class="col-sm-6 text-sm-right push">
                            <button type="submit" class="btn btn-alt-success">
                                <i class="fa fa-plus mr-10"></i> Create Account
                            </button>
                        </div>
                    </div>
                </div>
                <div class="block-content bg-body-light">
                    <div class="form-group text-center">
                        <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="#" data-toggle="modal"
                           data-target="#modal-terms">
                            <i class="fa fa-book text-muted mr-5"></i> Read Terms
                        </a>
                        <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="{{ route('login') }}">
                            <i class="fa fa-user text-muted mr-5"></i> Sign In
                        </a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END Sign Up Form -->
    </div>


@endsection

@section('js')
    <script src="{{ asset('') }}/assets/js/pages/op_auth_signup.js"></script>
@endsection