@extends('frontend.master')

@section('title')
    {{ $title }}
@endsection

@section('content')

    <div class="sidebar-page-container change-padding">


        <!--Sidebar Page Container-->
        <div class="sidebar-page-container">
            <div class="auto-container">
                <div class="row clearfix">

                    <!--Content Side-->
                    <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">


                        <div class="col-md-12 col-xs-12">
                            <div class="sec-title">
                                <h2>Institute Type: {{ $name }}</h2>
                            </div>
                            <div class="content">
                                <div class="row clearfix">

                                @if(count($institutes) > 0)

                                    @foreach($institutes as $institute)

                                        <!--News Block Two-->
                                            <div class="news-block-two with-margin col-md-4 col-sm-6 col-xs-12">
                                                <div class="inner-box" id="divEffect" style="min-height: 400px">
                                                    <div class="image">
                                                        <a href="{{ route('institute.details',
                                                    ['slug'=> str_replace(' ','-', $institute->institute_name)]) }}">
                                                            <img style="max-height: 180px"
                                                                 class="img-responsive wow fadeIn" data-wow-delay="0ms"
                                                                 data-wow-duration="2500ms"
                                                                 src="{{ asset($institute->logo) }}"
                                                                 alt="{{ asset($institute->institute_name) }}"/>
                                                        </a>
                                                    </div>
                                                    <div class="lower-box">
                                                        <h3>
                                                            <a href="{{ route('institute.details',
                                                    ['slug'=> str_replace(' ','-', $institute->institute_name)]) }}">
                                                                {{ $institute->institute_name }}
                                                            </a>
                                                        </h3>
                                                        <ul class="post-meta">
                                                            <li><span class="icon fa fa-clock-o"></span>
                                                                {{ $institute->created_at->toFormattedDateString() }}
                                                            </li>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>

                                        @endforeach
                                    @else

                                        <h2 class="alert alert-danger">No Records Found! </h2>

                                    @endif


                                </div>
                                {{ $institutes->links() }}
                            </div>

                        </div>

                    </div>

                    <!--Sidebar Side-->
                    <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <aside class="sidebar default-sidebar right-sidebar">

                            <!--Category Widget-->
                            <div class="sidebar-widget categories-widget">
                                <div class="sidebar-title">
                                    <h2>Categories</h2>
                                </div>

                                <ul class="cat-list">

                                    @php $inst_types = App\InstituteType::all();
                                    @endphp
                                    @foreach($inst_types as $value)
                                        <li class="clearfix">
                                            <a href="{{ route('institute.type',['slug'=>str_replace(' ','-', $value->institute_type_title)]) }}">
                                                {{ $value->institute_type_title }}
                                            </a>
                                        </li>
                                    @endforeach

                                </ul>
                            </div>
                            <!--End Category Widget-->


                            <!--Adds Widget-->
                            <div class="sidebar-widget sidebar-adds-widget">
                                <div class="adds-block"
                                     style="background-image:url({{ asset('frontend') }}/images/resource/add-image.jpg);">
                                    <div class="inner-box">
                                        <div class="text">Advertisement <span> 340 x 283</span></div>
                                        <a href="#" class="theme-btn btn-style-two">Purchase Now</a>
                                    </div>
                                </div>
                            </div>
                            <!--Ends Adds Widget-->


                        </aside>
                    </div>


                </div>
            </div>


        </div>


    </div>

@endsection

@section('js')

@endsection