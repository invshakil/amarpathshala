@extends('frontend.master')

@section('title')
    {{ $title }}
@endsection

@section('content')

    <div class="sidebar-page-container change-padding">


        <!--Sidebar Page Container-->
        <div class="sidebar-page-container">
            <div class="auto-container">
                <div class="row clearfix">

                    <!--Content Side-->
                    <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">


                        <div class="content">
                            <div class="blog-single">
                                <div class="inner-box">
                                    <div class="upper-box">

                                        <div class="sec-title">
                                            <ul class="breadcrumb-bar" style="padding: 10px 0 2px 12px;">
                                                <li><a href="{{ route('/') }}">Home</a></li>
                                                <li>
                                                    <a href="{{ route('news.type',
                                        ['type'=>str_replace(' ','-',$article_details->getNewsTypeName())])
                                        }}">{{ $article_details->getNewsTypeName() }}</a>
                                                </li>
                                                <li>{{ $article_details->article_title }}</li>
                                            </ul>
                                        </div>


                                        <img src="{{ asset($article_details->cover) }}" width="100%"
                                             class="img-responsive" alt="">


                                        <h2> {{ $article_details->article_title }} </h2>
                                        <ul class="post-meta" style="margin-bottom: 0!important;">
                                            <li>
                                                <span class="icon qb-clock"></span>{{ $article_details->created_at->toFormattedDateString() }}
                                            </li>
                                            <li><span class="icon qb-user2"></span>ADMIN</li>
                                            <li>
                                                <span class="fa fa-tags"></span>
                                                <a href="{{ route('news.type',
                                        ['type'=>str_replace(' ','-',$article_details->getNewsTypeName())])
                                        }}">{{ $article_details->getNewsTypeName() }}</a>

                                            </li>
                                            <li><span class="icon qb-eye"></span>{{ $article_details->hit_count+1 }}
                                                Views
                                            </li>

                                        </ul>
                                    </div>

                                    <!--Rating Box-->


                                    <div class="text" style="text-align: justify">


                                        {!! $article_details->content !!}

                                    </div>

                                @if($article_details->institute_type != NULL)
                                    <!--Author Box-->
                                        <div class="author-box">
                                            <div class="author-comment">
                                                <div class="inner-box">
                                                    <div class="image">
                                                        <a href="{{ $article_details->instituteDetails()->website }}"
                                                           target="_blank">
                                                            <img src="{{ asset($article_details->instituteDetails()->logo) }}"
                                                                 alt=""/>
                                                        </a>
                                                    </div>
                                                    <h3>
                                                        <a target="_blank" href="{{ route('institute.details',
                                                    ['slug'=> str_replace(' ','-', $article_details->instituteDetails()->institute_name)]) }}">
                                                            {{ $article_details->instituteDetails()->institute_name }}
                                                        </a>
                                                    </h3>
                                                    <div class="text">
                                                        {!! $article_details->instituteDetails()->description !!}
                                                    </div>
                                                    <ul class="social-icon-two">
                                                        <li>
                                                            <span class="fa fa-envelope"></span> Email:
                                                            <a href="mailto:{{ $article_details->instituteDetails()->email }}">
                                                                {{ $article_details->instituteDetails()->email }}
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <span class="fa fa-globe"></span> Website:
                                                            <a href="{{ $article_details->instituteDetails()->website }}"
                                                               target="_blank">
                                                                {{ $article_details->instituteDetails()->website }}
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                @endif

                                <!--New Article-->
                                    <ul class="social-icon-one alternate">
                                        <li class="share">Share:</li>
                                        <li><a target="_blank"
                                               href="http://www.facebook.com/sharer.php?u={{url()->current()}}"><span
                                                        class="fa fa-facebook"></span></a></li>
                                        <li class="twitter"><a target="_blank"
                                                               href="http://twitter.com/home?status={{url()->current()}}"><span
                                                        class="fa fa-twitter"></span></a></li>
                                        <li class="g_plus"><a target="_blank"
                                                              href="https://plus.google.com/share?url={{url()->current()}}"><span
                                                        class="fa fa-google-plus"></span></a>
                                        </li>
                                        <li class="linkedin"><a target="_blank"
                                                                href="http://www.linkedin.com/shareArticle?mini=true&url={{url()->current()}}"><span
                                                        class="fa fa-linkedin-square"></span></a>
                                        </li>
                                        <li class="pinteret"><a href="#"><span class="fa fa-pinterest-p"></span></a>
                                        </li>
                                    </ul>

                                    <ul class="new-article clearfix">

                                        @if(isset ($prev_slug))
                                            <li>
                                                <a href="{{ route('news.details', ['slug' => $prev_slug]) }}">
                                                    <span class="fa fa-angle-left"></span> &ensp; &ensp; &ensp; &ensp;
                                                    {{ $prev_title }}
                                                </a>
                                            </li>
                                        @else
                                            <li>
                                                <a href="javascript:void()">
                                                    First Article &ensp; &ensp; &ensp; &ensp;
                                                </a>

                                            </li>
                                        @endif

                                        @if(isset ($next_slug))
                                            <li>
                                                <a href="{{ route('news.details', ['slug'=>$next_slug]) }}">
                                                    {{ $next_title }} &ensp; &ensp; &ensp; &ensp;
                                                    <span class="fa fa-angle-right"></span>
                                                </a>
                                            </li>
                                        @else
                                            <li>
                                                <a href="javascript:void()">
                                                    Last Article &ensp; &ensp; &ensp; &ensp;
                                                </a>
                                            </li>
                                        @endif

                                    </ul>
                                </div>

                            @if(count($related_articles) > 0)

                                <!--Related Posts-->
                                    <div class="related-posts">
                                        <div class="sec-title">
                                            <h2>Related Articles</h2>
                                        </div>
                                        <div class="related-item-carousel owl-carousel owl-theme">


                                        @foreach($related_articles as $article)
                                            <!--News Block Two-->
                                                <div class="news-block-two small-block">
                                                    <div class="inner-box" id="divEffect">
                                                        <div class="image">
                                                            <a href="{{ route('news.details', ['slug'=>$article->url_slug]) }}">
                                                                <img src="{{ asset($article->thumbnail_size) }}"
                                                                     alt=""/></a>
                                                            <div class="category">
                                                                <a href="{{ route('news.type', ['type'=>str_replace(' ','-',$article->getNewsTypeName())]) }}">
                                                                    {{ $article->getNewsTypeName() }}</a></div>
                                                        </div>
                                                        <div class="lower-box">
                                                            <h3>
                                                                <a href="{{ route('news.details', ['slug'=>$article->url_slug]) }}">
                                                                    {{ $article->article_title}}</a></h3>
                                                            <ul class="post-meta">
                                                                <li>
                                                                    <span class="icon fa fa-clock-o"></span>{{ $article->updated_at->toFormattedDateString() }}
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>
                            @endif

                            <!--Comments Area-->
                                <div class="comments-area">
                                    <div class="sec-title"><h2>Comment Section</h2></div>
                                    <!--Comment Box-->
                                    <div class="comment-box">
                                        <div id="disqus_thread"></div>
                                        <script>

                                            (function () {  // DON'T EDIT BELOW THIS LINE
                                                var d = document, s = d.createElement('script');

                                                s.src = 'https://http-amarpathshala-com.disqus.com/embed.js';

                                                s.setAttribute('data-timestamp', +new Date());
                                                (d.head || d.body).appendChild(s);
                                            })();
                                        </script>
                                        <noscript>Please enable JavaScript to view the <a
                                                    href="https://disqus.com/?ref_noscript" rel="nofollow">comments
                                                powered by Disqus.</a></noscript>
                                    </div>


                                </div>

                            </div>
                        </div>

                    </div>

                    <!--Sidebar Side-->
                    <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                        <aside class="sidebar default-sidebar right-sidebar">

                            <!--Category Widget-->
                            <div class="sidebar-widget categories-widget">
                                <div class="product-widget-tabs">
                                    <!--Product Tabs-->
                                    <div class="prod-tabs tabs-box">

                                        <!--Tab Btns-->
                                        <ul class="tab-btns tab-buttons clearfix">
                                            <li data-tab="#prod-popular" class="tab-btn active-btn">Recent News</li>
                                            <li data-tab="#prod-recent" class="tab-btn">Recent Blogs</li>
                                        </ul>

                                        <!--Tabs Container-->
                                        <div class="tabs-content">

                                            <!--Tab / Active Tab-->
                                            <div class="tab active-tab" id="prod-popular" style="display: block;">
                                                <div class="content">

                                                    @php
                                                        $recent_news = DB::table('articles')->orderBy('created_at','desc')
                                                                        ->where('status','Yes')->limit(5)->get();
                                                    @endphp

                                                    @foreach($recent_news as $news)
                                                        <article class="widget-post" id="sideDivEffect">
                                                            <figure class="post-thumb"><a
                                                                        href="{{ route('news.details', ['slug'=>$news->url_slug]) }}"><img
                                                                            class="wow fadeIn animated animated"
                                                                            data-wow-delay="0ms"
                                                                            data-wow-duration="2500ms"
                                                                            src="{{ asset($news->cover) }}"
                                                                            alt=""
                                                                            style="visibility: visible; animation-duration: 2500ms; animation-delay: 0ms; animation-name: fadeIn;"></a>
                                                                <div class="overlay"><span
                                                                            class="icon qb-play-arrow"></span>
                                                                </div>
                                                            </figure>
                                                            <div class="text"><a
                                                                        href="{{ route('news.details', ['slug'=>$news->url_slug]) }}">{{ $news->article_title }}</a>
                                                            </div>
                                                            <div class="post-info">{{ date('d M, Y', strtotime($news->created_at)) }}</div>
                                                        </article>
                                                    @endforeach

                                                </div>
                                            </div>

                                            <!--Tab-->
                                            <div class="tab" id="prod-recent" style="display: none;">
                                                <div class="content">

                                                    @php
                                                        $recent_blogs = DB::table('blogs')->orderBy('created_at','desc')
                                                                        ->where('status', 1)->limit(5)->get();
                                                    @endphp

                                                    @foreach($recent_blogs as $blog)
                                                        <article class="widget-post" id="sideDivEffect">
                                                            <figure class="post-thumb"><a
                                                                        href="{{ route('blog.details', ['slug'=>$blog->url_slug]) }}"><img
                                                                            class="wow fadeIn animated animated"
                                                                            data-wow-delay="0ms"
                                                                            data-wow-duration="2500ms"
                                                                            src="{{ asset($blog->thumbnail) }}"
                                                                            alt=""
                                                                            style="visibility: visible; animation-duration: 2500ms; animation-delay: 0ms; animation-name: fadeIn;"></a>
                                                                <div class="overlay"><span
                                                                            class="icon qb-play-arrow"></span>
                                                                </div>
                                                            </figure>
                                                            <div class="text"><a
                                                                        href="{{ route('blog.details', ['slug'=>$blog->url_slug]) }}">{{ $blog->title }}</a>
                                                            </div>
                                                            <div class="post-info">{{ date('d M, Y', strtotime($blog->created_at)) }}</div>
                                                        </article>
                                                    @endforeach

                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>


                            </div>
                            <!--End Category Widget-->


                            <!--Adds Widget-->
                            <div class="sidebar-widget sidebar-adds-widget">
                                <div class="adds-block"
                                     style="background-image:url({{ asset('frontend') }}/images/resource/add-image.jpg);">
                                    <div class="inner-box">
                                        <div class="text">Advertisement <span> 340 x 283</span></div>
                                        <a href="#" class="theme-btn btn-style-two">Purchase Now</a>
                                    </div>
                                </div>
                            </div>
                            <!--Ends Adds Widget-->


                        </aside>
                    </div>


                </div>
            </div>


        </div>


    </div>


@endsection

@section('js')

@endsection