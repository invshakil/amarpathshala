@extends('frontend.master')

@section('title')
    {{ $title }}
@endsection

@section('content')

    <div class="sidebar-page-container change-padding">


        <!--Sidebar Page Container-->
        <div class="sidebar-page-container">
            <div class="auto-container">
                <div class="row clearfix">

                    <!--Content Side-->
                    <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">


                        <div class="col-md-12 col-xs-12">
                            <div class="sec-title">
                                <h2>Search Result of {{ $search }}</h2>
                            </div>
                            <div class="content">
                                <div class="row clearfix">

                                    @if(isset($articles))

                                        <h4 class="alert alert-success">Total {{ $count }} News Article Found! </h4>

                                        @foreach($articles as $article)

                                            <!--News Block Two-->
                                            <div class="news-block-two with-margin col-md-6 col-sm-6 col-xs-12">
                                                <div class="inner-box" id="divEffect">
                                                    <div class="image">
                                                        <a href="{{ route('news.details', ['slug'=>$article->url_slug]) }}">
                                                            <img class="wow fadeIn" data-wow-delay="0ms"
                                                                 data-wow-duration="2500ms" style="max-height: 180px"
                                                                 src="{{ asset($article->cover) }}"
                                                                 alt=""/></a>
                                                        <div class="category">
                                                            <a href="{{ route('news.details', ['slug'=>$article->url_slug]) }}">{{ $article->getNewsTypeName() }}</a>
                                                        </div>
                                                    </div>
                                                    <div class="lower-box">
                                                        <h3>
                                                            <a href="{{ route('news.details', ['slug'=>$article->url_slug]) }}">
                                                                {{ $article->article_title }}
                                                            </a>
                                                        </h3>
                                                        <ul class="post-meta">
                                                            <li><span class="icon fa fa-clock-o"></span>
                                                                {{ $article->created_at->toFormattedDateString() }}
                                                            </li>
                                                            <li><span class="icon fa fa-user"></span>Admin</li>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>

                                        @endforeach

                                    @elseif(isset($blogs))

                                        <h4 class="alert alert-success">Total {{ $count }} Blog Article Found! </h4>

                                        @foreach($blogs as $article)

                                            <!--News Block Two-->
                                            <div class="news-block-two with-margin col-md-6 col-sm-6 col-xs-12">
                                                <div class="inner-box" id="divEffect">
                                                    <div class="image">
                                                        <a href="{{ route('blog.details', ['slug'=>$article->url_slug]) }}">
                                                            <img style="max-height: 180px"
                                                                 class="img-responsive wow fadeIn" data-wow-delay="0ms"
                                                                 data-wow-duration="2500ms"
                                                                 src="{{ asset($article->cover) }}"
                                                                 alt=""/></a>
                                                        <div class="category">
                                                            <a href="#">{{ $article->getCategoryName() }}</a>
                                                        </div>
                                                    </div>
                                                    <div class="lower-box">
                                                        <h3>
                                                            <a href="{{ route('blog.details', ['slug'=>$article->url_slug]) }}">
                                                                {{ $article->title }}
                                                            </a>
                                                        </h3>
                                                        <ul class="post-meta">
                                                            <li><span class="icon fa fa-clock-o"></span>
                                                                {{ $article->created_at->toFormattedDateString() }}
                                                            </li>
                                                            <li><span class="icon fa fa-user"></span>Admin</li>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>

                                        @endforeach

                                    @elseif(isset($institutes))

                                        <h4 class="alert alert-success">Total {{ $count }} Institute Found! </h4>
                                        @foreach($institutes as $institute)

                                            <!--News Block Two-->
                                            <div class="news-block-two with-margin col-md-4 col-sm-6 col-xs-12">
                                                <div class="inner-box" id="divEffect">
                                                    <div class="image">
                                                        <a href="{{ route('institute.details',
                                                    ['slug'=> str_replace(' ','-', $institute->institute_name)]) }}">
                                                            <img style="max-height: 180px"
                                                                 class="img-responsive wow fadeIn" data-wow-delay="0ms"
                                                                 data-wow-duration="2500ms"
                                                                 src="{{ asset($institute->logo) }}"
                                                                 alt="{{ asset($institute->institute_name) }}"/>
                                                        </a>
                                                    </div>
                                                    <div class="lower-box">
                                                        <h3>
                                                            <a href="{{ route('institute.details',
                                                    ['slug'=> str_replace(' ','-', $institute->institute_name)]) }}">
                                                                {{ $institute->institute_name }}
                                                            </a>
                                                        </h3>
                                                        <ul class="post-meta">
                                                            <li><span class="icon fa fa-clock-o"></span>
                                                                {{ $institute->created_at->toFormattedDateString() }}
                                                            </li>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>

                                        @endforeach

                                    @else

                                        <h3 class="alert alert-danger">No Records Found! </h3>

                                    @endif


                                </div>

                                @if(isset($articles))

                                    @if(count($articles) > 0)

                                        {{ $articles->links() }}

                                    @endif

                                @elseif(isset($blogs))

                                    @if(count($blogs) > 0)

                                        {{ $blogs->links() }}

                                    @endif

                                @elseif(isset($institutes))

                                    @if(count($institutes) > 0)

                                        {{ $institutes->links() }}

                                    @endif

                                @endif
                            </div>

                        </div>

                    </div>

                    <!--Sidebar Side-->
                    <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                        <aside class="sidebar default-sidebar right-sidebar">

                            <!--Category Widget-->
                            <div class="sidebar-widget categories-widget">
                                <div class="product-widget-tabs">
                                    <!--Product Tabs-->
                                    <div class="prod-tabs tabs-box">

                                        <!--Tab Btns-->
                                        <ul class="tab-btns tab-buttons clearfix">
                                            <li data-tab="#prod-recent" class="tab-btn active-btn">Recent Blogs</li>
                                            <li data-tab="#prod-popular" class="tab-btn ">Recent News</li>
                                        </ul>

                                        <!--Tabs Container-->
                                        <div class="tabs-content">


                                            <!--Tab-->
                                            <div class="tab active-tab" id="prod-recent" style="display: block;">
                                                <div class="content">

                                                    @php
                                                        $recent_blogs = DB::table('blogs')->orderBy('created_at','desc')
                                                                        ->where('status', 1)->limit(5)->get();
                                                    @endphp

                                                    @foreach($recent_blogs as $blog)
                                                        <article class="widget-post" id="sideDivEffect">
                                                            <figure class="post-thumb"><a
                                                                        href="{{ route('blog.details', ['slug'=>$blog->url_slug]) }}"><img
                                                                            class="wow fadeIn animated animated"
                                                                            data-wow-delay="0ms"
                                                                            data-wow-duration="2500ms"
                                                                            src="{{ asset($blog->thumbnail) }}"
                                                                            alt=""
                                                                            style="visibility: visible; animation-duration: 2500ms; animation-delay: 0ms; animation-name: fadeIn;"></a>
                                                                <div class="overlay"><span
                                                                            class="icon qb-play-arrow"></span>
                                                                </div>
                                                            </figure>
                                                            <div class="text"><a
                                                                        href="{{ route('blog.details', ['slug'=>$blog->url_slug]) }}">{{ $blog->title }}</a>
                                                            </div>
                                                            <div class="post-info">{{ date('d M, Y', strtotime($blog->created_at)) }}</div>
                                                        </article>
                                                    @endforeach

                                                </div>
                                            </div>

                                            <!--Tab / Active Tab-->
                                            <div class="tab " id="prod-popular" style="display: none;">
                                                <div class="content">

                                                    @php
                                                        $recent_news = DB::table('articles')->orderBy('created_at','desc')
                                                                        ->where('status','Yes')->limit(5)->get();
                                                    @endphp

                                                    @foreach($recent_news as $news)
                                                        <article class="widget-post">
                                                            <figure class="post-thumb"><a
                                                                        href="{{ route('news.details', ['slug'=>$news->url_slug]) }}"><img
                                                                            class="wow fadeIn animated animated"
                                                                            data-wow-delay="0ms"
                                                                            data-wow-duration="2500ms"
                                                                            src="{{ asset($news->cover) }}"
                                                                            alt=""
                                                                            style="visibility: visible; animation-duration: 2500ms; animation-delay: 0ms; animation-name: fadeIn;"></a>
                                                                <div class="overlay"><span
                                                                            class="icon qb-play-arrow"></span>
                                                                </div>
                                                            </figure>
                                                            <div class="text"><a
                                                                        href="{{ route('news.details', ['slug'=>$news->url_slug]) }}">{{ $news->article_title }}</a>
                                                            </div>
                                                            <div class="post-info">{{ date('d M, Y', strtotime($news->created_at)) }}</div>
                                                        </article>
                                                    @endforeach

                                                </div>
                                            </div>


                                        </div>

                                    </div>

                                </div>


                            </div>
                            <!--End Category Widget-->


                            <!--Adds Widget-->
                            <div class="sidebar-widget sidebar-adds-widget">
                                <div class="adds-block"
                                     style="background-image:url({{ asset('frontend') }}/images/resource/add-image.jpg);">
                                    <div class="inner-box">
                                        <div class="text">Advertisement <span> 340 x 283</span></div>
                                        <a href="#" class="theme-btn btn-style-two">Purchase Now</a>
                                    </div>
                                </div>
                            </div>
                            <!--Ends Adds Widget-->


                        </aside>
                    </div>


                </div>
            </div>


        </div>


    </div>


@endsection

@section('js')

@endsection