@extends('frontend.master')

@section('title')
    {{ $title }}
@endsection

@section('content')


    <!--Main Slider Four-->
    <section class="main-slider-four">

        <div class="section-outer">
            <div class="sec-title" style="text-align: center;">
                <h2>Latest Video Lessons</h2>
            </div>
            <div class="main-three-item-carousel owl-carousel owl-theme">

                <!--News Block Eight-->
                <div class="news-block-eight">
                    <div class="inner-box">
                        <div class="image">
                            <img class="img-responsive img-thumbnail"
                                 src="https://img.youtube.com/vi/RINRz8gEq_0/mqdefault.jpg" alt=""/>
                            <div class="overlay-box">
                                <div class="content">
                                    <h5><a style="" href="#">Yulin Creative meat festival begins in China
                                            amid
                                            widespread</a></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--News Block Eight-->
                <div class="news-block-eight">
                    <div class="inner-box">
                        <div class="image">
                            <img src="https://img.youtube.com/vi/bcIcgFnwkzk/mqdefault.jpg" alt=""/>
                            <div class="overlay-box">
                                <div class="content">
                                    <h5><a href="#">Develop a new Silk Road through Russia Universal
                                            Code</a></h5>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--News Block Eight-->
                <div class="news-block-eight">
                    <div class="inner-box">
                        <div class="image">
                            <img src="https://img.youtube.com/vi/kAGgHqckj60/mqdefault.jpg" alt=""/>
                            <div class="overlay-box">
                                <div class="content">
                                    <h5><a href="#">A modern day security strategy for your computer
                                            antivirus</a></h5>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--News Block Eight-->
                <div class="news-block-eight">
                    <div class="inner-box">
                        <div class="image">
                            <img src="https://img.youtube.com/vi/Ox0VLT4GJs0/mqdefault.jpg" alt=""/>
                            <div class="overlay-box">
                                <div class="content">
                                    <h5><a href="#">Yulin Creative meat festival begins in China amid
                                            widespread</a></h5>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--News Block Eight-->
                <div class="news-block-eight">
                    <div class="inner-box">
                        <div class="image">
                            <img src="https://img.youtube.com/vi/RINRz8gEq_0/mqdefault.jpg" alt=""/>
                            <div class="overlay-box">
                                <div class="content">
                                    <h5><a href="#">Develop a new Silk Road through Russia Universal
                                            Code</a></h5>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--News Block Eight-->
                <div class="news-block-eight">
                    <div class="inner-box">
                        <div class="image">
                            <img src="https://img.youtube.com/vi/kAGgHqckj60/mqdefault.jpg" alt=""/>
                            <div class="overlay-box">
                                <div class="content">
                                    <h5><a href="#">A modern day security strategy for your computer
                                            antivirus</a></h5>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section>
    <!--End Main Slider-->


    <div class="fullwidth-add text-center">
        <div class="image">
            <a href="#">
                <img class="wow fadeIn animated image-responsive" data-wow-delay="0ms" data-wow-duration="2500ms"
                     src="http://via.placeholder.com/900x80" alt=""
                     style="visibility: visible; animation-duration: 2500ms; animation-delay: 0ms; animation-name: fadeIn;">
            </a>
        </div>
    </div>

    <div class="sidebar-page-container change-padding">
        <!-- TUTORIAL BY CATEGORY -->
        <div class="auto-container">
            <div class="row clearfix">
                <div class="content-side col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="category-tabs-box">
                        <!--Product Tabs-->
                        <div class="prod-tabs tabs-box">

                            <!--Tab Btns-->
                            <div class="tab-btns tab-buttons clearfix">
                                <div class="pull-left">
                                    <div class="category">Browse Video Lessons By Class</div>
                                </div>
                                <div class="pull-right">
                                    <div>
                                        <div data-tab="#1" class="tab-btn active-btn">JSC</div>
                                        <div data-tab="#2" class="tab-btn">SSC</div>
                                        <div data-tab="#3" class="tab-btn">HSC</div>
                                        <div data-tab="#4" class="tab-btn">A Level</div>
                                        <div data-tab="#5" class="tab-btn">O Level</div>
                                        <div data-tab="#6" class="tab-btn">Other Courses</div>

                                    </div>
                                </div>

                            </div>

                            <!--Tabs Container-->
                            <div class="tabs-content">

                                <!--Tab / Active Tab-->
                                <div class="tab active-tab" id="1">
                                    <div class="content">
                                        <div class="row clearfix">

                                            <div class="col-md-3 col-sm-4 col-xs-12" style="height: 150px; ">
                                                <a href="{{ route('class.subject.chapter') }}" class="lightbox-image">
                                                    <figure class="image"
                                                            style="background: rgba(0, 0, 0, 0.4);padding: 50px;text-align: center;'">
                                                        <h3 style="color:white;font-weight: bold;">ENGLISH</h3>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class="col-md-3 col-sm-4 col-xs-12" style="height: 150px">
                                                <a href="{{ route('class.subject.chapter') }}" class="lightbox-image">
                                                    <figure class="image"
                                                            style="background: rgba(0, 0, 0, 0.4);padding: 50px;text-align: center;'">
                                                        <h3 style="color:white;font-weight: bold;">MATH</h3>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class="col-md-3 col-sm-4 col-xs-12" style="height: 150px">
                                                <a href="{{ route('class.subject.chapter') }}" class="lightbox-image">
                                                    <figure class="image"
                                                            style="background: rgba(0, 0, 0, 0.4);padding: 50px;text-align: center;'">
                                                        <h3 style="color:white;font-weight: bold;">SCIENCE</h3>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class="col-md-3 col-sm-4 col-xs-12" style="height: 150px">
                                                <a href="{{ route('class.subject.chapter') }}" class="lightbox-image">
                                                    <figure class="image"
                                                            style="background: rgba(0, 0, 0, 0.4);padding: 50px;text-align: center;'">
                                                        <h3 style="color:white;font-weight: bold;">BANGLA</h3>
                                                    </figure>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <!--Tab-->
                                <div class="tab" id="2">
                                    <div class="content">
                                        <div class="row clearfix">

                                            <div class="col-md-3 col-sm-4 col-xs-12">
                                                <a href="{{ route('class.subject.chapter') }}" class="lightbox-image">
                                                    <figure class="image"
                                                            style="background: rgba(0, 0, 0, 0.4);padding: 50px;text-align: center;'">
                                                        <h3 style="color:white;font-weight: bold;">ENGLISH</h3>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class="col-md-3 col-sm-4 col-xs-12">
                                                <a href="{{ route('class.subject.chapter') }}" class="lightbox-image">
                                                    <figure class="image"
                                                            style="background: rgba(0, 0, 0, 0.4);padding: 50px;text-align: center;'">
                                                        <h3 style="color:white;font-weight: bold;">MATH</h3>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class="col-md-3 col-sm-4 col-xs-12">
                                                <a href="{{ route('class.subject.chapter') }}" class="lightbox-image">
                                                    <figure class="image"
                                                            style="background: rgba(0, 0, 0, 0.4);padding: 50px;text-align: center;'">
                                                        <h3 style="color:white;font-weight: bold;">SCIENCE</h3>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class="col-md-3 col-sm-4 col-xs-12">
                                                <a href="{{ route('class.subject.chapter') }}" class="lightbox-image">
                                                    <figure class="image"
                                                            style="background: rgba(0, 0, 0, 0.4);padding: 50px;text-align: center;'">
                                                        <h3 style="color:white;font-weight: bold;">BANGLA</h3>
                                                    </figure>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <!--Tab-->
                                <div class="tab" id="3">
                                    <div class="content">
                                        <div class="row clearfix">

                                            <div class="col-md-3 col-sm-4 col-xs-12">
                                                <a href="{{ route('class.subject.chapter') }}" class="lightbox-image">
                                                    <figure class="image"
                                                            style="background: rgba(0, 0, 0, 0.4);padding: 50px;text-align: center;'">
                                                        <h3 style="color:white;font-weight: bold;">ENGLISH</h3>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class="col-md-3 col-sm-4 col-xs-12">
                                                <a href="{{ route('class.subject.chapter') }}" class="lightbox-image">
                                                    <figure class="image"
                                                            style="background: rgba(0, 0, 0, 0.4);padding: 50px;text-align: center;'">
                                                        <h3 style="color:white;font-weight: bold;">MATH</h3>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class="col-md-3 col-sm-4 col-xs-12">
                                                <a href="{{ route('class.subject.chapter') }}" class="lightbox-image">
                                                    <figure class="image"
                                                            style="background: rgba(0, 0, 0, 0.4);padding: 50px;text-align: center;'">
                                                        <h3 style="color:white;font-weight: bold;">SCIENCE</h3>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class="col-md-3 col-sm-4 col-xs-12">
                                                <a href="{{ route('class.subject.chapter') }}" class="lightbox-image">
                                                    <figure class="image"
                                                            style="background: rgba(0, 0, 0, 0.4);padding: 50px;text-align: center;'">
                                                        <h3 style="color:white;font-weight: bold;">BANGLA</h3>
                                                    </figure>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <!--Tab-->
                                <div class="tab" id="4">
                                    <div class="content">
                                        <div class="row clearfix">

                                            <div class="col-md-3 col-sm-4 col-xs-12">
                                                <a href="{{ route('class.subject.chapter') }}" class="lightbox-image">
                                                    <figure class="image"
                                                            style="background: rgba(0, 0, 0, 0.4);padding: 50px;text-align: center;'">
                                                        <h3 style="color:white;font-weight: bold;">ENGLISH</h3>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class="col-md-3 col-sm-4 col-xs-12">
                                                <a href="{{ route('class.subject.chapter') }}" class="lightbox-image">
                                                    <figure class="image"
                                                            style="background: rgba(0, 0, 0, 0.4);padding: 50px;text-align: center;'">
                                                        <h3 style="color:white;font-weight: bold;">MATH</h3>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class="col-md-3 col-sm-4 col-xs-12">
                                                <a href="{{ route('class.subject.chapter') }}" class="lightbox-image">
                                                    <figure class="image"
                                                            style="background: rgba(0, 0, 0, 0.4);padding: 50px;text-align: center;'">
                                                        <h3 style="color:white;font-weight: bold;">SCIENCE</h3>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class="col-md-3 col-sm-4 col-xs-12">
                                                <a href="{{ route('class.subject.chapter') }}" class="lightbox-image">
                                                    <figure class="image"
                                                            style="background: rgba(0, 0, 0, 0.4);padding: 50px;text-align: center;'">
                                                        <h3 style="color:white;font-weight: bold;">BANGLA</h3>
                                                    </figure>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <!--Tab-->
                                <div class="tab" id="5">
                                    <div class="content">
                                        <div class="row clearfix">

                                            <div class="col-md-3 col-sm-4 col-xs-12">
                                                <a href="{{ route('class.subject.chapter') }}" class="lightbox-image">
                                                    <figure class="image"
                                                            style="background: rgba(0, 0, 0, 0.4);padding: 50px;text-align: center;'">
                                                        <h3 style="color:white;font-weight: bold;">ENGLISH</h3>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class="col-md-3 col-sm-4 col-xs-12">
                                                <a href="{{ route('class.subject.chapter') }}" class="lightbox-image">
                                                    <figure class="image"
                                                            style="background: rgba(0, 0, 0, 0.4);padding: 50px;text-align: center;'">
                                                        <h3 style="color:white;font-weight: bold;">MATH</h3>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class="col-md-3 col-sm-4 col-xs-12">
                                                <a href="{{ route('class.subject.chapter') }}" class="lightbox-image">
                                                    <figure class="image"
                                                            style="background: rgba(0, 0, 0, 0.4);padding: 50px;text-align: center;'">
                                                        <h3 style="color:white;font-weight: bold;">SCIENCE</h3>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class="col-md-3 col-sm-4 col-xs-12">
                                                <a href="{{ route('class.subject.chapter') }}" class="lightbox-image">
                                                    <figure class="image"
                                                            style="background: rgba(0, 0, 0, 0.4);padding: 50px;text-align: center;'">
                                                        <h3 style="color:white;font-weight: bold;">BANGLA</h3>
                                                    </figure>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <!--Tab-->
                                <div class="tab" id="6">
                                    <div class="content">
                                        <div class="row clearfix">

                                            <div class="col-md-3 col-sm-4 col-xs-12">
                                                <a href="{{ route('class.subject.chapter') }}" class="lightbox-image">
                                                    <figure class="image"
                                                            style="background: rgba(0, 0, 0, 0.4);padding: 50px;text-align: center;'">
                                                        <h3 style="color:white;font-weight: bold;">ENGLISH</h3>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class="col-md-3 col-sm-4 col-xs-12">
                                                <a href="{{ route('class.subject.chapter') }}" class="lightbox-image">
                                                    <figure class="image"
                                                            style="background: rgba(0, 0, 0, 0.4);padding: 50px;text-align: center;'">
                                                        <h3 style="color:white;font-weight: bold;">MATH</h3>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class="col-md-3 col-sm-4 col-xs-12">
                                                <a href="{{ route('class.subject.chapter') }}" class="lightbox-image">
                                                    <figure class="image"
                                                            style="background: rgba(0, 0, 0, 0.4);padding: 50px;text-align: center;'">
                                                        <h3 style="color:white;font-weight: bold;">SCIENCE</h3>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class="col-md-3 col-sm-4 col-xs-12">
                                                <a href="{{ route('class.subject.chapter') }}" class="lightbox-image">
                                                    <figure class="image"
                                                            style="background: rgba(0, 0, 0, 0.4);padding: 50px;text-align: center;'">
                                                        <h3 style="color:white;font-weight: bold;">BANGLA</h3>
                                                    </figure>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <div class="auto-container">
            <div class="row clearfix">


                <!-- LATEST EDU NEWS -->
                <div class="content-side pull-left col-lg-9 col-md-9 col-sm-12 col-xs-12">

                    <!--Latest News-->
                    <div class="latest-news-section">
                        <!--Sec Title-->
                        <div class="sec-title">
                            <h2>Latest Educational News</h2>
                        </div>
                        <div class="row clearfix">

                        @foreach($articles as $key=>$article)

                            <!--News Block Two-->
                                <div class="news-block-two with-margin col-md-4 col-sm-4 col-xs-12">
                                    <div class="inner-box" id="divEffect">
                                        <div class="image">
                                            <a href="{{ route('news.details', ['slug'=>$article->url_slug]) }}">
                                                <img class="wow fadeIn img-responsive img-thumbnail"
                                                     data-wow-delay="0ms" style="max-height: 135px"
                                                     data-wow-duration="{{ 2500+$key*500 }}ms"
                                                     src="{{ asset($article->tiny_thumb) }}"
                                                     alt=""/></a>
                                            <div class="category">
                                                <a href="{{ route('news.type',
                                        ['type'=>str_replace(' ','-',$article->getNewsTypeName())])
                                        }}">{{ $article->getNewsTypeName() }}</a>
                                            </div>
                                        </div>
                                        <div class="lower-box">
                                            <h3>
                                                <a href="{{ route('news.details', ['slug'=>$article->url_slug]) }}">
                                                    {{ $article->article_title }}
                                                </a>
                                            </h3>
                                            <ul class="post-meta">
                                                <li><span class="icon fa fa-clock-o"></span>
                                                    {{ $article->created_at->toFormattedDateString() }}
                                                </li>
                                                <li><span class="icon fa fa-user"></span>Admin</li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>

                            @endforeach

                        </div>

                        {{ $articles->links() }}

                    </div>

                </div>


                <div class="sidebar-side pull-right col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <aside class="sidebar default-sidebar left-sidebar">

                        <!--ALL NEWS TYPE-->
                        <div class="sidebar-widget categories-widget">
                            <div class="sidebar-title">
                                <h2>Categories</h2>
                            </div>
                            <ul class="cat-list">

                                @foreach($news_types as $news_type)
                                    <li class="clearfix">
                                        <a style="color: black;" href="{{ route('news.type',
                                        ['type'=>str_replace(' ','-',$news_type->news_type_title)])
                                        }}">
                                            {{ $news_type->news_type_title }}
                                            <span>({{ $news_type->ArticleCount($news_type->id)  }})</span>
                                        </a>
                                    </li>
                                @endforeach

                            </ul>
                        </div>
                        <!--End Category Widget-->


                        <!--Adds Widget-->
                        <div class="sidebar-widget sidebar-adds-widget">
                            <div class="adds-block"
                                 style="background-image:url({{ asset('frontend') }}/images/resource/add-image.jpg);">
                                <div class="inner-box">
                                    <div class="text">Advertisement <span> 340 x 283</span></div>
                                    <a href="#" class="theme-btn btn-style-two">Purchase Now</a>
                                </div>
                            </div>
                        </div>
                        <!--Ends Adds Widget-->

                    </aside>
                </div>

            </div>

        </div>
    </div>

    <div class="fullwidth-add text-center">
        <div class="image">
            <a href="#">
                <img class="wow fadeIn animated image-responsive" data-wow-delay="0ms" data-wow-duration="2500ms"
                     src="http://via.placeholder.com/900x80" alt=""
                     style="visibility: visible; animation-duration: 2500ms; animation-delay: 0ms; animation-name: fadeIn;">
            </a>
        </div>
    </div>

    <div class="sidebar-page-container change-padding">


        <!--Sidebar Page Container-->
        <div class="sidebar-page-container">
            <div class="auto-container">
                <div class="row clearfix">

                    <!--LATEST BLOG-->
                    <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <div class="sec-title">
                            <h2>Latest Blogs</h2>
                        </div>

                        @foreach($blogs->chunk(4) as $blogChunk)
                            <div class="col-md-6 col-sm-6 col-xs-12"
                                 style="padding: 0!important;padding-bottom: 5px!important;">

                            @foreach($blogChunk as $blog)
                                <!--Widget Post-->
                                    <article class="widget-post" id="sideDivEffect" style="min-height: 85px">
                                        <figure class="post-thumb">
                                            <a href="{{ route('blog.details', ['slug'=>$blog->url_slug]) }}">
                                                <img
                                                        class="wow fadeIn img-thumbnail" data-wow-delay="0ms"
                                                        data-wow-duration="2500ms"
                                                        src="{{ asset($blog->thumbnail) }}"
                                                        alt="">
                                            </a>
                                        </figure>
                                        <div class="text">
                                            <a href="{{ route('blog.details', ['slug'=>$blog->url_slug]) }}">
                                                {{ $blog->title }}
                                            </a>
                                        </div>
                                        <div class="post-info">{{ $blog->created_at->toFormattedDateString() }}</div>
                                    </article>
                                @endforeach

                            </div>
                        @endforeach

                        <br/>

                        {{ $blogs->links() }}

                    </div>

                    <!--FOLLOW US-->
                    <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <aside class="sidebar default-sidebar right-sidebar">

                            <!--Social Widget-->
                            <div class="sidebar-widget sidebar-social-widget">
                                <div class="sidebar-title">
                                    <h2>Follow Us</h2>
                                </div>
                                <ul class="social-icon-one alternate">
                                    <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                                    <li class="twitter"><a href="#"><span class="fa fa-twitter"></span></a></li>
                                    <li class="g_plus"><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                    <li class="linkedin"><a href="#"><span class="fa fa-linkedin-square"></span></a>
                                    </li>
                                    <li class="pinteret"><a href="#"><span class="fa fa-pinterest-p"></span></a></li>
                                    <li class="instagram"><a href="#"><span class="fa fa-instagram"></span></a></li>
                                    <li class="vimeo"><a href="#"><span class="fa fa-vimeo"></span></a></li>
                                </ul>
                            </div>
                            <!--End Social Widget-->

                            <!--Adds Widget-->
                            <div class="sidebar-widget sidebar-adds-widget">
                                <a href="https://placeholder.com">
                                    <img class="img-responsive img-thumbnail" src="http://via.placeholder.com/350x150">
                                </a>
                            </div>
                            <!--Ends Adds Widget-->


                        </aside>
                    </div>


                </div>
            </div>
        </div>


    </div>
    <!--End pagewrapper-->

@endsection

@section('js')

@endsection