@extends('frontend.master')

@section('title')
    {{ $title }}
@endsection

@section('content')

    <div class="sidebar-page-container change-padding">


        <!--Sidebar Page Container-->
        <div class="sidebar-page-container">
            <div class="auto-container">
                <div class="row clearfix">

                    <!--Content Side-->
                    <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">



                        <div class="col-md-12 col-xs-12">
                            <div class="sec-title">
                                <h2>ALL CHAPTERS OF ENGLISH (JSC)</h2>

                            </div>
                            <div class="content">
                                <div class="row clearfix">

                                    <div id="chapter-list" class="col-md-4 col-sm-6 col-xs-12">
                                        <div>
                                            <a href="{{ route('chapter.video.list') }}" class="lightbox-image">
                                                <figure class="image overlay-box">
                                                    <img src="http://via.placeholder.com/400x180">
                                                    <h4>A Chapter Name</h4>
                                                </figure>
                                            </a>
                                        </div>
                                    </div>

                                    <div id="chapter-list" class="col-md-4 col-sm-6 col-xs-12">
                                        <div>
                                            <a href="{{ route('chapter.video.list') }}" class="lightbox-image">
                                                <figure class="image overlay-box">
                                                    <img src="http://via.placeholder.com/400x180">
                                                    <h4>A Chapter Name</h4>
                                                </figure>
                                            </a>
                                        </div>
                                    </div>

                                    <div id="chapter-list" class="col-md-4 col-sm-6 col-xs-12">
                                        <div>
                                            <a href="{{ route('chapter.video.list') }}" class="lightbox-image">
                                                <figure class="image overlay-box">
                                                    <img src="http://via.placeholder.com/400x180">
                                                    <h4>A Chapter Name</h4>
                                                </figure>
                                            </a>
                                        </div>
                                    </div>

                                    <div id="chapter-list" class="col-md-4 col-sm-6 col-xs-12">
                                        <div>
                                            <a href="{{ route('chapter.video.list') }}" class="lightbox-image">
                                                <figure class="image overlay-box">
                                                    <img src="http://via.placeholder.com/400x180">
                                                    <h4>A Chapter Name</h4>
                                                </figure>
                                            </a>
                                        </div>
                                    </div>



                                </div>
                            </div>

                            <div class="text-center">
                                <button class="btn btn-info">LOAD MORE</button>
                            </div>
                        </div>

                    </div>

                    <!--Sidebar Side-->
                    <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <aside class="sidebar default-sidebar right-sidebar">

                            <!--Category Widget-->
                            <div class="sidebar-widget categories-widget">
                                <div class="sidebar-title">
                                    <h2>SUBJECTS OF JSC</h2>
                                </div>
                                <ul class="cat-list">
                                    <li class="clearfix"><a href="#">BANGLA <span>(30)</span></a></li>
                                    <li class="clearfix"><a href="#">ENGLISH<span>(9)</span></a></li>
                                    <li class="clearfix"><a href="#">MATH<span>(13)</span></a></li>
                                    <li class="clearfix"><a href="#">SCIENCE<span>(70)</span></a></li>
                                    <li class="clearfix"><a href="#">INFORMATION TECHNOLOGY<span>(11)</span></a></li>
                                </ul>
                            </div>
                            <!--End Category Widget-->

                            <!--Adds Widget-->
                            <div class="sidebar-widget sidebar-adds-widget">
                                <a href="https://placeholder.com">
                                    <img class="img-responsive img-thumbnail" src="http://via.placeholder.com/350x150">
                                </a>
                            </div>
                            <!--Ends Adds Widget-->


                        </aside>
                    </div>


                </div>
            </div>


        </div>


    </div>
    <style>
        .image h4{

            position: absolute;
            bottom: 0;
            background: rgba(0, 0, 0, 0.7);
            text-align: left;
            padding: 10px;
            margin: 10px 40px 20px 30px;
            -moz-box-shadow: 3px 3px 3px #000;
            -webkit-box-shadow: 3px 3px 3px #000;
            box-shadow: 3px 3px 3px #000;
            color: white;
            font-size: 15px;
            font-weight: bold;
        }
    </style>
@endsection

@section('js')

@endsection