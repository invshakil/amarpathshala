@extends('frontend.master')

@section('title')
    {{ $title }}
@endsection

@section('content')
    <!--Page Title-->
    <section class="page-title">
        <div class="auto-container">
            <div class="clearfix">

                <div class="pull-right">
                    <ul class="page-title-breadcrumb">
                        <li><a href="{{ route('/') }}"><span class="fa fa-home"></span>Home</a></li>
                        <li>{{ $name }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <!--About Section-->
    <section class="about-section">

        <!--Author Info-->
        <div class="author-info">
            <div class="auto-container">
                <div class="row clearfix">
                    <!--Image Column-->
                    <div class="image-column col-md-3 col-sm-3 col-xs-12">
                        <div class="image">
                            <img src="{{ asset($info->logo) }}" class="image-responsive" width="80%" alt="{{ $name }}"/>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-1"></div>
                    <!--Image Column-->
                    <div class="content-column col-md-8 col-sm-8 col-xs-12">
                        <div class="content-inner">
                            <h2><span class="theme_color">{{ $name }}</span></h2>
                            <div class="text">
                                <p>Institute Head: {{ $info->director }}</p>
                                <p>Address: {{ $info->description }}</p>
                                <p>Type: <b>{{ $info->getInstituteTypeName($info->type) }}</b></p>
                                <p>Website: <a target="_blank" href="{{ $info->website }}">{{ $info->website }}</a></p>
                            </div>
                            <ul class="social-icon-one alternate">
                                <li><a target="_blank"
                                       href="http://www.facebook.com/sharer.php?u={{url()->current()}}"><span
                                                class="fa fa-facebook"></span></a></li>
                                <li class="twitter"><a target="_blank"
                                                       href="http://twitter.com/home?status={{url()->current()}}"><span
                                                class="
                                                               fa fa-twitter"></span></a></li>
                                <li class="g_plus"><a target="_blank"
                                                      href="https://plus.google.com/share?url={{url()->current()}}"><span
                                                class="fa fa-google-plus"></span></a>
                                </li>
                                <li class="linkedin"><a target="_blank"
                                                        href="http://www.linkedin.com/shareArticle?mini=true&url={{url()->current()}}"><span
                                                class="fa fa-linkedin-square"></span></a>
                                </li>
                                <li class="pinteret"><a href="#"><span class="fa fa-pinterest-p"></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="fullwidth-add text-center">
                    <div class="image">
                        <a href="#">
                            <img class="wow fadeIn animated image-responsive" data-wow-delay="0ms"
                                 data-wow-duration="2500ms"
                                 src="http://via.placeholder.com/900x80" alt=""
                                 style="visibility: visible; animation-duration: 2500ms; animation-delay: 0ms; animation-name: fadeIn;">
                        </a>
                    </div>
                </div>
                <div class="row clearfix" style="padding-top: 20px;">

                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="sec-title">
                            <h2>Latest News on {{ $name }}</h2>
                        </div>

                    @if(count($articles) > 0)
                        @foreach($articles as $key=>$article)

                            <!--News Block Two-->
                                <div class="news-block-two with-margin col-md-4 col-sm-6 col-xs-12">
                                    <div class="inner-box" id="divEffect">
                                        <div class="image">
                                            <a href="{{ route('news.details', ['slug'=>$article->url_slug]) }}">
                                                <img class="wow fadeIn img-responsive img-thumbnail"
                                                     data-wow-delay="0ms" style="max-height: 180px"
                                                     data-wow-duration="{{ 2500+$key*500 }}ms"
                                                     src="{{ asset($article->cover) }}"
                                                     alt=""/></a>
                                            <div class="category">
                                                <a href="{{ route('news.details', ['slug'=>$article->url_slug]) }}">{{ $article->getNewsTypeName() }}</a>
                                            </div>
                                        </div>
                                        <div class="lower-box">
                                            <h3>
                                                <a href="{{ route('news.details', ['slug'=>$article->url_slug]) }}">
                                                    {{ $article->article_title }}
                                                </a>
                                            </h3>
                                            <ul class="post-meta">
                                                <li><span class="icon fa fa-clock-o"></span>
                                                    {{ $article->created_at->toFormattedDateString() }}
                                                </li>
                                                <li><span class="icon fa fa-user"></span>Admin</li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>

                            @endforeach
                        @else
                            <h3 class="alert alert-danger">No News Posted Yet!</h3>

                        @endif
                    </div>

                    {{ $articles->links() }}
                </div>
            </div>


        </div>


    </section>


@endsection