@extends('frontend.master')

@section('title')
    {{ $title }}
@endsection

@section('content')

    <div class="sidebar-page-container change-padding">


        <!--Sidebar Page Container-->
        <div class="sidebar-page-container">
            <div class="auto-container">
                <div class="row clearfix">

                    <!--Content Side-->
                    <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">


                        <div class="col-md-12 col-xs-12">
                            <div class="sec-title">
                                <h2>Latest Updates on {{ $cat_name }}</h2>
                            </div>
                            <div class="content">
                                <div class="row clearfix">

                                @if(count($articles) > 0)

                                    @foreach($articles as $article)

                                        <!--News Block Two-->
                                            <div class="news-block-two with-margin col-md-6 col-sm-6 col-xs-12">
                                                <div class="inner-box" id="divEffect">
                                                    <div class="image">
                                                        <a href="{{ route('blog.details', ['slug'=>$article->url_slug]) }}">
                                                            <img style="max-height: 180px"
                                                                 class="img-responsive wow fadeIn" data-wow-delay="0ms"
                                                                 data-wow-duration="2500ms"
                                                                 src="{{ asset($article->cover) }}"
                                                                 alt=""/></a>

                                                    </div>
                                                    <div class="lower-box">
                                                        <h3>
                                                            <a href="{{ route('blog.details', ['slug'=>$article->url_slug]) }}">
                                                                {{ $article->title }}
                                                            </a>
                                                        </h3>
                                                        <ul class="post-meta">
                                                            <li><span class="icon fa fa-clock-o"></span>
                                                                {{ $article->created_at->toFormattedDateString() }}
                                                            </li>
                                                            <li><span class="icon fa fa-user"></span>Admin</li>
                                                        </ul>

                                                    </div>
                                                </div>
                                            </div>

                                        @endforeach

                                    @else

                                        <h2 class="alert alert-danger">No Records Found! </h2>

                                    @endif


                                </div>
                                {{ $articles->links() }}
                            </div>

                        </div>

                    </div>

                    <!--Sidebar Side-->
                    <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <aside class="sidebar default-sidebar right-sidebar">

                            <!--Category Widget-->
                            <div class="sidebar-widget categories-widget">
                                <div class="sidebar-title">
                                    <h2>Categories</h2>
                                </div>

                                <ul class="cat-list">

                                    @foreach($categories as $category)
                                        <li class="clearfix">
                                            <a href="{{ route('blog.type',
                                        ['type'=>str_replace(' ','-',$category->name)])
                                        }}">
                                                {{ $category->name }}
                                                <span>({{ $category->BlogCount($category->id)  }})</span>
                                            </a>
                                        </li>
                                    @endforeach

                                </ul>
                            </div>
                            <!--End Category Widget-->


                            <!--Adds Widget-->
                            <div class="sidebar-widget sidebar-adds-widget">
                                <div class="adds-block"
                                     style="background-image:url({{ asset('frontend') }}/images/resource/add-image.jpg);">
                                    <div class="inner-box">
                                        <div class="text">Advertisement <span> 340 x 283</span></div>
                                        <a href="#" class="theme-btn btn-style-two">Purchase Now</a>
                                    </div>
                                </div>
                            </div>
                            <!--Ends Adds Widget-->


                        </aside>
                    </div>


                </div>
            </div>


        </div>


    </div>

@endsection

@section('js')

@endsection