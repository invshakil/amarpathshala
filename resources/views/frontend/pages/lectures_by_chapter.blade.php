@extends('frontend.master')

@section('title')
    {{ $title }}
@endsection

@section('content')


    <div class="sidebar-page-container">
        <div class="auto-container">
            <div class="sec-title">
                <div class="col-md-6 col-xs-12" style="padding-left: 0!important;">
                    <h2>All Video Lessons: Chapter 1 / Bangla / JSC</h2>
                </div>

                <div id="select-wrapper">
                    <div class="col-md-2 col-xs-12 form-group">

                        <select class="form-control">
                            <option>Select Class</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                        </select>
                    </div>

                    <div class="col-md-2 col-xs-12 form-group">

                        <select class="form-control">
                            <option>Select Subject</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                        </select>
                    </div>

                    <div class="col-md-2 col-xs-12 form-group" style="padding-right: 0!important;">

                        <select class="form-control">
                            <option>Select Chapter</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                        </select>
                    </div>
                </div>


            </div>


            <div class="row clearfix">
                <div class="col-md-8 col-sm-8 col-xs-12 pull-right" style="padding-left: 0!important;">
                    <!-- THE YOUTUBE PLAYER -->

                    <div class="video-container">

                        <iframe id="vid_frame" height="390"
                                src="http://www.youtube.com/embed/cOSEOYi9JS4?rel=0&showinfo=0&autohide=1"
                                frameborder="0"
                                allowfullscreen>

                        </iframe>
                    </div>

                </div>

                <div class="col-md-4 col-sm-4 col-xs-12 pull-left" style="padding-right: 0!important;">
                    <!-- THE PLAYLIST -->
                    <div class="vid-list-container ">
                        <ol id="vid-list">
                            <li>
                                <a href="javascript:void(0);" class="active"
                                   onClick="document.getElementById('vid_frame').src='https://youtube.com/embed/cOSEOYi9JS4?autoplay=1&rel=0&showinfo=0&autohide=1'">
                                        <span class="vid-thumb">
                                            <img width=72 class="wow fadeIn animated animated"
                                                 src="https://img.youtube.com/vi/cOSEOYi9JS4/default.jpg"/></span>
                                    <div class="desc">WeatherBeater™ Product Video</div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);"
                                   onClick="document.getElementById('vid_frame').src='https://youtube.com/embed/9P7mEf4bilg?autoplay=1&rel=0&showinfo=0&autohide=1'">
                                        <span class="vid-thumb">
                                            <img width=72 class="wow fadeIn animated animated"
                                                 src="https://img.youtube.com/vi/9P7mEf4bilg/default.jpg"/></span>
                                    <div class="desc">X-act Contour® Product Video</div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);"
                                   onClick="document.getElementById('vid_frame').src='https://youtube.com/embed/KHxNpXovl58?autoplay=1&rel=0&showinfo=0&autohide=1'">
                                        <span class="vid-thumb">
                                            <img width=72 class="wow fadeIn animated animated"
                                                 src="https://img.youtube.com/vi/KHxNpXovl58/default.jpg"/></span>
                                    <div class="desc">GearBox® Product Video</div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);"
                                   onClick="document.getElementById('vid_frame').src='https://youtube.com/embed/D_a2UBGsePQ?autoplay=1&rel=0&showinfo=0&autohide=1'">
                                        <span class="vid-thumb">
                                            <img width=72 class="wow fadeIn animated animated"
                                                 src="https://img.youtube.com/vi/D_a2UBGsePQ/default.jpg"/></span>
                                    <div class="desc">Mud Guards Product Video</div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);"
                                   onClick="document.getElementById('vid_frame').src='https://youtube.com/embed/vYoh2IgoBXg?autoplay=1&rel=0&showinfo=0&autohide=1'">
                                        <span class="vid-thumb">
                                            <img width=72 class="wow fadeIn animated animated"
                                                 src="https://img.youtube.com/vi/vYoh2IgoBXg/default.jpg"/></span>
                                    <div class="desc">Wheel Well Guards Product Video</div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);"
                                   onClick="document.getElementById('vid_frame').src='https://youtube.com/embed/RTHI_uGyfTM?autoplay=1&rel=0&showinfo=0&autohide=1'">
                                        <span class="vid-thumb">
                                            <img width=72 class="wow fadeIn animated animated"
                                                 src="https://img.youtube.com/vi/RTHI_uGyfTM/default.jpg"/></span>
                                    <div class="desc">Cargo Liner Product Video</div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);"
                                   onClick="document.getElementById('vid_frame').src='https://youtube.com/embed/EvTjAjLNphE?autoplay=1&rel=0&showinfo=0&autohide=1'">
                                        <span class="vid-thumb">
                                            <img width=72 class="wow fadeIn animated animated"
                                                 src="https://img.youtube.com/vi/EvTjAjLNphE/default.jpg"/></span>
                                    <div class="desc">Husky Liners Products</div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);"
                                   onClick="document.getElementById('vid_frame').src='https://youtube.com/embed/-Qpc79oaJQg?autoplay=1&rel=0&showinfo=0&autohide=1'">
                                        <span class="vid-thumb">
                                            <img width=72 class="wow fadeIn animated animated"
                                                 src="https://img.youtube.com/vi/-Qpc79oaJQg/default.jpg"/></span>
                                    <div class="desc">Custom Molded Mud Guards</div>
                                </a>
                            </li>

                        </ol>
                    </div>
                </div>

            </div>
        </div>

        <div class="fullwidth-add text-center" style="margin: 20px 0 20px 0">
            <div class="image">
                <a href="#">
                    <img class="wow fadeIn animated" data-wow-delay="0ms" data-wow-duration="2500ms"
                         src="{{ asset('frontend') }}/images/resource/add-1.jpg" alt=""
                         style="visibility: visible; animation-duration: 2500ms; animation-delay: 0ms; animation-name: fadeIn;">
                </a>
            </div>
        </div>

        <div class="auto-container">
            <div class="row clearfix">

                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">

                    <div class="col-md-12 col-xs-12">
                        <div class="sec-title" style="margin-bottom: 35px!important;">
                            <h2>ALL Quiz test On this chapter</h2>

                        </div>
                        <div class="content">
                            <div class="row clearfix">

                                <div id="chapter-list" class="col-md-6 col-sm-6 col-xs-12">
                                    <div>
                                        <a href="{{ route('chapter.video.list') }}" class="lightbox-image">
                                            <figure class="image overlay-box">
                                                <img class="wow fadeIn animated animated"
                                                     src="http://via.placeholder.com/400x180">
                                                <h4>Quiz Test 1 </h4>
                                            </figure>
                                        </a>
                                    </div>
                                </div>

                                <div id="chapter-list" class="col-md-6 col-sm-6 col-xs-12">
                                    <div>
                                        <a href="#" class="lightbox-image">
                                            <figure class="image overlay-box">
                                                <img class="wow fadeIn animated animated"
                                                     src="http://via.placeholder.com/400x180">
                                                <h4>Quiz Test 2</h4>
                                            </figure>
                                        </a>
                                    </div>
                                </div>

                                <div id="chapter-list" class="col-md-6 col-sm-6 col-xs-12">
                                    <div>
                                        <a href="#" class="lightbox-image">
                                            <figure class="image overlay-box">
                                                <img class="wow fadeIn animated animated"
                                                     src="http://via.placeholder.com/400x180">
                                                <h4>Quiz Test 3</h4>
                                            </figure>
                                        </a>
                                    </div>
                                </div>

                                <div id="chapter-list" class="col-md-6 col-sm-6 col-xs-12">
                                    <div>
                                        <a href="#" class="lightbox-image">
                                            <figure class="image overlay-box">
                                                <img class="wow fadeIn animated animated"
                                                     src="http://via.placeholder.com/400x180">
                                                <h4>Quiz Test 4</h4>
                                            </figure>
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="text-center">
                            <button class="btn btn-info">LOAD MORE</button>
                        </div>
                    </div>

                </div>

                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar default-sidebar right-sidebar">

                        <div class="sidebar-widget posts-widget">

                            <!--Product widget Tabs-->
                            <div class="product-widget-tabs">
                                <!--Product Tabs-->
                                <div class="prod-tabs tabs-box">

                                    <!--Tab Btns-->
                                    <ul class="tab-btns tab-buttons clearfix">
                                        <li data-tab="#prod-popular" class="tab-btn active-btn">Recent Blogs</li>
                                        <li data-tab="#prod-recent" class="tab-btn">Recent News</li>
                                    </ul>

                                    <!--Tabs Container-->
                                    <div class="tabs-content">

                                        <!--Tab / Active Tab-->
                                        <div class="tab active-tab" id="prod-popular" style="display: block;">
                                            <div class="content">

                                                <article class="widget-post">
                                                    <figure class="post-thumb"><a href="blog-single.html"><img
                                                                    class="wow fadeIn animated" data-wow-delay="0ms"
                                                                    data-wow-duration="2500ms"
                                                                    src="{{ asset('frontend') }}/images/resource/post-thumb-1.jpg"
                                                                    alt=""
                                                                    style="visibility: visible; animation-duration: 2500ms; animation-delay: 0ms; animation-name: fadeIn;"></a>
                                                        <div class="overlay"><span class="icon qb-play-arrow"></span>
                                                        </div>
                                                    </figure>
                                                    <div class="text"><a href="blog-single.html">Priyanka Chopra got her
                                                            what photoshopped?</a></div>
                                                    <div class="post-info">April 01, 2016</div>
                                                </article>

                                                <article class="widget-post">
                                                    <figure class="post-thumb"><a href="blog-single.html"><img
                                                                    class="wow fadeIn animated" data-wow-delay="0ms"
                                                                    data-wow-duration="2500ms"
                                                                    src="{{ asset('frontend') }}/images/resource/post-thumb-2.jpg"
                                                                    alt=""
                                                                    style="visibility: visible; animation-duration: 2500ms; animation-delay: 0ms; animation-name: fadeIn;"></a>
                                                        <div class="overlay"><span class="icon qb-play-arrow"></span>
                                                        </div>
                                                    </figure>
                                                    <div class="text"><a href="blog-single.html">Jerry Seinfeld grabs
                                                            coffee with Margaret Cho and it gets</a></div>
                                                    <div class="post-info">April 02, 2016</div>
                                                </article>

                                                <article class="widget-post">
                                                    <figure class="post-thumb"><a href="blog-single.html"><img
                                                                    class="wow fadeIn animated" data-wow-delay="0ms"
                                                                    data-wow-duration="2500ms"
                                                                    src="{{ asset('frontend') }}/images/resource/post-thumb-3.jpg"
                                                                    alt=""
                                                                    style="visibility: visible; animation-duration: 2500ms; animation-delay: 0ms; animation-name: fadeIn;"></a>
                                                        <div class="overlay"><span class="icon qb-play-arrow"></span>
                                                        </div>
                                                    </figure>
                                                    <div class="text"><a href="blog-single.html">American Black Film
                                                            Festival New projects from film TV</a></div>
                                                    <div class="post-info">April 03, 2016</div>
                                                </article>

                                                <article class="widget-post">
                                                    <figure class="post-thumb"><a href="blog-single.html"><img
                                                                    class="wow fadeIn animated" data-wow-delay="0ms"
                                                                    data-wow-duration="2500ms"
                                                                    src="{{ asset('frontend') }}/images/resource/post-thumb-1.jpg"
                                                                    alt=""
                                                                    style="visibility: visible; animation-duration: 2500ms; animation-delay: 0ms; animation-name: fadeIn;"></a>
                                                        <div class="overlay"><span class="icon qb-play-arrow"></span>
                                                        </div>
                                                    </figure>
                                                    <div class="text"><a href="blog-single.html">Priyanka Chopra got her
                                                            what photoshopped?</a></div>
                                                    <div class="post-info">April 01, 2016</div>
                                                </article>

                                            </div>
                                        </div>

                                        <!--Tab-->
                                        <div class="tab" id="prod-recent" style="display: none;">
                                            <div class="content">

                                                <article class="widget-post">
                                                    <figure class="post-thumb"><a href="blog-single.html"><img
                                                                    class="wow fadeIn animated" data-wow-delay="0ms"
                                                                    data-wow-duration="2500ms"
                                                                    src="{{ asset('frontend') }}/images/resource/post-thumb-2.jpg"
                                                                    alt=""
                                                                    style="visibility: visible; animation-duration: 2500ms; animation-delay: 0ms; animation-name: fadeIn;"></a>
                                                        <div class="overlay"><span class="icon qb-play-arrow"></span>
                                                        </div>
                                                    </figure>
                                                    <div class="text"><a href="blog-single.html">Jerry Seinfeld grabs
                                                            coffee with Margaret Cho and it gets</a></div>
                                                    <div class="post-info">April 02, 2016</div>
                                                </article>

                                                <article class="widget-post">
                                                    <figure class="post-thumb"><a href="blog-single.html"><img
                                                                    class="wow fadeIn animated" data-wow-delay="0ms"
                                                                    data-wow-duration="2500ms"
                                                                    src="{{ asset('frontend') }}/images/resource/post-thumb-3.jpg"
                                                                    alt=""
                                                                    style="visibility: visible; animation-duration: 2500ms; animation-delay: 0ms; animation-name: fadeIn;"></a>
                                                        <div class="overlay"><span class="icon qb-play-arrow"></span>
                                                        </div>
                                                    </figure>
                                                    <div class="text"><a href="blog-single.html">American Black Film
                                                            Festival New projects from film TV</a></div>
                                                    <div class="post-info">April 03, 2016</div>
                                                </article>

                                                <article class="widget-post">
                                                    <figure class="post-thumb"><a href="blog-single.html"><img
                                                                    class="wow fadeIn animated" data-wow-delay="0ms"
                                                                    data-wow-duration="2500ms"
                                                                    src="{{ asset('frontend') }}/images/resource/post-thumb-4.jpg"
                                                                    alt=""
                                                                    style="visibility: visible; animation-duration: 2500ms; animation-delay: 0ms; animation-name: fadeIn;"></a>
                                                        <div class="overlay"><span class="icon qb-play-arrow"></span>
                                                        </div>
                                                    </figure>
                                                    <div class="text"><a href="blog-single.html">Amy Schumer swaps lives
                                                            with Anna Wintour</a></div>
                                                    <div class="post-info">April 04, 2016</div>
                                                </article>

                                                <article class="widget-post">
                                                    <figure class="post-thumb"><a href="blog-single.html"><img
                                                                    class="wow fadeIn animated" data-wow-delay="0ms"
                                                                    data-wow-duration="2500ms"
                                                                    src="{{ asset('frontend') }}/images/resource/post-thumb-1.jpg"
                                                                    alt=""
                                                                    style="visibility: visible; animation-duration: 2500ms; animation-delay: 0ms; animation-name: fadeIn;"></a>
                                                        <div class="overlay"><span class="icon qb-play-arrow"></span>
                                                        </div>
                                                    </figure>
                                                    <div class="text"><a href="blog-single.html">Priyanka Chopra got her
                                                            what photoshopped?</a></div>
                                                    <div class="post-info">April 01, 2016</div>
                                                </article>


                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                            <!--End Product Info Tabs-->

                        </div>


                    </aside>
                </div>


            </div>
        </div>
    </div>


    <style>

        .image h4 {

            position: absolute;
            bottom: 0;
            background: rgba(0, 0, 0, 0.7);
            text-align: left;
            padding: 10px;
            margin: 10px 40px 20px 30px;
            -moz-box-shadow: 3px 3px 3px #000;
            -webkit-box-shadow: 3px 3px 3px #000;
            box-shadow: 3px 3px 3px #000;
            color: white;
            font-size: 15px;
            font-weight: bold;
        }

        /*  VIDEO PLAYER CONTAINER
       ############################### */
        .video-container {
            position: relative;
            padding-bottom: 56.25%;
            padding-top: 30px;
            height: 0;
            overflow: hidden;
        }

        .video-container iframe, .video-container object, .video-container embed {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

        /*  VIDEOS PLAYLIST
         ############################### */
        .vid-list-container {
            width: 100%;
            height: 471px;
            overflow: hidden;
            float: right;

        }

        .vid-list-container:hover, .vid-list-container:focus {
            overflow-y: auto;
        }

        ol#vid-list {
            margin: 0;
            padding: 0;
            background: #222;
        }

        ol#vid-list li {
            list-style: none;
        }

        ol#vid-list li a {
            text-decoration: none;
            background-color: #222;
            height: 70px;
            display: block;
            padding: 10px;
        }

        ol#vid-list li a:hover {
            background-color: #666666
        }

        ol#vid-list li .active {
            background-color: #666666
        }

        .vid-thumb {
            float: left;
            margin-right: 8px;
        }

        .active-vid {
            background: #3A3A3A;
        }

        #vid-list .desc {
            color: #CACACA;
            font-size: 13px;
            margin-top: 5px;
        }

        @media (max-width: 767px) {
            body {
                margin: 15px;
            }

            .caption {
                margin-top: 40px;
            }

            .col-xs-12 {
                padding-left: 0 !important;
                padding-right: 0 !important;
            }

            .vid-list-container {

                padding-bottom: 20px;
            }

        }


    </style>

    <script>

        /*JS FOR SCROLLING THE ROW OF THUMBNAILS*/
        $(document).ready(function () {
            $('.vid-item').each(function (index) {
                $(this).on('click', function () {
                    var current_index = index + 1;
                    $('.vid-item .thumb').removeClass('active');
                    $('.vid-item:nth-child(' + current_index + ') .thumb').addClass('active');
                });
            });
        });

    </script>

@endsection

@section('js')



@endsection