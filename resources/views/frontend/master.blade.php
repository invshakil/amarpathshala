<!DOCTYPE html>
<html>
<head>
    @include('frontend.includes.header_script')
</head>

<body>
<div class="page-wrapper">

    <!-- Preloader -->
    <div class="preloader"></div>

    <!-- Main Header -->
    <header class="main-header">
        @include('frontend.includes.navigation')
    </header>
    <!--End Header Style Two -->


    <!--MAIN CONTENT -->
@yield('content')
<!--MAIN CONTENT -->


    <!--Main Footer-->
    <footer class="main-footer">
        <div class="sticky-stopper"></div>
        @include('frontend.includes.footer')
    </footer>
    <!--End Main Footer-->


    <!--Scroll to top-->
    <div class="scroll-to-top scroll-to-target" data-target="html">
        <span class="icon fa fa-angle-double-up"></span>
    </div>
</div>

<!--End Scroll to top-->
@include('frontend.includes.footer_script')

@yield('js')

</body>
</html>
