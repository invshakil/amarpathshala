

<script src="{{ asset('frontend') }}/js/bootstrap.min.js"></script>
<script src="{{ asset('frontend') }}/js/jquery.fancybox.pack.js"></script>
<script src="{{ asset('frontend') }}/js/jquery.fancybox-media.js"></script>
<script src="{{ asset('frontend') }}/js/owl.js"></script>
<script src="{{ asset('frontend') }}/js/appear.js"></script>
<script src="{{ asset('frontend') }}/js/wow.js"></script>
<script src="{{ asset('frontend') }}/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="{{ asset('frontend') }}/js/script.js"></script>
<script src="{{ asset('frontend') }}/js/color-settings.js"></script>
