@php
    $news_types= App\NewsType::where('visibility_status', 'Yes')->get();
    $blog_categories= App\BlogCategory::where('status', 1)->get();
@endphp


<!--Header Top-->
<div class="header-top">
    <div class="auto-container">
        <div class="clearfix">
            <!--Top Left-->
            <div class="top-left col-md-6 col-sm-12 col-xs-12">
                <div class="trend">Latest:</div>
                <div class="single-item-carousel owl-carousel owl-theme">

                    @php $latestTitles = DB::table('articles')->select('article_title','url_slug')->limit(5)->get(); @endphp

                    @foreach($latestTitles as $title)
                        <div class="slide">
                            <div class="text"><a style="color: #ffffff" href="{{ route('news.details', ['slug'=>$title->url_slug]) }}">{{ $title->article_title }}</a></div>
                        </div>
                    @endforeach

                </div>
            </div>
            <!--Top Right-->
            <div class="top-right pull-right col-md-6 col-sm-12 col-xs-12">
                <ul class="top-nav">
                    @if(Auth::check())
                        <li><a href="
                                    @if(auth()->user()->role == 'Admin')
                                        {{ route('admin.dashboard') }}
                                    @elseif(auth()->user()->role == 'Teacher')
                                        {{ route('teacher') }}
                                    @elseif(auth()->user()->role == 'Student')
                                        {{ route('student') }}
                                    @endif
                                    " target="_blank">Dashboard</a></li>
                        <li><a href="{{ route('logout') }}">Logout</a></li>
                    @else
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                    @endif
                    <li><a href="#">FAQ</a></li>
                </ul>

                <ul class="social-nav">
                    <li><a href="#"><span class="fa fa-facebook-square"></span></a></li>
                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                    <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                    <li><a href="#"><span class="fa fa-youtube-play"></span></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--Header-Upper-->
<div class="header-upper">
    <div class="auto-container">
        <div class="clearfix">

            <div class="pull-left logo-outer">
                <div class="logo"><a href="{{ route('/') }}"></a></div>
            </div>

            <div class="pull-right upper-right clearfix">
                <div class="add-image">
                    <a href="{{ route('/') }}"><img src="{{ asset('frontend') }}/images/resource/header-add.jpg"
                                                    alt=""/></a>
                </div>
            </div>

        </div>
    </div>
</div>
<!--End Header Upper-->

<!--Header Lower-->
<div class="header-lower">
    <div class="auto-container">
        <div class="nav-outer clearfix">
            <!-- Main Menu -->
            <nav class="main-menu">
                <div class="navbar-header">
                    <!-- Toggle Button -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="navbar-collapse collapse clearfix" id="bs-example-navbar-collapse-1">
                    <ul class="navigation clearfix">
                        <li class="@if(Request::url() == route('/')) current @endif"><a href="{{ route('/') }}">Home</a>
                        </li>

                        <li class="mega-menu"><a href="#">Video Lessons</a>
                            <div class="mega-menu-bar" style="width: 50%;">

                                <!--Tab Section-->
                                <div class="tab-section">
                                    <!--Product Tabs-->
                                    <div class="info-tabs tabs-box">
                                        <div class="row clearfix">

                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <!--Tab Btns-->
                                                <ul class="tab-btns tab-buttons clearfix">
                                                    <li data-tab="#jsc" class="tab-btn active-btn">
                                                        JSC
                                                    </li>
                                                    <li data-tab="#ssc" class="tab-btn">SSC</li>
                                                    <li data-tab="#hsc" class="tab-btn">HSC</li>
                                                    <li data-tab="#o-level" class="tab-btn">O Level</li>
                                                    <li data-tab="#a-level" class="tab-btn">A Level</li>
                                                    <li data-tab="#others" class="tab-btn">Other Courses</li>
                                                </ul>
                                            </div>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <!--Tabs Container-->
                                                <div class="tabs-content">

                                                    <!--Tab / Active Tab-->
                                                    <div class="tab active-tab" id="jsc">
                                                        <div class="content">

                                                            <div class="row clearfix">

                                                                <ul class="cat-list" id="mega_menu_content">
                                                                    <li class="clearfix"><a href="#">Chapter 1</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 2</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 3</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 4</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 5</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 6</a></li>
                                                                </ul>

                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="tab" id="ssc">
                                                        <div class="content">

                                                            <div class="row clearfix">

                                                                <ul class="cat-list" id="mega_menu_content">
                                                                    <li class="clearfix"><a href="#">Chapter 1</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 2</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 3</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 4</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 5</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 6</a></li>
                                                                </ul>

                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="tab" id="hsc">
                                                        <div class="content">

                                                            <div class="row clearfix">

                                                                <ul class="cat-list" id="mega_menu_content">
                                                                    <li class="clearfix"><a href="#">Chapter 1</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 2</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 3</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 4</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 5</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 6</a></li>
                                                                </ul>

                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="tab" id="o-level">
                                                        <div class="content">

                                                            <div class="row clearfix">

                                                                <ul class="cat-list" id="mega_menu_content">
                                                                    <li class="clearfix"><a href="#">Chapter 1</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 2</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 3</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 4</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 5</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 6</a></li>
                                                                </ul>

                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="tab" id="a-level">
                                                        <div class="content">

                                                            <div class="row clearfix">

                                                                <ul class="cat-list" id="mega_menu_content">
                                                                    <li class="clearfix"><a href="#">Chapter 1</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 2</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 3</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 4</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 5</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 6</a></li>
                                                                </ul>

                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="tab" id="others">
                                                        <div class="content">

                                                            <div class="row clearfix">

                                                                <ul class="cat-list" id="mega_menu_content">
                                                                    <li class="clearfix"><a href="#">Chapter 1</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 2</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 3</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 4</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 5</a></li>
                                                                    <li class="clearfix"><a href="#">Chapter 6</a></li>
                                                                </ul>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </li>

                        <li class="dropdown"><a href="#">Edu News</a>
                            <ul>
                                @foreach($news_types as $news_type)
                                    <li>
                                        <a href="{{ route('news.type',
                                        ['type'=>str_replace(' ','-',$news_type->news_type_title)])
                                        }}">
                                            {{ $news_type->news_type_title }}

                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>


                        <li class="dropdown"><a href="#">Blog</a>
                            <ul>
                                @foreach($blog_categories as $category)
                                    <li>
                                        <a href="{{ route('blog.type',
                                        ['type'=>str_replace(' ','-',$category->name)])
                                        }}">
                                            {{ $category->name }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="dropdown"><a href="#">Solutions</a>
                        </li>
                        <li><a href="#">Tutor & Tuition</a></li>
                    </ul>
                </div>
            </nav>
            <!-- Main Menu End-->
            <div class="outer-box">
                <!--Search Box-->
                <div class="search-box-outer">
                    <div class="dropdown">
                        <button class="search-box-btn dropdown-toggle" type="button" id="dropdownMenu1"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span
                                    class="fa fa-search"></span></button>
                        <ul class="dropdown-menu pull-right search-panel" aria-labelledby="dropdownMenu1">
                            <li class="panel-outer">
                                <div class="form-container">
                                    <form method="get" action="{{ route('search') }}">

                                        <div class="form-group">
                                            <input type="search" name="search" value=""
                                                   placeholder="Search Here" required>
                                            <button type="submit" class="search-btn"><span
                                                        class="fa fa-search"></span></button>
                                        </div>
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>

            <!-- Hidden Nav Toggler -->
            <div class="nav-toggler">
                <button class="hidden-bar-opener"><span class="icon qb-menu1"></span></button>
            </div>

        </div>
    </div>
</div>
<!--End Header Lower-->

<!--Sticky Header-->
<div class="sticky-header">
    <div class="auto-container clearfix">
        <!--Logo-->
        <div class="logo pull-left">
            <a href="{{ route('/') }}" class="img-responsive" title=""></a>
        </div>

        <!--Right Col-->
        <div class="right-col pull-right">
            <!-- Main Menu -->
            <nav class="main-menu">
                <div class="navbar-header">
                    <!-- Toggle Button -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="navbar-collapse collapse clearfix">
                    <ul class="navigation clearfix">
                        <li><a href="{{ route('/') }}">Home</a></li>

                        <li class="dropdown"><a href="#">Video Lessons</a>
                            <ul>
                                <li class="dropdown">
                                    <a href="#">JSC</a>
                                    <ul>
                                        <li><a href="#">Chapter 1</a></li>
                                        <li><a href="#">Chapter 2</a></li>
                                    </ul>
                                </li>

                                <li class="dropdown">
                                    <a href="#">SSC</a>
                                    <ul>
                                        <li><a href="#">Chapter 1</a></li>
                                        <li><a href="#">Chapter 2</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>

                        <li class="dropdown"><a href="#">Edu News</a>
                            <ul>
                                @foreach($news_types as $news_type)
                                    <li>
                                        <a href="{{ route('news.type',
                                        ['type'=>str_replace(' ','-',$news_type->news_type_title)])
                                        }}">
                                            {{ $news_type->news_type_title }}

                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>

                        <li class="dropdown"><a href="#">Blog</a>
                            <ul>
                                @foreach($blog_categories as $category)
                                    <li>
                                        <a href="{{ route('blog.type',
                                        ['type'=>str_replace(' ','-',$category->name)])
                                        }}">
                                            {{ $category->name }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="dropdown"><a href="#">Solutions</a>
                        </li>
                        <li><a href="#">Tutor & Tuition</a></li>
                    </ul>
                </div>
            </nav><!-- Main Menu End-->
        </div>

    </div>
</div>
<!--End Sticky Header-->

<!-- Hidden Navigation Bar -->
<section class="hidden-bar left-align">

    <div class="hidden-bar-closer">
        <button><span class="qb-close-button"></span></button>
    </div>

    <!-- Hidden Bar Wrapper -->
    <div class="hidden-bar-wrapper">
        <div class="logo">
            <a href="index.html"></a>
        </div>
        <!-- .Side-menu -->
        <div class="side-menu">
            <!--navigation-->
            <ul class="navigation clearfix">
                <li><a href="{{ route('/') }}">Home</a></li>

                <li class="dropdown"><a href="#">Video Lessons</a>
                    <ul>
                        <li class="dropdown">
                            <a href="#">JSC</a>
                            <ul>
                                <li><a href="#">Chapter 1</a></li>
                                <li><a href="#">Chapter 2</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#">SSC</a>
                            <ul>
                                <li><a href="#">Chapter 1</a></li>
                                <li><a href="#">Chapter 2</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="dropdown"><a href="#">Edu News</a>
                    <ul>
                        @foreach($news_types as $news_type)
                            <li>
                                <a href="{{ route('news.type',
                                        ['type'=>str_replace(' ','-',$news_type->news_type_title)])
                                        }}">
                                    {{ $news_type->news_type_title }}

                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>

                <li class="dropdown"><a href="#">Blog</a>
                    <ul>
                        @foreach($blog_categories as $category)
                            <li>
                                <a href="{{ route('blog.type',
                                        ['type'=>str_replace(' ','-',$category->name)])
                                        }}">
                                    {{ $category->name }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>
                <li><a href="#">Solutions</a>
                </li>
                <li><a href="#">Tutor & Tuition</a></li>
            </ul>
        </div>
        <!-- /.Side-menu -->

        <!--Options Box-->
        <div class="options-box">
            <!--Sidebar Search-->
            <div class="sidebar-search">
                <form method="get" action="{{ route('search') }}">

                    <div class="form-group">
                        <input type="search" name="search" value="" placeholder="Search ..." required="">
                        <button type="submit" class="theme-btn"><span class="fa fa-search"></span></button>
                    </div>
                </form>
            </div>

            <!--Social Links-->
            <ul class="social-links clearfix">
                <li><a href="#"><span class="fa fa-facebook-f"></span></a></li>
                <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                <li><a href="#"><span class="fa fa-instagram"></span></a></li>
                <li><a href="#"><span class="fa fa-pinterest"></span></a></li>
            </ul>

        </div>

    </div><!-- / Hidden Bar Wrapper -->

</section>
<!-- End / Hidden Bar -->