<div class="widgets-section">
    <div class="auto-container">
        <div class="row clearfix">

            <!--Widget Column / Reviews Widget-->
            <div class="widget-column col-md-6 col-sm-6 col-xs-12">

                <!--Review Block-->
                <div class="footer-widget popular-tags">
                    @php $inst_types = App\InstituteType::all();
                    $total = count($inst_types);

                    @endphp
                    <h2>Browse Institute Types</h2>

                    @foreach($inst_types->chunk($total/2) as $type_chunk)
                        @foreach($type_chunk as $value)
                    <ul class="cat-list col-md-6" id="footer_ul">

                        <li class="clearfix">
                            <a href="{{ route('institute.type',['slug'=>str_replace(' ','-', $value->institute_type_title)]) }}">
                                {{ $value->institute_type_title }}
                            </a>
                        </li>

                    </ul>
                        @endforeach
                    @endforeach


                </div>

            </div>

            <!--Widget Column / Instagram Widget-->
            <div class="widget-column col-md-3 col-sm-6 col-xs-12">
                <div class="footer-widget popular-tags">
                    <h2>About us & Contacts</h2>
                    <ul class="cat-list" id="footer_ul">
                        <li class="clearfix"><a href="#">Query</a></li>
                        <li class="clearfix"><a href="#">Donation</a></li>
                        <li class="clearfix"><a href="#">Advertise</a></li>
                        <li class="clearfix"><a href="#">Work With Us</a></li>
                        <li class="clearfix"><a href="#">Our Teachers</a></li>
                        <li class="clearfix"><a href="#">Our Team</a></li>
                    </ul>
                </div>

            </div>

            <!--Widget Column / Popular Widget-->
            <div class="widget-column col-md-3 col-sm-6 col-xs-12">
                <div class="footer-widget popular-widget">
                    <h2>Subscribe to Newsletter</h2>
                    <!--NewsLetter Widget-->
                    <div class="newsletter-widget">
                        <div class="inner-box">

                            <div class="emailed-form">
                                <form method="post" action="contact.html">
                                    <div class="form-group">
                                        <input type="email" name="email" value=""
                                               placeholder="Enter your email..." required>
                                        <button type="submit" class="theme-btn">Subscribe!</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
<!--Footer Bottom-->
<div class="footer-bottom">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Column-->
            <div class="column col-md-4 col-sm-12 col-xs-12">
                <div class="logo">
                    <a href="{{ route('/') }}"></a>
                </div>
            </div>
            <!--Column-->
            <div class="column col-md-5 col-sm-12 col-xs-12">
                <div class="text">
                    Amarpathshala is a voulantary work. We provide all service at free of cost. So we need your support
                    or donation to continue our project and to make this project more effective. If you donate us after
                    that if you want how will expending your money we will give you details. To donate us call at
                    +8801713304386 or +880258314722.
                    Also you can email us at <a href="mailto:info@amarpathshala.com">info@amarpathshala.com</a>
                </div>
            </div>
            <!--Column-->
            <div class="column col-md-3 col-sm-12 col-xs-12">
                <ul class="social-icon-one">
                    <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                    <li class="twitter"><a href="#"><span class="fa fa-twitter"></span></a></li>
                    <li class="g_plus"><a href="#"><span class="fa fa-google-plus"></span></a></li>
                    <li class="linkedin"><a href="#"><span class="fa fa-linkedin"></span></a></li>
                    <li class="pinteret"><a href="#"><span class="fa fa-pinterest-p"></span></a></li>
                    <li class="android"><a href="#"><span class="fa fa-android"></span></a></li>
                    <li class="dribbble"><a href="#"><span class="fa fa-dribbble"></span></a></li>
                    <li class="rss"><a href="#"><span class="fa fa-rss"></span></a></li>
                </ul>
            </div>
        </div>
    </div>
    <!--Copyright Section-->
    <div class="copyright-section">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <ul class="footer-nav">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Advertisement</a></li>
                        <li><a href="#">Contacts</a></li>
                    </ul>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="copyright">&copy; Copyright Techno-71. All rights reserved.</div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .popular-tags a{
        text-align: left;
        background: #28292D;
    }
</style>