<meta charset="utf-8">
<title>{{ $title }}</title>
<!-- Stylesheets -->
<link href="{{ asset('frontend') }}/css/bootstrap.css" rel="stylesheet">
<link href="{{ asset('frontend') }}/css/style.css" rel="stylesheet">
<link href="{{ asset('frontend') }}/css/responsive.css" rel="stylesheet">

<!--Color Themes-->
<link id="theme-color-file" href="{{ asset('frontend') }}/css/color-themes/default-theme.css" rel="stylesheet">

<!--Favicon-->
<link rel="shortcut icon" href="{{ asset('assets/') }}/img/favicons/favicon.png" type="image/x-icon">
<link rel="icon" href="{{ asset('frontend') }}/images/favicon.png" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]>
<script src="{{ asset('frontend') }}/js/respond.js"></script><![endif]-->

<meta charset="UTF-8">

@if(isset($keywords))

    <meta name="description" content="{{ $description }}">
    <meta name="keywords" content="{{ $keywords }}">
    <meta name="author" content="{{ $author }}">

    <meta property="og:title" content="{{ $title }}"/>
    <meta property="og:image" content="{{ $image }}"/>
    <meta property="og:site_name" content="{{ $site_name }}"/>
    <meta property="og:description" content="{{ $description }}"/>
    <meta property="og:url" content="{{ $url }}"/>
    <meta property="og:type" content="Website"/>

@endif



<style>
    .overlay-box .content h5 {
        background: rgba(0, 0, 0, 0.5);
        padding: 10px;
    }

    .overlay-box .content a {
        color: white !important;
        font-weight: bold
    }

    .lightbox-image .image h3, h4 {
        -moz-box-shadow: 1px 2px 3px #ccc;
        -webkit-box-shadow: 2px 3px 3px #ccc;
        box-shadow: 1px 2px 3px #ccc;
    }

    #chapter-list {
        margin-bottom: 30px;
    }

    .pagination > li {
        display: inline-block;
        margin: 0 5px 5px 0;
        color: #999999;
        font-weight: 400;
        text-align: center;
        transition: all 500ms ease;
        -webkit-transition: all 500ms ease;
        -ms-transition: all 500ms ease;
        -o-transition: all 500ms ease;
        -moz-transition: all 500ms ease;
        font-family: 'Montserrat', sans-serif;

    }

    .pagination li a:hover,
    .pagination .active > span {
        color: #ffffff;
        border-color: #e73f43;
        background-color: #e73f43;
        font-weight: 600;
    }

    .pagination li a {
        color: #999 !important;
    }

    #footer_ul {
        background-color: #28292D;
        padding-right: 0 !important;
    }

    #footer_ul li {
        border-bottom: none;
        margin-bottom: 0;
    }

    #footer_ul li a {
        font-size: 10px !important;
        font-weight: 600;
    }

    #footer_ul li a:hover {
        color: white;
    }

    #mega_menu_content {
        background-color: #101010;
        padding-right: 10px !important;
    }

    #mega_menu_content li {
        border-bottom: none;
        margin-bottom: 0;
    }

    #mega_menu_content li a {
        color: white;
    }

    /* The sticky */
    .sidebar-side {
        position: -webkit-sticky;
        position: sticky;
        top: 0;
    }

    .news-block-two .inner-box {
        min-height: 340px;
    }

    .news-block-two .inner-box .image a img {
        max-height: 200px;
    }

    .news-block-two .inner-box .lower-box h3 a {
        font-size: 16px;
        font-weight: 600;
    }

    .cat-list li a {
        color: #337ab7!important;
    }

    #footer_ul li a {
        color: #ffffff!important;
    }

    #divEffect {
        border-radius: 5px!important;
        background-color: #ffffff!important;
        padding: 10px 10px;
        /*box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 20px rgba(0, 0, 0, 0.2) inset;*/
        margin-bottom: 10px;
        -moz-box-shadow: 1px 1px 3px 2px #DCDCDC;
        -webkit-box-shadow: 1px 1px 3px 2px #DCDCDC;
        box-shadow: 1px 1px 3px 2px #DCDCDC;
    }

    #sideDivEffect{
        border-radius: 5px!important;
        background-color: #ffffff!important;
        padding-top: 10px;
        padding-right: 5px;
        /*box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 20px rgba(0, 0, 0, 0.2) inset;*/
        margin-bottom: 10px;
        -moz-box-shadow: 1px 1px 3px 2px #DCDCDC;
        -webkit-box-shadow: 1px 1px 3px 2px #DCDCDC;
        box-shadow: 1px 1px 3px 2px #DCDCDC;
        margin-left: 5px;
    }

    #sideDivEffect > figure{
        padding-top: 10px;
        background-color: #fff;
        padding-left: 10px;

    }

    .pagination li a{
        font-weight: bold;
        color: #0a0c0e;
    }

    .pagination>li:first-child>a, .pagination>li:first-child>span{
        font-weight: bold;
    }

    .pagination li a:hover {
        color: #fff!important;
    }
</style>


<script src="{{ asset('frontend') }}/js/jquery.js"></script>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-67799298-1', 'auto');
    ga('send', 'pageview');

</script>