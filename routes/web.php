<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * WEBSITE ROUTES
 */

Route::get('/', ['as' => '/', 'uses' => 'HomeController@index'] );

Route::get('/search-queries', ['as' => 'search', 'uses' => 'HomeController@SearchQuery'] );

Route::get('/news-type/{name}', ['as' => 'news.type', 'uses' => 'HomeController@getArticleByNewsType'] );
Route::get('/blog-type/{name}', ['as' => 'blog.type', 'uses' => 'HomeController@getBlogsByBlogCategory'] );

Route::get('/news-details/{slug}', ['as' => 'news.details', 'uses' => 'HomeController@getArticleDetails'] );

Route::get('/class-name/subject-name/chapter-list', ['as' => 'class.subject.chapter', 'uses' => 'HomeController@getChaptersBySubject'] );
Route::get('/class-name/subject-name/chapter-name/video-lessons', ['as' => 'chapter.video.list', 'uses' => 'HomeController@getLectureBySubject'] );

Route::get('/blog-details/{slug}', ['as' => 'blog.details', 'uses' => 'HomeController@getBlogDetails'] );

Route::get('/institute-information/{slug}', ['as' => 'institute.details', 'uses' => 'HomeController@getInstituteDetails'] );

Route::get('/institutes/{slug}', ['as' => 'institute.type', 'uses' => 'HomeController@getInstitutesByType'] );
Route::get('/profile/{slug}', ['as' => 'author.details', 'uses' => 'HomeController@getAuthorProfile'] );





/*
 * ADMIN ROUTES
 */


Route::group(['middleware' => ['auth','AdminMiddleware']], function () {

    Route::get('/admin/dashboard', ['as' => 'admin.dashboard', 'uses' => 'AdminController@index']);
    Route::get('/admin/dashboard/user-settings', ['as' => 'user.settings', 'uses' => 'AdminController@user_settings']);
    Route::post('/user-settings/update', ['as' => 'user.settings.update', 'uses' => 'AdminController@UpdateUserSettings']);

    Route::get('/admin/module-details', ['as' => 'admin.moduleDetails', 'uses' => 'AdminController@ModuleDetails']);

    /*
     * NEWS PUBLICATIONS URI
     */

    Route::get('/admin/news-publications/post-news', ['as' => 'admin.new.news', 'uses' => 'AdminController@newsPublications']);
    Route::get('/admin/news-publications/manage-news', ['as' => 'admin.manage.news', 'uses' => 'AdminController@ManageNews']);
    Route::get('/admin/news-publications/news-type', ['as' => 'admin.news.type', 'uses' => 'AdminController@newsType']);
    Route::get('/admin/institute-types', ['as' => 'admin.institute.types', 'uses' => 'AdminController@instituteType']);
    Route::get('/admin/institute/group-name', ['as' => 'admin.institute.group', 'uses' => 'AdminController@instituteGroup']);
    Route::get('/admin/add-institutes', ['as' => 'admin.add.institutes', 'uses' => 'AdminController@AddInstitute']);
    Route::get('/admin/manage-institutes', ['as' => 'admin.manage.institutes', 'uses' => 'AdminController@ManageInstitute']);


    /*
     * SEARCH QUERIES ROUTE
     */

    Route::get('/admin/search-institute-type', ['as' => 'admin.search.institute.type', 'uses' => 'AdminSearchQueries@SearchInstituteType']);
    Route::get('/admin/search-institute', ['as' => 'admin.search.institute', 'uses' => 'AdminSearchQueries@SearchInstitute']);
    Route::get('/admin/search-news', ['as' => 'admin.search.news', 'uses' => 'AdminSearchQueries@SearchNews']);
    Route::get('/admin/search-blog', ['as' => 'admin.search.blog', 'uses' => 'AdminSearchQueries@Searchblog']);

    /*
     * -> SEARCH QUERIES ROUTE <-
     */


    /*
     * -> NEWS TYPE CRUD <-
     */
    Route::get('/get-news-type/index', ['as' => 'news-type.index', 'uses' => 'NewsPublicationsCrud@news_type_index']);
    Route::post('/get-news-type/store', ['as' => 'news-type.store', 'uses' => 'NewsPublicationsCrud@news_type_store']);
    Route::post('/get-news-type/update/', ['as' => 'news-type.edit', 'uses' => 'NewsPublicationsCrud@news_type_update']);
    Route::post('/get-news-type/delete/', ['as' => 'news-type.delete', 'uses' => 'NewsPublicationsCrud@news_type_delete']);

    /*
     * -> NEWS TYPE CRUD <-
     */

    /*
     * -> INSTITUTE TYPE CRUD <-
     */
    Route::get('institute-type/index', ['as' => 'institute-type.index', 'uses' => 'NewsPublicationsCrud@inst_type_index']);
    Route::post('institute-type/store', ['as' => 'institute-type.store', 'uses' => 'NewsPublicationsCrud@inst_type_store']);
    Route::post('institute-type/update/', ['as' => 'institute-type.edit', 'uses' => 'NewsPublicationsCrud@inst_type_update']);
    Route::get('institute-type/update/', ['as' => 'institute-type.edit', 'uses' => 'NewsPublicationsCrud@inst_type_update']);
    Route::post('institute-type/delete/', ['as' => 'institute-type.delete', 'uses' => 'NewsPublicationsCrud@inst_type_delete']);

    /*
     * -> INSTITUTE TYPE CRUD <-
     */

    /*
     * -> INSTITUTE GROUP CRUD <-
     */
    Route::get('institute-group/index', ['as' => 'institute-group.index', 'uses' => 'NewsPublicationsCrud@inst_group_index']);
    Route::post('institute-group/store', ['as' => 'institute-group.store', 'uses' => 'NewsPublicationsCrud@inst_group_store']);
    Route::post('institute-group/update/', ['as' => 'institute-group.edit', 'uses' => 'NewsPublicationsCrud@inst_group_update']);
    Route::post('institute-group/delete/', ['as' => 'institute-group.delete', 'uses' => 'NewsPublicationsCrud@inst_group_delete']);

    /*
     * -> INSTITUTE GROUP CRUD <-
     */

    /*
     * -> COUNTRY, STATE, AREA <-
     */
    Route::post('country/store', ['as' => 'country.store', 'uses' => 'NewsPublicationsCrud@countryStore']);
    Route::get('countries/fetchCountries', ['as' => 'countries.fetch', 'uses' => 'NewsPublicationsCrud@getAllCountries']);

    Route::post('state/store', ['as' => 'state.store', 'uses' => 'NewsPublicationsCrud@stateStore']);
    Route::get('states/fetchStates/{id}', ['as' => 'states.fetch.byId', 'uses' => 'NewsPublicationsCrud@getStateById']);

    Route::post('area/store', ['as' => 'area.store', 'uses' => 'NewsPublicationsCrud@areaStore']);
    Route::get('area/fetchAreas/{id}', ['as' => 'areas.fetch.byId', 'uses' => 'NewsPublicationsCrud@getAreaById']);
    /*
     * -> COUNTRY, STATE, AREA <-
     */

    /*
     * -> INSTITUTE CRUD <-
     */

    Route::post('institute/store', ['as' => 'institute.store', 'uses' => 'NewsPublicationsCrud@InstituteStore']);
    Route::get('institute/delete/{id}', ['as' => 'institute.delete', 'uses' => 'NewsPublicationsCrud@InstituteDelete']);
    Route::post('institute/update/{id}', ['as' => 'institute.update', 'uses' => 'NewsPublicationsCrud@InstituteUpdate']);

    Route::get('institute/fetchByTypeId/{id}', ['as' => 'institute.fetch.byTypeId', 'uses' => 'NewsPublicationsCrud@fetchByTypeId']);

    /*
     * -> INSTITUTE CRUD <-
     */

    /*
     * -> NEWS CRUD <-
     */


    Route::post('news/store', ['as' => 'news.store', 'uses' => 'NewsPublicationsCrud@StoreNews']);
    Route::get('news/delete/{id}', ['as' => 'news.delete', 'uses' => 'NewsPublicationsCrud@NewsDelete']);
    Route::get('news/edit/{id}', ['as' => 'news.edit', 'uses' => 'NewsPublicationsCrud@EditNews']);
    Route::post('news/update/', ['as' => 'news.update', 'uses' => 'NewsPublicationsCrud@UpdateNewsInfo']);


    /*
     * -> NEWS CRUD <-
     */


    /*
     * -> BLOG CATEGORIES CRUD <-
     */

    Route::get('admin/blogs/category/index', ['as' => 'blog.category.index', 'uses' => 'BlogManagement@blogCategory']);
    Route::get('admin/blogs/category/fetchCategories', ['as' => 'blog.category.fetch', 'uses' => 'BlogManagement@FetchBlogCategory']);
    Route::post('admin/blogs/category/store', ['as' => 'blog.category.store', 'uses' => 'BlogManagement@BlogCategoryStore']);
    Route::post('admin/blogs/category/delete', ['as' => 'blog.category.delete', 'uses' => 'BlogManagement@BlogCategoryDelete']);
    Route::post('admin/blogs/category/update', ['as' => 'blog.category.update', 'uses' => 'BlogManagement@BlogCategoryUpdate']);


    /*
     * -> BLOG CRUD <-
     */

    Route::get('admin/blogs/create', ['as' => 'create.blog.index', 'uses' => 'BlogManagement@CreateBlog']);
    Route::post('admin/blogs/store', ['as' => 'blog.store', 'uses' => 'BlogManagement@BlogStore']);

    Route::get('admin/blogs/manage', ['as' => 'admin.manage.blog', 'uses' => 'BlogManagement@ManageBlog']);
    Route::get('admin/blogs/manage-pending-blogs', ['as' => 'admin.pending.blog', 'uses' => 'BlogManagement@PendingBlog']);
    Route::get('admin/blogs/my-blogs/', ['as' => 'admin.blog.by.user', 'uses' => 'BlogManagement@BlogByUser']);

    Route::get('admin/blogs/edit/{id}', ['as' => 'blog.edit', 'uses' => 'BlogManagement@EditBlog']);
    Route::get('admin/blogs/delete/{id}', ['as' => 'blog.delete', 'uses' => 'BlogManagement@BlogDelete']);
    Route::post('admin/blogs/update', ['as' => 'blog.update', 'uses' => 'BlogManagement@BlogUpdate']);
});

Route::group(['middleware' => ['auth'] ], function () {
   Route::get('/teacher', function (){
       echo 'On Development stage...please come back later.<br>';
       echo '<a href="'.route('logout').'">Logout</a>';
   });

    Route::get('/student', function (){
        echo 'On Development stage...please come back later.<br>';
        echo '<a href="'.route('logout').'">Logout</a>';
    })->name('student');
});

/*
 * NEWS PUBLICATIONS
 */

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout')->name('logout');